## GRADE ##
Hey, is that our grade down there? If so, everything that we lost points for is actually in the game. Can we tell Travis that?

Wait where are you seeing the grade?

I opened the google-doc down there, but I think I'm wrong, it looks like it was just someone going through and looking at what we're still missing at some point before we turned in.

Yup that was mine. I never got around to updating it at the end there :)

Haha, okay, good to hear! Got a little nervous there for a moment! 

## POST-RELEASE ##
So, I know we probably want to take a little bit longer of a break before we maybe start working on this again, but since I've noticed some bugs and stuff and have some ideas, figured I might as well start collecting them here before I forget about them. This is post release for the record Travis! You're not allowed to notice this stuff if you haven't :P


* (MAJOR) Tar Bullets still freeze slow units eventually! Very noticable on bosses. So what I think is happening here is because we're doing a calculation/reverse-calculation process with doubles/ints the return value is eventually becoming zero, causing the enemies to just flip between zero and zero, effectively making the move speeds of these slow enemies, well, zero. My proposed fix is to give every enemy a move speed max constant, along with their "current" move speed for slowing purposes. Rather than reverse calculate to return to normal speed, enemies should instead just set their current movespeed back to their max move speed. This way weird math stuff should stop happening. Changing the move speed of the units into a double isn't a bad idea either, for cleaner calculating, but I still think we should implement the constant even if we do that too, the more fail-safes the better I think. Tar bullets should also probably be more expensive just as a general balance note, slowing is too good even if it's not broken like it is now.

* (MAJOR) The winding road map just loads forever. Don't know if this happens all the time, but it happened on my sister's laptop. 

* (FEATURE) We talked about this before, but didn't think to implement as we went forward, but I think it's a good idea. I think we should implement the ability to purchase levels with energy. The cost should increase the higher the level the unit is. I think the best way to handle it might be to base the cost off the amount of exp the unit needs to get to the next level. It'd be cool for example, if buying the last bit of experience a tower needs wouldn't cost as much. Along with this, displaying the exp a tower has/exp a tower needs would be neat and helpful on the info panel. The reason I think this would help is that towers you build later in the game tend to be essentially useless since they're too low a level to do much damage. Being able to use your surplus energy on boosting your new towers would be a pretty big boon I think, and add another layer of strategy for players. (For example, I think buying levels early on would be a really bad idea and a bad use of energy, since those towers will level naturally, and you're better off buying items/attack towers/economy towers then. 

* (FEATURE) There should be more to engineer towers! Not really specializations for them per say, but unique bonuses they can only get one of per tower, perhaps also costing 50 energy? So you have the choice to either increase the energy output (existing), give exp for hitting enemies instead of just killing, increase exp gain for defenders dying, give energy for letting enemies through, funky little boosts like that. It'd add more interactivity with them and provide some fun choices I think. 

* (ART NOTICE) I'll work on this eventually, but I'm planning to create death animations for all the enemies. Crystals will shatter, Spirit mages will like dissipate or something like that. Also I'll make zambambo recolors, and make the boss zambambo giant sized. 

* (MUSIC NOTICE) Probably the fastest way to add quite a lot to the thing, I still have all the music I picked out, so if anyone's planning to work on implementing that, hit me up and I'll give you the folder I made with all the music and sounds. 

# Ataraxia Defense
## CS335 Final Project ##

[Project Requirements Google Doc](https://docs.google.com/document/d/14-YCvxkifY18OjLUAhnKx0t21J_q-TNbYWVrYDepP8Q/edit?usp=sharing)

## Hello Again Travis! ##
Hey Travis,

You can run our game from PlayGame.java in the controller package, or MainMenu.java in view. You can also run the JAR file we generated. Everything should be self explanatory from there on. You can read instructions about gameplay from the menu.

Thanks for everything, and have a good Christmas!

~ Team Guardians of the Galaxy

## BUGS ##
* FIXED ~~(MAJOR) Towers that should lose units when the specialize currently do not!~~
* FIXED ~~(CHANGE) Towers when they specialize should behave as if they are level 11 for XP requirement purposes. Currently they jump in strength too much~~
* FIXED ~~(MAJOR) If an enemy is targeted by a friendly defender, it cannot be targeted by ranged units either.~~
* Not sure if this is an easy fix, but when two towers are built across the path from each other, their units overlap with each other (NATHAN)
* (minor) Blinking in Multiplayer sprites caused by previousImage not being updated. Slightly complicated fix so I'm leaving it for the moment to focus on more critical stuff
* ~~Found a bug where the timer reached 0 and all gameplay stopped. Cannot tell if the game was won or not~~
* ~~Frequently encountering a freezing bug during gameplay. Error is occurring with the info bar at line 212 in MapPanel~~ (fixed by Nathan)

## SPRINT 4 (12/1-12/7) ##
* Most likely dropping this ~~Map Builder~~
* CHRISTOPHER: ~~Make options panel work~~
* CHRISTOPHER: ~~Update sprites of enemy and defender units~~
* CHRISTOPHER: ~~Fix rifleman sprite bug~~
* MATT: ~~**Create more tower unit upgrades** - probably the biggest thing left to do~~
* MATT: ~~Item/Tower/Enemy Descriptions~~
* CASEY: ~~Next wave info/time ticker~~
* NATHAN: ~~Minor improvements to multiplayer~~
* ALL: ~~Sound~~ dropped
* ALL: ~~Clean up code for delivery; JavaDoc, remove commented out code, get rid of all warnings, create Jar file~~

## Completed Items ##
* DONE ~~Array index out of range: 4 on all maps other than default (NATHAN)~~
* DONE? ~~enemy attack bugs (running away after killing one unit) (NATHAN)~~
* DONE ~~if chat message is sent at same time as a gamestate update it crashes the server (NATHAN)~~
* DONE ~~Enemies are not stopping at all to fight (NATHAN)~~
* DONE ~~Enemies attack and kill RiflemanUnits that attack them (sorry NATHAN :))~~
* DONE ~~Adding many engineer towers lags the game (CHRISTOPHER)~~
* DONE: ~~Bugfixes listed above~~
* DONE ~~Multiplayer~~
* DONE ~~Add tower unit item drag and drop capability to upgrade~~
* DONE ~~Add enemy wave capabilities, with random waves maybe?~~
* DONE ~~Create rifleman with all range abilities~~
* DONE ~~Larger maps are cut off (CASEY)~~
* CHRISTOPHER: ~~Add applyOnUpdateTower to item~~
* CHRISTOPHER: ~~GameConfig list of Tower Units, Enemy Units, Towers, Items, and Maps~~
* CHRISTOPHER: ~~Make towers only allow one of each item~~
* CHRISTOPHER: ~~Randomize wave generation~~
* MATT: ~~Create different types of enemy units~~
* CASEY: ~~Map selector~~
* DONE ~~NATHAN reassigning to CASEY and CHRISTOPHER: Game speed toggling (also listed as required) -- this should be pretty easy, just add a "speed" instance variable to Game and then as the first line of Game.update() put deltaTime *= speed. Then changing the speed is just a matter of setting that instance variable. And pausing can be implemented as speed=0 rather than having its own boolean flag.~~
* DONE ~~ Since the win and lose panels are now JDialogs, the old game frame does not properly close upon returning to the main menu~~
* ~~Multiplayer maps are not working (NATHAN)~~ Fixed, but see below:
* DONE ~~The bigger maps we've added are too big for the Multiplayer view to work with, and we're going to have to change the GUI a bit. We have a few choices:
    1. Created a tabbed pane for the other player's view. I don't really like this because the head-to-head view we have right now is really cool.
    2. Create an alternate MapPanel class that allows for scaling the entire view down by an arbitrary amount. I'll try to implement this (probably tomorrow), otherwise a tabbed pane is a good backup.
    3. A third option is to use a JScrollPane and have a horizontal scrollbar when necessary. It would look pretty ghetto but its another backup option
    4. Limit multiplayer maps to only 640x640~~
* FIXED ~~Discovered a bug with the crystal units where they cannot be killed and do no damage to defenders. Creates a game state where all nearby towers infinitely attack the crystal and enemies bypass the engaged towers.~~
* CASEY: ~~Options panel (FPS, difficulty level, game speed)~~ Just need to add listeners to the options panel
* ~~CASEY: Tutorial (Just text)~~
* CASEY: ~~Options button from Game play screen~~ Menu bar is created. Let me know what else you want in this area.
* ~~CHRISTOPHER: Create scion with all range abilities~~
* DONE ~~NATHAN: Pause men~~u
* DONE ~~NATHAN: Save and load game (listed as required in project specs)~~

## Hello Travis! ##
You can run our game either from PlayGame.java or HomePage.java in the Controller package. You can build towers by dragging and dropping from the buttons on the top right
of the GUI to an empty tower spot. The middle tower increases resource generation, and the right tower spawns friendly soldiers that block the enemy.
The left tower (the greyed out one) hasn't been implemented yet.

If you need anything email any of us!

~ Team Guardians of the Galaxy

### Contributors:###
@echochristopher 
@nbabcock 
@mattscout007 
@CSkowron11

### Christopher: ###
* Tower select mode
* Add RiflemanUnit
### Matt: ###
* Create sprite objects
### Nathan ###
* Units on top of each other
### Casey ###
* Tower select GUI