package controller;

import model.tower.Item;
import model.tower.Tower;
import model.tower.item.*;
import model.tower.type.*;

/**
 * Static game configuration variables
 * Some of these could be updated in options
 * @author echochristopher
 *
 */
public class GameConfig {
	public static final int FPS = 30;
	public static final int STARTING_ENERGY = 500;
	public static final int LIVES = 25;
	public static final Tower[] towerList = {new EngineerTower(),new DefenderTower(), new EngineerTower()};
	public static final Item[] itemList = {
		new PlasmaCoreI(),
		new Poison(),
		new SlowDown(),
		new IncreaseEngineerTowerRate(),
		new CryoBar(),
		new Criticola(),
		new Guardian(),
		new Berserker(),
		new Agent(),
		new Marksman(),
		new Empress(),
		new Outcast()
		};
	public static final String[] maps = {
		"Default",
		"BiggerMap",
		"FinalDestination",
		"GreenHouseBasic",
		"GreenHouseMulti",
		"LavaBasic",
		"LavaFork",
		"LavaMulti",
		"WindingRoad"
	};
	public static final String[] multiplayerMaps = {
		"Default",
		"GreenHouseMulti",
		"LavaMulti",
	};
	/**
	 * Checks if a map is multipath. This must be hardcoded
	 * @param mapID the map to check
	 */
	public static final boolean isMultipathMap(String mapID){
		return mapID.equals("LavaFork"); // || mapID.equals("MultiPath");
	}
	public static int gameSpeed = 1;
}
