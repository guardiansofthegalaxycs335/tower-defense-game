package controller;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Game;
import model.map.Map;
import network.AddMessageCommand;
import network.Command;
import network.DisconnectCommand;
import network.UpdateGameCommandServer;
import view.ChatPanel;
import view.CountdownDialog;
import view.GamePanel;
import view.LoadingDialog;
import view.LobbyPanel;
import view.MainMenu;
import view.MapPanel;

/**
 * The client side of our multiplayer server, based on the NRC client that Gabe wrote for section.
 * 
 * @author Nathan Babcock
 * @author Gabriel Kishi
 */
public class Multiplayer extends JFrame {
	private static final long serialVersionUID = -5149932638873549855L;
	private String clientName; // user name of the client
	private ChatPanel chatPanel;
	private LobbyPanel lobbyPanel;
	private GamePanel gamePanel;
	private MapPanel otherMap;
	private Game otherGame;
	private Game game;
	private Timer ping;
	private String player1name, player2name;

	private Socket server; // connection to server
	private ObjectOutputStream out; // output stream
	private ObjectInputStream in; // input stream

	public Multiplayer(String host, String port, String username){
		this.clientName = username;

		if (host == null || port == null || clientName == null)
			return;

		try{
			// Open a connection to the server
			server = new Socket(host, Integer.parseInt(port));
			out = new ObjectOutputStream(server.getOutputStream());
			in = new ObjectInputStream(server.getInputStream());

			// Handle duplicate client names
			while(true){
				out.writeObject(clientName);
				if((boolean) in.readObject() == true){
					System.out.println("Connection accepted by server "+host+" on port "+port+" with username "+clientName);
					break;
				}
				else{
					System.out.println("Server denied connection; duplicate username "+clientName);
					clientName = JOptionPane.showInputDialog("A player with that username is already connected to the server.\nPlease choose a different user name:");
				}
			}

			// add a listener that sends a disconnect command to when closing
			this.addWindowListener(new WindowAdapter(){
				public void windowClosing(WindowEvent arg0) {
					close();
				}
			});

			setupGUI();

			// start a thread for handling server events
			new Thread(new ServerHandler()).start();

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Responsible for arranging the multiplayer GUI
	 * 
	 * @author Nathan
	 */
	private void setupGUI(){
		chatPanel = new ChatPanel(clientName, this);
		lobbyPanel = new LobbyPanel(clientName, out);

		setLayout(new BorderLayout());
		add(lobbyPanel, BorderLayout.NORTH);
		add(chatPanel, BorderLayout.SOUTH);

		// Window
		setSize(1000, 850);
		setExtendedState(MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * Responsible for starting the server
	 * 
	 * @param server name
	 * @param port number
	 * @param clientName
	 */
	public void startClient(String host, String port, final String clientName){
		// ask the user for a host, port, and user name
		this.clientName = clientName;

		if (host == null || port == null || clientName == null)
			return;

		try{
			// Open a connection to the server
			server = new Socket(host, Integer.parseInt(port));
			out = new ObjectOutputStream(server.getOutputStream());
			in = new ObjectInputStream(server.getInputStream());

			// write out the name of this client
			out.writeObject(clientName);

			// Set up chatPanel
			chatPanel = new ChatPanel(clientName, this);
			add(chatPanel, BorderLayout.SOUTH);

			// add a listener that sends a disconnect command to when closing
			this.addWindowListener(new WindowAdapter(){
				public void windowClosing(WindowEvent arg0) {
					try {
						out.writeObject(new DisconnectCommand(clientName));
						out.close();
						in.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			// start a thread for handling server events
			new Thread(new ServerHandler()).start();

		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Starts the timer and everything needed for the game to run
	 */
	public void startGame(Game baseGamestate){
		JDialog loadingMsg = new LoadingDialog(this);

		// Get base gamestate
		// To do this we have to generate our own game on the clientside using the MapID
		// and then merge, because Map is not serialized.
		// TODO instead of merge(), create a setMap() for Game
		game = new Game(new Map(baseGamestate.getMapID()));
		game.merge(baseGamestate);
		otherGame = new Game(new Map(baseGamestate.getMapID()));
		otherGame.merge(game);

		// Set player names
		if(clientName.equals(player1name)){
			game.setPlayerName(player1name + " (host)");
			otherGame.setPlayerName(player2name);
		} else {
			game.setPlayerName(player2name);
			otherGame.setPlayerName(player1name + " (host)");
		}

		// Game panels container
		JPanel container = new JPanel();
		container.setLayout(new BorderLayout());

		// Game Panel
		gamePanel = new GamePanel(game, false);
		container.add(gamePanel, BorderLayout.WEST);

		// Other map panel
		otherGame.merge(game);
		otherMap = new MapPanel(otherGame, null);
		otherMap.setBorder(new EmptyBorder(10, 0, 0, 0));
		container.add(otherMap, BorderLayout.EAST);

		// Server-side game updates
		ping = new Timer();
		ping.scheduleAtFixedRate(new TimerTask(){public synchronized void run() {
			pushGamestate();
		}}, 0, 2000);

		// Client-side game updates
		final Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask(){public void run() {
			if(otherGame.lost() || game.won()){
				game.win();
				otherGame.lose();
				ping.cancel();
				timer.cancel();
//				return;
			}
			if(otherGame.won() || game.lost()){
				game.lose();
				otherGame.win();
				ping.cancel();
				timer.cancel();
//				return;
			}
			game.update(1.0/GameConfig.FPS);
			otherGame.update(1.0/GameConfig.FPS);
			otherMap.setGame(otherGame);
			otherMap.repaint();
		}}, 5000, 1000/GameConfig.FPS);

		// Switch out GUI elements
		loadingMsg.dispose();
		lobbyPanel.setVisible(false);
		add(container);
		setSize(new Dimension(gamePanel.getWidth() + otherMap.getWidth() + 80, gamePanel.getHeight() + chatPanel.getHeight()));
		setExtendedState(MAXIMIZED_BOTH);
		new CountdownDialog(this);
	}
	
	/**
	 * Pushes the latest copy of this instance's gamestate to the server, which in turn prompts the server to return a copy of the opponents game.
	 * This return from the server will come in the form of a UpdateGameCommandClient, which is caught by the ServerHandler thread like any normal command.
	 */
	public synchronized void pushGamestate(){
		try {
			Command<Server> command = new UpdateGameCommandServer(new String(clientName), game.deepClone());
			out.writeObject(command);
		} catch (Exception e) {
		}
	}

	/**
	 * This class reads and executes commands sent from the server
	 * 
	 * @author Gabriel Kishi
	 *
	 */
	private class ServerHandler implements Runnable{
		@SuppressWarnings("unchecked")
		public void run() {
			try{
				while(true){
					// read a command from server and execute it
					Command<Multiplayer> c = (Command<Multiplayer>)in.readObject();
					c.execute(Multiplayer.this);
				}
			}
			catch(SocketException e){
				return; // "gracefully" terminate after disconnect
			}
			catch (EOFException e) {
				return; // "gracefully" terminate
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}


	/**
	 * Closes the current multiplayer game and returns to the main menu. Handles showing the confirmation dialog and sending a DisconnectCommand to the server
	 */
	public void close(){
		// Confirmation dialog
		int option = JOptionPane.YES_OPTION;

		if(game != null)
			if(! (game.lost() || game.won()))
				option = JOptionPane.showConfirmDialog(this,"Are you sure you want to forfeit the game and return to the main menu?", "Confirm ragequit", JOptionPane.YES_NO_OPTION);

		if(option == JOptionPane.YES_OPTION){
			try {
				out.writeObject(new DisconnectCommand(clientName));
				out.close();
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			dispose();
			new MainMenu();
		}
	}
	
	/**
	 * Takes the latest copy of the opponent's gamestate from the server and merges it into the local copy
	 * @param clientName
	 * @param game
	 */
	public void updateGame(String clientName, Game game){
		if(! clientName.equals(this.clientName))
			otherGame.merge(game);
	}

	/**
	 * Updates the ChatPanel with the updated message log
	 * 
	 * @param messages	the log of messages to display
	 */
	public void update(List<String> messages) {
		chatPanel.update(messages);
	}

	/**
	 * Takes ready states from the server and applies them on the client
	 * @param player1ready
	 * @param player2ready
	 */
	public void setReadyStates(boolean player1ready, boolean player2ready){
		lobbyPanel.setReadyStates(player1ready, player2ready);
	}

	/**
	 * Takes player names from the server and applies them on the client
	 * @param player1name
	 * @param player2name
	 */
	public void setPlayerNames(String player1name, String player2name){
		this.player1name = player1name;
		this.player2name = player2name;
		lobbyPanel.setPlayerNames(player1name, player2name);
	}

	/**
	 * Method that is called on other clients when the host terminates the server
	 */
	public void serverTerminated(){
		if(ping != null) ping.cancel();
		if(game != null){
			game.togglePaused();
			otherGame.togglePaused();
		}
		JOptionPane.showMessageDialog(this, "Disconnected -- host terminated server.", "Disconnected", JOptionPane.OK_OPTION);
		dispose();
		new MainMenu();
	}

	/**
	 * Sends a chat message to the server to be added to the log.
	 * @param message
	 */
	public synchronized void addMessage(String message) {
		try {
			out.writeObject(new AddMessageCommand(message));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getClientName(){
		return clientName;
	}

	public Game getGame(){
		return game;
	}
}
