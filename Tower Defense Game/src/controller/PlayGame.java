package controller;

import view.MainMenu;

/**
 * For the sake of convenience, an easy way to launch the main menu from Controller.
 * So you don't have to dig around in the dozens of View files.
 * @author Nathan
 *
 */

public class PlayGame {
	public static void main(String[] ags){
		new MainMenu();
	}
}
