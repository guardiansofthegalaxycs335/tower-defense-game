package controller;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import model.Game;
import model.map.Map;
import model.unit.enemyunit.WaveGenerator;
import network.Command;
import network.DisconnectCommand;
import network.GameStartCommand;
import network.PlayerNamesCommand;
import network.RagequitCommand;
import network.ReadyCommandClient;
import network.ServerTerminatedCommand;
import network.UpdateChatCommand;
import network.UpdateGameCommandClient;

/**
 * The server side of multiplayer. Based on Gabe's NRC server from section.
 * 
 * @author Nathan Babcock
 * @author Gabriel Kishi
 */
public class Server {
	private ServerSocket socket; // the server socket

	private List<String> messages;	// the chat log
	private HashMap<String, ObjectOutputStream> outputs; // map of all connected users' output streams
	private String mapID;

	private String player1name, player2name;
	private boolean player1ready, player2ready;

	private Timer startTimer;
	private int countDown;

	/**
	 *	This thread reads and executes commands sent by a client
	 */
	private class ClientHandler implements Runnable{
		private ObjectInputStream input; // the input stream from the client

		public ClientHandler(ObjectInputStream input){
			this.input = input;
		}

		@SuppressWarnings("unchecked")
		public void run() {
			try{
				while(true){
					// read a command from the client, execute on the server
					Command<Server> command = (Command<Server>)input.readObject();
					command.execute(Server.this);

					// terminate if client is disconnecting
					if (command instanceof DisconnectCommand){
						input.close();
						return;
					}
				}
			} catch(Exception e){
				// e.printStackTrace();
			}
		}
	}

	/**
	 *	This thread listens for and sets up connections to new clients
	 */
	private class ClientAccepter implements Runnable{
		public void run() {
			try{
				while(true){
					// accept a new client, get output & input streams
					Socket s = socket.accept();
					ObjectOutputStream output = new ObjectOutputStream(s.getOutputStream());
					ObjectInputStream input = new ObjectInputStream(s.getInputStream());

					// Handle duplicate client names
					String clientName;
					while(true){
						clientName = (String) input.readObject();
						if(outputs.containsKey(clientName)){
							System.out.println("Refused connection from client with duplicate username "+clientName);
							output.writeObject(false);
						}
						else{
							//							System.out.println(clientName + " has connected");
							output.writeObject(true);
							break;
						}
					}

					// map client name to output stream
					outputs.put(clientName, output);

					// Send player1 and 2 names
					if(player1name == null)
						player1name = clientName;
					else if(player2name == null)
						player2name = clientName;
					sendPlayerNames();

					// spawn a thread to handle communication with this client
					new Thread(new ClientHandler(input)).start();

					// add a notification message to the chat log
					addMessage(clientName + " connected");
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	public Server(String port, String mapID){
		this.messages = new Vector<String>(); // create the chat log
		this.outputs = new HashMap<String, ObjectOutputStream>(); // setup the map
		this.mapID = mapID;

		try{
			// start a new server on port 9001
			socket = new ServerSocket(Integer.parseInt(port));
			System.out.println("Tower defense server started on port "+port);

			// spawn a client accepter thread
			new Thread(new ClientAccepter()).start();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Adds a message to the chat log. Called by an AddMessageCommand.
	 * 
	 * @param message	message to add
	 */
	public void addMessage(String message){
		System.out.println(">>> "+message);
		messages.add(message);
		updateClients();
	}

	/**
	 * Writes an UpdateClientCommand to every connected user.
	 */
	public synchronized void updateClients() {
		// make an UpdateClientCommmand, write to all connected users
		UpdateChatCommand update = new UpdateChatCommand(messages);
		try{
			for (ObjectOutputStream out : outputs.values())
				out.writeObject(update);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Called on the server from clients to update the ready states of players in the lobby
	 * @param player1ready
	 * @param player2ready
	 */
	public void setReadyStates(boolean player1ready, boolean player2ready){
		if(this.player1ready != player1ready){
			this.player1ready = player1ready;
			System.out.println(player1name + " changed status to " + (player1ready ? "ready" : "not ready"));
		}
		if(this.player2ready != player2ready) {
			this.player2ready = player2ready;
			System.out.println(player2name + " changed status to " + (player2ready ? "ready" : "not ready"));
		}

		ReadyCommandClient readyState = new ReadyCommandClient(player1ready, player2ready);
		try{
			for (ObjectOutputStream out : outputs.values()){
				out.writeObject(readyState);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

		if(player1ready && player2ready){ // Start game if both are ready
			startGame();
		}

	}

	/**
	 * Sends the server's copy of player names to all clients
	 */
	public void sendPlayerNames(){
		if(outputs.size() <= 2){
			PlayerNamesCommand names = new PlayerNamesCommand(player1name, player2name);
			try{
				for (ObjectOutputStream out : outputs.values())
					out.writeObject(names);
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	/**
	 * Starts the game, called once all clients have readied up. Instantiates a game, generates waves on it, and pushes this base gamestate to clients.
	 * Then it starts the server-side countdown to the game start.
	 */
	public void startGame(){
		System.out.println("Starting game...");
		System.out.println("- Building gamestate");
		Game game = new Game(new Map(mapID));
		System.out.println("- Generating waves");
		WaveGenerator waveGenerator = new WaveGenerator(game);
		waveGenerator.generate();
		System.out.println("- Pushing gamestate to clients");
		try{
			for (ObjectOutputStream out : outputs.values()){
				out.writeObject(new GameStartCommand(game));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("- Initiating countdown");
		// Five second countdown before game starts
		// This gives both server and players time to prepare themselves
		// Even after the 5 second delay is over, there is still the normal delay before the first wave
		// Countdown is printed to console
		startTimer = new Timer();
		countDown = 5;
		addMessage("Game starting in");
		startTimer.scheduleAtFixedRate(new TimerTask(){public void run() {
			if(countDown <= 0 ){
				startTimer.cancel();
				addMessage("GO!");
			} else
				addMessage("..." + countDown--);
		}}, 1100, 1000); // Delay to compensate for some initial lag and try to keep timers in sync
	}

	/**
	 * Disconnects a given user from the server gracefully
	 * @param clientName	user to disconnect
	 */
	public void disconnect(String clientName) {
		try{
			outputs.get(clientName).close(); // close output stream
			outputs.remove(clientName); // remove from map

			// add notification message
			addMessage(clientName + " disconnected");

			if(! clientName.equals(player1name)) { // guest left
				if(outputs.size() < 2){
					if(player1ready && player2ready){ // Game already started
						try {
							outputs.get(player1name).writeObject(new RagequitCommand());
						} catch (Exception e){
							e.printStackTrace();
						}
					} else { // Still in lobby
						player2name = null;
						player2ready = false;
						sendPlayerNames();
						setReadyStates(player1ready, player2ready);
					}
				}
			}
			else { // host left
				try {
					outputs.get(player2name).writeObject(new ServerTerminatedCommand());
				} catch (Exception e){
					// Ignore error if player2 has already left
				}
			}

		} catch(Exception e){
		}
	}

	/**
	 * Updates the server-side copy of a client's game
	 * @param clientName
	 * @param game
	 */
	public synchronized void updateGame(String clientName, Game game){
		ObjectOutputStream out;
		// TODO this logic is ugly and redundant but it works
		if(clientName.equals(player1name)){
			if(game.lost())
				addMessage(player1name + " has lost the game! " + player2name + "wins.");
			else if (game.won())
				addMessage(player1name + " has won the game! " + player2name + "loses.");
			out = outputs.get(player2name);
		} else {
			out = outputs.get(player1name);
		}
		try {
			out.writeObject(new UpdateGameCommandClient(clientName, game.deepClone()));
		} catch (Exception e){
			// Ignore errors ;D
		}
	}
	
	public static void main(String[] args){
		new Server("9001", "default");
	}
}