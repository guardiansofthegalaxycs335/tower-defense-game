package controller;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import model.Game;
import model.map.Map;
import model.unit.enemyunit.WaveGenerator;
import view.GamePanel;
import view.MainMenu;
import view.PauseMenu;

/**
 * The singleplayer mode of the game
 * @author Nathan
 *
 */

public class Singleplayer extends JFrame {
	private static final long serialVersionUID = -1189627096496222070L;
	private Game game;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItem;

	/**
	 * Allows us to create a Singleplayer using a previous gamestate (like from a savegame)
	 */
	public Singleplayer(Game baseGamestate){
		if(GameConfig.isMultipathMap(baseGamestate.getMapID()))
			game = new Game(new Map(baseGamestate.getMapID(), 2));
		else
			game = new Game(new Map(baseGamestate.getMapID()));
		game.merge(baseGamestate);

		init();
		toFront();
		
		// Start paused if it's a loadgame
		game.togglePaused();
		new PauseMenu(this);
		game.togglePaused();
	}

	/**
	 * The standard constructor, which takes a mapID as a string
	 * @param map
	 */
	public Singleplayer(String map){
		if(GameConfig.isMultipathMap(map))
			game = new Game(new Map(map, 2));
		else
			game = new Game(new Map(map));
		init();
	}

	/**
	 * Sets up primary parts of the single player game mode GUI
	 * @author Nathan
	 */
	public void init(){
		GamePanel gamePanel = new GamePanel(game);
		add(gamePanel);
//		setJMenuBar(layoutMenu());

		setSize(gamePanel.getSize());
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setVisible(true);

		addWindowListener(new ClosingListener());
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new MyDispatcher());

		if(game.getWaves().size() == 0){ // If waves haven't been generated yet, do that.
			WaveGenerator waveGenerator = new WaveGenerator(game);
			waveGenerator.generate();
		}

		startGame();
	}


	/**
	 * A KeyEventDispatcher that handles listening to the spacebar for pausing the game
	 * @author Nathan
	 *
	 */
	private class MyDispatcher implements KeyEventDispatcher {
		@Override
		public boolean dispatchKeyEvent(KeyEvent e) {
			if (e.getKeyChar() == ' ') {
				if(game.paused()) return false;
				game.togglePaused();
				new PauseMenu(Singleplayer.this);
				game.togglePaused();
			}
			return false;
		}
	}

	/**
	 * A listener for closing the window, calling close() to generate the confirmation dialog
	 * @author Nathan
	 *
	 */
	private class ClosingListener implements WindowListener {
		@Override
		public void windowClosing(WindowEvent e) {
			close();
		}

		public void windowOpened(WindowEvent e) {}
		public void windowClosed(WindowEvent e) {}
		public void windowIconified(WindowEvent e) {}
		public void windowDeiconified(WindowEvent e) {}
		public void windowActivated(WindowEvent e) {}
		public void windowDeactivated(WindowEvent e) {}
	}
	
	
	/**
	 * Serializes a singleplayer game and saves it to a .save file.
	 * @param saveName
	 * @author Nathan
	 */
	public boolean saveGame(String saveName){
		try {
			FileOutputStream fileOut = new FileOutputStream(new File(getClass().getResource("/saves/"+saveName+".save").toURI()));
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(game);
			objectOut.close();
			fileOut.close();
			return true;
		} catch (Exception e) {
			System.err.println("Error writing file: " + e);
			return false;
		}
	}

	/**
	 * Creates the menu bar atop the single player game mode
	 * 
	 * @return Menu bar
	 * @author Casey
	 */
	public JMenuBar layoutMenu(){
		menuBar = new JMenuBar();
		menu = new JMenu("Menu");
		// Main menu item
		menuItem = new JMenuItem("Main Menu");
		menuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		menu.add(menuItem);

		// Pause item
		menuItem = new JMenuItem("Pause");
		menuItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				game.togglePaused();
				new PauseMenu(Singleplayer.this);
				game.togglePaused();
			}
		});
		menu.add(menuItem);

		menuBar.add(menu);
		return menuBar;
	}

	private void startGame(){
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask(){public void run() {game.update(1.0/GameConfig.FPS);}}, 0, 1000/GameConfig.FPS);
	}

	/**
	 * Handles the confirmation dialog for closing the singleplayer game and returning to the main menu. This is called from the pause menu and the window closing listener.
	 * @author Nathan
	 */
	public void close(){
		boolean alreadyPaused = false;
		if(game.paused()) alreadyPaused = true;
		if(!alreadyPaused)
			game.togglePaused();
		int exit = JOptionPane.showConfirmDialog(null, "Would you like to save your game?", "Quit game", JOptionPane.YES_NO_CANCEL_OPTION);
		switch(exit){
		case JOptionPane.YES_OPTION:
			String name = JOptionPane.showInputDialog("Please choose a name for this savegame.");
			if(name != null)
				if(saveGame(name))
					JOptionPane.showMessageDialog(Singleplayer.this, "Game saved!");
				else
					JOptionPane.showMessageDialog(Singleplayer.this, "Game save failed.");
			else{ // Closing the confirmation dialog is the same as cancelling
				if(!alreadyPaused)
					game.togglePaused();
				return;
			}
			// Fall through to closing
		case JOptionPane.NO_OPTION:
			new MainMenu();
			dispose();
			break;
		case JOptionPane.CANCEL_OPTION:
			if(!alreadyPaused)
				game.togglePaused();
			break;
		}
	}

	public static void main(String[] args){
		new Singleplayer("Default");
	}
}
