package model;

import java.io.Serializable;

/**
 * A bunch of classes shared the same logic about cooldowns: unit attacks, unit spawns, enemy attacks, etc, so I abstracted it into this class.
 * Eventually this can also be used for spawning enemy waves, resource regeneration, etc -- basically anything that occurs on a regular interval.
 * 
 * @author Nathan
 */
public class Cooldown implements Serializable {
	private static final long serialVersionUID = -7987799115273905096L;
	double base, current;
	
	/**
	 * Allow instantiating a cooldown without knowing the base rate first. I.e. in abstract Units before attack speed is known
	 */
	public Cooldown(){}
	
	public Cooldown(double base){
		this.base = base;
		reset();
	}
	
	public double getBase(){
		return base;
	}
	
	public void setBase(double n){
		base = n;
	}
	
	public double getCurrent(){
		return current;
	}
	
	public void setCurrent(double n){
		current = n;
	}
	
	public void decrease(double n){
		if(current == 0) return;
		current -= n;
		if(current < 0) current = 0;
	}
	
	/**
	 * Resets the current cooldown to the base value
	 */
	public void reset(){
		current = base;
	}
}
