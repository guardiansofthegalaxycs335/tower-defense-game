package model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Vector;

import model.map.Map;
import model.tower.Tower;
import model.tower.type.DefenderTower;
import model.tower.type.EmptyTower;
import model.unit.UnitState;
import model.unit.enemyunit.EnemyUnit;
import model.unit.enemyunit.Wave;
import model.unit.towerunit.TowerUnit;
import controller.GameConfig;

/**
 * This class is the lowest-level model class, containing instances of Map, EnemyUnit, etc.
 * It also has game-wide information like number of lives left and resource amount.
 * @author Nathan
 */

public class Game extends Observable implements Serializable {
	private static final long serialVersionUID = -348846910264044765L;
	private transient Map map;
	private Tower[] towers;
	private List<EnemyUnit> enemies;
	private LinkedList<Wave> waves;
	private boolean lost, won;
	private int energy, lives, level;
	private transient Tower selectedTower = null;
	private String message = "";
	private String mapID;
	private String playerName;
	private Cooldown resourceGenCooldown;
	private transient int gameSpeed;
	// This value is updated by engineer towers
	private int resourceGenAmount = 1;
	private double resourceGenRate = 1.5;
	
	public Game(Map map){
		enemies = new Vector<EnemyUnit>();
		towers = map.getTowerSpaces();
		waves = new LinkedList<Wave>();
		mapID = map.getMapID();
		lives = GameConfig.LIVES;
		level = 0;
		energy = GameConfig.STARTING_ENERGY;
		this.map = map;
		lost = won = false;
		resourceGenCooldown = new Cooldown(resourceGenRate);
		gameSpeed = GameConfig.gameSpeed;
	}
	
	/**
	 * I ran into SOOOOO many issues serializing the Game object, especially when it is being modified concurrently by multiple different threads
	 * in multiplayer, so this method creates a deep clone (i.e. all instance variables are also cloned, not copied by reference) of the entire Game.
	 * This copy won't be messed with by other threads so it is serialized and sent properly.
	 * @return A perfect deep clone of this Game
	 */
	public Game deepClone(){
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(this);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return (Game) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Merges a different gamestate into the current one. Specifically, it takes all enemies, towers, units, lives, and resources from the other game
	 * and overwrites all of them on the current game while leaving stuff like the background image and path alone. This allows us to make those things
	 * transient and reduce the weight of the Game object as it is sent over the network.
	 * @param other the Game
	 */
	public void merge(Game other){
		setLives(other.getLives());
		setEnergy(other.getEnergy());
		setTowers(other.getTowers());
		setEnemies(other.getEnemies());
		setWaves(other.getWaves());
		setLevel(other.getLevel());
		setChanged();
		notifyObservers();
	}
	
	/**
	 * Updates the state of the model based on a change in time. This is the central function called by the main game loop in PlayGame, and it spiderwebs out
	 * a bunch of calls to update every tower, friendly unit, and enemy unit as well as doing a bunch of checks like health and range.
	 * @param deltaTime The change in time
	 */
	public void update(double deltaTime) {
		deltaTime = deltaTime * gameSpeed;
		if( lost || won) return;
		spawnEnemies(deltaTime);
		checkTowerRange();
		updateTowers(deltaTime);
		updateEnemies(deltaTime);
		checkHealth(deltaTime);
		// Increase energy
		if(resourceGenCooldown.getCurrent() > 0)
			resourceGenCooldown.decrease(deltaTime);
		else{
			addEnergy(resourceGenAmount);
			resourceGenCooldown.reset();
		}
		setChanged();
		notifyObservers();
	}

	/**
	 * Spawns enemies, and handles their spawn delays
	 * @param deltaTime the time delta
	 */
	public void spawnEnemies(double deltaTime){
		if(waves.isEmpty()){
			if(enemies.isEmpty()) win();
			return;
		}
		Wave wave = waves.peek();
		if(wave.getWaveDelay().getCurrent() > 0) // Still waiting to spawn
			wave.getWaveDelay().decrease(deltaTime);
		else { // Ready to spawn
			if(! wave.hasNext()){ // No more enemies in current wave
				waves.pop();
			} else { // Enemies left to spawn
				if(wave.getNextSpawnDelay().getCurrent() > 0)
					wave.getNextSpawnDelay().decrease(deltaTime);
				else {
					EnemyUnit enemy = wave.next();
					enemy.setPosition(enemy.getPath().get(0));
					enemies.add(enemy);
				}
			}
		}
	}

	/**
	 * Loops through every enemy in the game and calls update() on them
	 * @param deltaTime The change in time
	 */
	public void updateEnemies(double deltaTime){
		for(EnemyUnit enemy : enemies)
			enemy.update(deltaTime);
	}

	/**
	 * Loops through every tower in the game and calls update() on them
	 * @param deltaTime The change in time
	 */
	public void updateTowers(double deltaTime){
		for(Tower tower : towers) {
			tower.update(deltaTime,this);
		}
	}

	/**
	 * Checks the health of all enemies and Defenders to see if they have died yet.
	 * Also checks for enemies at the end of the path, removes them so the game doesn't crash, and removes a life.
	 * If left with zero lives, calls lose() to end the game.
	 * @param deltaTime The delta time
	 */
	public void checkHealth(double deltaTime){
		// Base/Game health
		for(EnemyUnit enemy : enemies){
			double displacement = deltaTime * enemy.getMoveSpeed();
			int curIndex = enemy.getPath().indexOf(enemy.getPosition());
			if((int) (Math.round(curIndex + displacement)) > enemy.getPath().size() - 1){
				enemy.setHealth(0); // Remove enemy (by killing them)
				enemy.getSprite().setState(UnitState.DEAD);
				loseLife();
				if(getLives() <= 0)
					lose();
			}
		}
		// Enemies
		for(EnemyUnit enemy : enemies.toArray(new EnemyUnit[enemies.size()])) {
			if(enemy.getSprite().getState() == UnitState.DEAD) {
				enemies.remove(enemy);
			}
		}
		// Allies
		for(Tower tower : getTowers())
			for(TowerUnit unit : tower.getUnits().toArray(new TowerUnit[tower.getUnits().size()]))
				if(unit.getSprite().getState() == UnitState.DEAD){
					tower.removeUnit(unit);
					if(tower instanceof DefenderTower) {
						tower.addXp(unit.getPower()*2);
						((DefenderTower) tower).setDefenderOrigins();
					}
				}	
	}

	/**
	 * Checks the range of towers and handles their assignment of targets to units.
	 */
	public void checkTowerRange() {
		for(Tower tower : towers){
			if(tower instanceof EmptyTower) continue;
			enemyLoop: for(EnemyUnit enemy : enemies){
				if(enemy.getPosition().distance(tower.getCenter()) <= tower.getRange()){
					for(TowerUnit unit : tower.getUnits()){
						if(unit.hasTarget() || (tower instanceof DefenderTower && enemy.hasTarget())) continue;
						unit.setTarget(enemy);
						if(tower instanceof DefenderTower) // Prevent enemies from acquiring riflemen as targets
							enemy.setTarget(unit);
						continue enemyLoop;
					}
				}
			}
		}
	}

	/**** GETTERS and SETTERS ****/
	public Map getMap(){
		return map;
	}

	public List<EnemyUnit> getEnemies(){
		return enemies;
	}
	
	public void setEnemies(List<EnemyUnit> enemies){
		this.enemies = enemies;
	}

	public boolean lost(){
		return lost;
	}

	public boolean won(){
		return won;
	}

	public void win(){
		won = true;
	}

	public void lose(){
		lost = true;
	}

	/**
	 * Prints a bunch of information about the game for debug purposes
	 * @return Information for debugging
	 */
	public String debug() {
		String output="TOWER DEFENSE GAME";
		output+="\n- Lives: "+lives;
		output+="\n- Energy: "+energy;
		output+="\n- Towers: {";
		for(Tower tower : towers)
			output+="\n"+tower.debug();
		output+="\n}";
		output+="\n - Enemies ("+enemies.size()+"): {";
		for(EnemyUnit enemy : enemies)
			output+="\n"+enemy.debug();
		output+="\n}";
		return output;
	}

	public int getEnergy(){
		return energy;
	}

	public void setEnergy(int energy){
		this.energy = energy;
	}

	public int getLives(){
		return lives;
	}

	public void loseLife(){
		lives--;
	}
	
	public void setLives(int lives){
		this.lives = lives;
	}

	public int getLevel(){
		return level;
	}
	
	public void setLevel(int level){
		this.level = level;
	}

	public void levelUp(){
		level++;
	}

	public void addEnergy(int toAdd) {
		energy += toAdd;
	}

	public void removeEnergy(int toRemove) {
		energy -= toRemove;
	}

	public LinkedList<Wave> getWaves(){
		return waves;
	}
	
	public void setWaves(LinkedList<Wave> waves){
		this.waves = waves;
	}

	public void addWave(Wave wave){
		waves.add(wave);
	}
	
	public Tower[] getTowers(){
		return towers;
	}

	public Tower getTower(int i){
		return towers[i];
	}

	public void setTower(int i, Tower tower){
		towers[i] = tower;
	}
	
	public void setTowers(Tower[] towers){
		this.towers = towers;
	}

	public Tower getSelectedTower() {
		return selectedTower;
	}

	public void setSelectedTower(Tower selectedTower) {
		this.selectedTower = selectedTower;
		setChanged();
		notifyObservers();
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String string) {
		this.message = string;
		setChanged();
		notifyObservers();
	}
	
	public String getMapID(){
		return mapID;
	}

	public int getResourceGenAmount() {
		return resourceGenAmount;
	}

	public void setResourceGenAmount(int resourceGenAmount) {
		this.resourceGenAmount = resourceGenAmount;
	}

	public double getResourceGenRate() {
		return resourceGenRate;
	}

	public void setResourceGenRate(double resourceGenRate) {
		this.resourceGenRate = resourceGenRate;
	}

	public int getGameSpeed() {
		return gameSpeed;
	}

	public void setGameSpeed(int d) {
		this.gameSpeed = d;
	}

	public void togglePaused() {
		if(gameSpeed == 0) gameSpeed = GameConfig.gameSpeed;
		else gameSpeed = 0;
	}

	public boolean paused() {
		return gameSpeed == 0;
	}
	
	public void setPlayerName(String name){
		playerName = name;
	}
	
	public String getPlayerName(){
		return playerName;
	}

	public void setMap(Map map2) {
		map = map2;	
	}	
}
