package model.map;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.geom.Point2D;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 * A custom visualization of the pathfinding engine at work. Useful when debugging new maps
 * @author Nathan
 *
 */
@SuppressWarnings("serial")
public class DebugPathFinding extends JFrame{
	Map map;
	Timer timer;
	int time;

	public DebugPathFinding(Map map){
		this.map = map;

		add(new MapPanel());

		setVisible(true);
		setSize(1100,1100);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		time = 0;
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask(){public void run() {
			time++;
			repaint();
		}}, 0, 33);
	}

	public class MapPanel extends JPanel {
		public MapPanel(){
			setVisible(true);
		}

		@Override
		public void paintComponent(Graphics g){
			// Draw map image
			g.drawImage(map.getBackgroundImage(), 0, 0, this);
			for(Path path : map.getPaths()){
				// Trace boundaryA
				g.setColor(new Color(0, 0, 255));
				for(Point2D p : path.getBoundaryA())
					g.fillRect((int) p.getX(), (int) p.getY(), 1, 1);
				// Trace boundaryB
				g.setColor(new Color(0, 0, 255));
				for(Point2D p : path.getBoundaryB())
					g.fillRect((int) p.getX(), (int) p.getY(), 1, 1);
				// Draw control points
				g.setColor(new Color(0, 255, 0));
				for(Point2D p : path.getControlPoints())
					g.fillOval((int) p.getX() - 2, (int) p.getY() - 2, 5, 5);
				// Draw path
				g.setColor(new Color(255, 0, 0));
				for(Point2D p : path.getPath())
					g.fillRect((int) p.getX(), (int) p.getY(), 1, 1);
				// Draw sample enemy
				g.setColor(new Color(255, 128, 0));
				if(time < path.getPath().size())
					g.fillOval((int) path.getPath().get(time).getX()-15, (int) path.getPath().get(time).getY()-15, 30, 30);
			}
		}
	}
	
	static public void main(String[] args){
		new DebugPathFinding(new Map("BiggerMap"));
	}
}
