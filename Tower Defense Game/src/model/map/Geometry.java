package model.map;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A collection of static methods useful in geometric calculations. Used throughout the game for pathfinding, unit movement, etc.
 * @author Nathan
 */

public final class Geometry {
	/**
	 * Creates a linear approximation of the derivative of a curve.
	 * @param curve The curve to find the tangent of, passed in as a list of discrete points.
	 * @param i The point (index of the curve array) to find the tangent vector at.
	 * @return A {@link model.map.Vector2D} object tangent to the curve at point i
	 */
	public static Vector2D tangentVector(List<Point2D> curve, int i){
		// Pick sample points
		Point2D before, after;
		if(i - 2 < 0){
			before = curve.get(0);
			after = curve.get(4);
		} else if(i + 2 >= curve.size()) {
			before = curve.get(curve.size() - 5);
			after = curve.get(curve.size() - 1);
		} else {
			before = curve.get(i-2);
			after = curve.get(i+2);
		}

		return new Vector2D(before, after);
	}

	/**
	 * Finds all points adjacent to the given point. Expects a point with integer coordinates, and finds the points directly above, below, left, and right of it.
	 * @param point The point to find adjacent points around
	 * @return A collection of all the points adjacent to the given point
	 */
	public static Collection<Point> adjacentPoints(Point2D point){
		Collection<Point> points = new ArrayList<Point>();
		int x = (int) point.getX();
		int y = (int) point.getY();
		points.add(new Point(x+1, y));
		points.add(new Point(x-1, y));
		points.add(new Point(x, y+1));
		points.add(new Point(x, y-1));
		return points;
	}

	/**
	 * Finds all points adjacent to or diagonal from the given point. Same as {@link #adjacentPoints(Point2D)} but adds in the diagonals.
	 * @param point The point to find nearby points around
	 * @return A collection of all the points nearby the given point
	 */
	public static Collection<Point> nearbyPoints(Point2D point){
		Collection<Point> points = new ArrayList<Point>();
		int x = (int) point.getX();
		int y = (int) point.getY();
		// Adjacent
		points.addAll(adjacentPoints(point));
		// Diagonals		
		points.add(new Point(x+1, y+1));
		points.add(new Point(x+1, y-1));
		points.add(new Point(x-1, y+1));
		points.add(new Point(x-1, y-1));
		return points;
	}
	
	/**
	 * Given an origin, finds points evenly distributed around that point. This implementation uses points
	 * on the circumference of a circle, which works well for 1-4 points, but a different implementation
	 * (like a square or hexagonal grid) would be better for a larger number of points. This may need to
	 * be changed if we need to distribute > 4 points around a single origin.
	 * 
	 * Used for origin points of Defender units, possibly other things in the future.
	 * @param origin The origin point to distribute around.
	 * @param numberOfPoints Number of points to create (the size of the return list)
	 * @param minDistance The minimum distance each point can be to another
	 * @return An ArrayList of points evenly distributed around the origin point
	 */
	public static List<Point2D> distributePoints(Point2D origin, int numberOfPoints, double minDistance){
		// Rename some variables for my own convenience
		int n = numberOfPoints;
		Point2D o = origin;
		double d = minDistance;
		
		List<Point2D> distributed = new ArrayList<Point2D>();
		
		// Handle base case
		if(n == 1){
			distributed.add(o);
			return distributed;
		}
		
		double theta = (2 * Math.PI) / n;
		double deltaX = d/2.0;
		double deltaY = deltaX / Math.tan(theta / 2.0);
		
		Point2D p = new Point2D.Double(o.getX() + deltaX, o.getY() + deltaY);
		Vector2D translation = new Vector2D(o, p);

		for(int i = 0; i < n; i++){
			translation.rotate(theta);
			p = new Point2D.Double(o.getX() + translation.getDeltaX(), o.getY() - translation.getDeltaY() - d/2);
			distributed.add(p);
		}
		
		return distributed;
	}
}
