package model.map;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import model.tower.Tower;
import model.tower.type.EmptyTower;

/**
 * Contains a list of Towers and Paths, as well as a few other view-related items such as backgroundImage and selectedTower.
 * @author Nathan
 */

public class Map implements Serializable {
	private static final long serialVersionUID = 1L;
	private transient BufferedImage backgroundImage;	
	private String mapID;
	private Tower[] towerSpaces;
	private List<Path> paths;
	
	/**
	 * Default constructor assumes one path
	 * @param mapID
	 */
	public Map(String mapID) {
		this(mapID, 1);
	}

	/**
	 * The more specific constructor called by the default one, specifying a specific number of paths.
	 * @param mapID
	 * @param numPaths
	 */
	public Map(String mapID, int numPaths){
		this.mapID = mapID;
		findBackgroundImage();

		paths = new ArrayList<Path>();
		for(int i=0; i<numPaths; i++){
			paths.add(new Path("maps/" + mapID + "_pathmap"+(i==0?"":""+(i+1) )+".bmp"));
		}
		
		towerSpaces = generateTowers();
	}

	/**
	 * Scans the pathmap for blue squares ( rgb(0, 0, 255) ) which represent tower spaces and initializes an array of empty towers.
	 */
	public Tower[] generateTowers(){
		ArrayList<Tower> towers = new ArrayList<Tower>();
		BufferedImage pathMap = getPathMap();
		for(int y=0; y < pathMap.getHeight(); y++){
			searching: for(int x=0; x < pathMap.getWidth(); x++){
				Color color = new Color(pathMap.getRGB(x, y));
				if(color.equals(Color.BLUE)){
					for(Tower tower : towers){
						if(tower.containsPoint(new Point(x, y)))
							continue searching;
					}
					towers.add(new EmptyTower(new Point(x, y)));
					x += towers.get(towers.size() - 1).getWidth() - 1;
				}
			}
		}
		return towers.toArray(new Tower[towers.size()]);
	}

	/**** GETTERS & SETTERS ****/
	public void findBackgroundImage(){
		try {
			backgroundImage = ImageIO.read(getClass().getResource("/maps/" + mapID + "_background.png"));
		} catch (Exception e) {
//			e.printStackTrace();
		}
	}
	
	public void setBackgroundImage(BufferedImage image) {
		this.backgroundImage = image;
	}

	public BufferedImage getBackgroundImage() {
		return backgroundImage;
	}

	/**
	 * When getPath() is called with no parameters, assume we want the first path (legacy support)
	 * @return The first path (== getPath(0) == paths.get(0))
	 */
	public Path getPath(){
		return getPath(0);
	}

	public Path getPath(int i){
		return paths.get(i);
	}

	public List<Path> getPaths(){
		return paths;
	}

	/**
	 * A helper function which returns the closest point on any path on the map to a given point. Used in setting DefenderTower spawn point
	 * @param from
	 * @return The closest point on any path
	 */
	public Point2D closestPointOnAnyPath(Point2D from){
		Point2D closestP = null;
		double minDist = Double.POSITIVE_INFINITY;
		for(Path path : getPaths()){
			double dist = path.closestPointOnPath(from).distance(from);
			if(dist < minDist){
				minDist = path.closestPointOnPath(from).distance(from);
				closestP = path.closestPointOnPath(from);
			}
		}

		if(closestP == null) // Theoretically this could never happen
			System.out.println("Could not find closest point.");

		return closestP;
	}

	/**
	 * The first pathmap will be the primary one, i.e. the one with tower locations defined, etc, so return this one
	 * @return The first pathMap
	 */
	public BufferedImage getPathMap(){
		return paths.get(0).getPathMap();
	}

	public String getMapID() {
		return mapID;
	}

	public void setMapID(String mapID) {
		this.mapID = mapID;
	}
	
	public Tower[] getTowerSpaces(){
		return towerSpaces;
	}
}
