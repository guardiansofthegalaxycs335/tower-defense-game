package model.map;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;

/**
 * The pathfinding engine of the game. Map contains one or more instances of Path.
 * @author Nathan
 */

public class Path implements Serializable{
	private static final long serialVersionUID = -8530563675245888103L;
	private transient BufferedImage pathMap;
	private int pathWidth;
	// TODO Refactor this to static array rather than a dynamic linked list for performance (not super important)
	private List<Point2D> boundaryA, boundaryB, controlPoints, path;
	private Spline2D spline;
	private int pollingInterval = 1; //pathWidth / 2; // How often to choose control points

	/**
	 * Constructor
	 * @param pathMap The filesystem path to the pathmap file which will be used to define this path
	 */
	public Path(String pathMap){
		try {
			this.pathMap = ImageIO.read(getClass().getResource("/"+pathMap));
		} catch (Exception e) {
			e.printStackTrace();
		}

		path = generatePath();
	}

	/**
	 * The central function of the Path class -- this is the heart of our pathfinding engine.
	 * <ol>
	 * 		<li> Finds the start points of the path boundaries and calculate the pathwidth
	 * 		<li> Calls traceBoundary for each side of the path
	 * 		<li> Calls generateControlPoints
	 * 		<li> Forms a spline through said control points
	 * 		<li> Finally returns a list of discrete points on that spline.
	 * </ol>
	 * @return A discrete list of points defining the path for enemies to travel
	 */
	private List<Point2D> generatePath(){
		// Step 1: Find path boundary starting points
		Point boundaryAStart = null;
		Point boundaryBStart = null;

		// Create a flat 1D array of all border pixels that we will search for the start and end points of the path
		// This 1D implementation allows corners to work
		ArrayList<Point> borders = new ArrayList<Point>();
		for(int x=0; x < pathMap.getWidth() - 1; x++)
			borders.add(new Point(x, 0));
		for(int y=1; y < pathMap.getHeight() - 1; y++)
			borders.add(new Point(pathMap.getWidth() - 1, y));
		for(int x=pathMap.getWidth() - 2; x >= 0; x--)
			borders.add(new Point(x, pathMap.getHeight() - 1));
		for(int y = pathMap.getHeight() - 2; y >= 0; y--)
			borders.add(new Point(0, y));

		// Handle special case of upper-left corner
		int startIndex = 0;
		if(new Color(pathMap.getRGB(0, 0)).equals(Color.GREEN))
			startIndex = (int) (0.25 * borders.size());

		// Loop through border looking for green
		for(int i = startIndex + 1; i != startIndex && boundaryBStart == null; i++){
			if(i == borders.size()) // Wraparound
				i = 0;
			if(i == startIndex)
				break;
			int x = (int) borders.get(i).getX();
			int y = (int) borders.get(i).getY();
			Color color = new Color(pathMap.getRGB(x, y));
			if(color.equals(Color.GREEN))
				if(boundaryAStart == null)
					boundaryAStart = new Point(x, y);
				else
					continue;
			else
				if(boundaryAStart != null)
					if(i == 0)
						boundaryBStart = new Point((int) borders.get(borders.size()-1).getX(), (int) borders.get(borders.size()-1).getY());
					else
						boundaryBStart = new Point((int) borders.get(i-1).getX(), (int) borders.get(i-1).getY());
				else
					continue;
		}

		// Handle exceptions
		if(boundaryAStart == null || boundaryBStart == null){ // TODO Write exception for this error
			System.out.println("Map parsing error: cannot find start points for path boundary");
			return null;
		}

		// OPTIONAL: Infer path width rather than passing it in -- may be less accurate in some situations
		pathWidth = (int) boundaryAStart.distance(boundaryBStart) + 1;

		// Step 2: Trace boundaries
		boundaryA = traceBoundary(boundaryAStart);
		boundaryB = traceBoundary(boundaryBStart);

		// Step 3: Walk boundaryA and generate control points
		controlPoints = generateControlPoints(boundaryA);

		// Step 4: Generate spline through control points
		spline = new Spline2D(controlPoints.toArray(new Point[controlPoints.size()]));

		// Step 5: Interpolate points on the spline
		List<Point2D> path = new Vector<Point2D>();
		for(double i=0; i<spline.getLength(); i++){
			path.add(new Point2D.Double(spline.getPoint(i/spline.getLength())[0], spline.getPoint(i/spline.getLength())[1]));
		}

		return path;
	}

	/**
	 * Part of the logic for generatePath(). Given a starting point, it will trace the boundary of a path by checking all nearby points until it finds a
	 * match and then continuing. The output of this function becomes the input for generateControlPoints().
	 * @param start The starting point of the boundary, as computed and passed in by generatePath()
	 * @return A list of discrete points defining this boundary
	 */
	public List<Point2D> traceBoundary(Point start){
		List<Point2D> boundary = new Vector<Point2D>();
		boundary.add(start);
		boolean pointFound = true;
		while(pointFound){
			pointFound = false;
			Collection<Point> nearby = Geometry.nearbyPoints(boundary.get(boundary.size()-1));

			for(Point point : nearby){
				int x = (int) point.getX();
				int y = (int) point.getY();

				// Discard points off of the image boundaries
				if( x < 0 || x >= pathMap.getWidth() - 1 || y < 0 || y >= pathMap.getHeight() - 1 ) 
					continue;

				// Discard points that aren't white
				if(! new Color(pathMap.getRGB(x, y)).equals(new Color(255, 255, 255)))
					continue;

				// Discard points which are backtracking (already in path)
				if(boundary.contains(point))
					continue;

				// Check that the current point is adjacent to a black point
				Collection<Point> adjacent = Geometry.adjacentPoints(point);
				boolean blackAdjacent = false;
				for(Point adj : adjacent){
					try{
						Color pixelColor = new Color(pathMap.getRGB((int) adj.getX(), (int) adj.getY()));
						if(pixelColor.equals(new Color(0, 0 ,0)) || pixelColor.equals(new Color(0, 0, 255))){
							blackAdjacent = true;
							break;
						}
					} catch (ArrayIndexOutOfBoundsException e){
						// Silently ignore errors from checking adjacent pixels off of the image
					} catch (Exception e){
						e.printStackTrace();
					}

				}
				if(blackAdjacent){
					boundary.add(point);
					pointFound = true;
				}
			}
		}
		return boundary;
	}

	/**
	 * Part of the logic for generatePath()
	 * <ol>
	 * 		<li> Walks along a boundary, stopping every pollingInterval
	 * 		<li> Finds a vector tangent to the boundary at each point
	 * 		<li> Rotates that vector 90 degrees clockwise and sets its magnitude to half the pathWidth, then recomputes the endpoint
	 * 		<li> Creates a new point defined by the boundary point and the newly computed translation vector and adds it to the list
	 * </ol>
	 * 
	 * The output of this function will be the input for the spline.
	 * 
	 * <p>Notes:
	 * <ul>
	 * 		<li> In this implementation, the boundary passed in MUST BE the left boundary (if the path start is at the top)
	 * 		<li> If the path is too choppy or not accurate enough, try changing the pollingInterval before calling this
	 * </ul>
	 * @param boundary The boundary to walk along, as computed by traceBoundary()
	 * @return A list of discrete control points for the spline
	 */
	private List<Point2D> generateControlPoints(List<Point2D> boundary) {
		List<Point2D> controlPoints = new Vector<Point2D>();
		for(int i=0; i < boundary.size(); i+= pollingInterval){ 
			double radius = pathWidth / 2.0;
			int x = (int) boundary.get(i).getX();
			int y = (int) boundary.get(i).getY();
			double theta = Geometry.tangentVector(boundary, i).getDirection();

			// Calculate displacement to control point
			double deltaX_c = radius * Math.cos(theta);
			double deltaY_c = radius * Math.sin(theta);
			int controlX = (int) (x + deltaX_c);
			int controlY = (int) (y - deltaY_c);
			controlPoints.add(new Point(controlX, controlY));

			// Double check the surrounding boundary points to make sure it's not too close anywhere
			// Necessary for drawing correct right-angled paths
			for(int j=0; j < pathWidth; j++){
				// Check forwards
				try{
					if(controlPoints.get(controlPoints.size() - 1).distance(boundary.get(i+j)) < 0.9 * radius){
						for(int h=(int) (radius / pollingInterval); h >= 0; h-- )
							if(controlPoints.get(controlPoints.size() - 1 - h).distance(controlPoints.get(controlPoints.size() - 1)) <= radius)
								controlPoints.remove(controlPoints.size() - 1 - h);
						i += radius * 3 - pollingInterval;
						break;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					// Silently ignore
				}
			}
		}
		return controlPoints;
	}
	
	/**
	 * A helper function used in pathfinding logic and elsewhere in the project. Finds the closest point on the path given any arbitrary point.
	 * Gives preference to points closer to the end of the path (for the benefit of enemy pathfinding)
	 * @param from The starting point
	 * @return The closest point on the path
	 */
	public Point2D closestPointOnPath(Point2D from){
		Point2D closestP = null;
		double minDist = Double.POSITIVE_INFINITY;
		for(int x = path.size() - 1; x >= 0; x--){
			double dist = path.get(x).distance(from);
			if(dist < minDist){
				minDist = path.get(x).distance(from);
				closestP = path.get(x);
			}
		}

		if(closestP == null) // Theoretically this could never happen
			System.out.println("Could not find closest point.");

		return closestP;
	}

	/**** GETTERS & SETTERS ****/
	// Pathfinding
	public int getPathWidth(){
		return pathWidth;
	}

	public BufferedImage getPathMap(){
		return pathMap;
	}

	public List<Point2D> getBoundaryA(){
		return boundaryA;
	}

	public List<Point2D> getBoundaryB(){
		return boundaryB;
	}

	public List<Point2D> getControlPoints(){
		return controlPoints;
	}

	public List<Point2D> getPath(){
		return path;
	}

	public List<Point2D> getDiscretePoints(){
		return path;
	}

	public Spline2D getSpline(){
		return spline;
	}

	public int getPollingInterval(){
		return pollingInterval;
	}

	public void setPollingInterval(int interval){
		pollingInterval = interval;
	}

	/**** COLLECTION METHODS ****/

	/*
	 * Allow treating the class Path like it's the instance variable path for two reasons:
	 * 1) legacy support for single-path based coding, and
	 * 2) simpler/more concise access to the useful information contained in this class
	 */

	public Point2D get(int i){
		return path.get(i);
	}

	public int indexOf(Point2D p){
		return path.indexOf(p);
	}

	public boolean contains(Point2D p){
		return path.contains(p);
	}

	public int size(){
		return path.size();
	}

}
