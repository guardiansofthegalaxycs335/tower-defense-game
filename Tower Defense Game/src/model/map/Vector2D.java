package model.map;

import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * A class for representing vectors in 2D space, defined by a start and endpoint.
 * Contains methods to calculate magnitude, direction, etc, as well as other manipulations.
 * @author Nathan
 */

public class Vector2D {
	private Point2D a, b;
	
	/**** CONSTRUCTORS ****/
	
	/**
	 * Basic constructor defining a vector via two points
	 * @param a Start point
	 * @param b End point
	 */
	public Vector2D(Point2D a, Point2D b){
		this.a = a;
		this.b = b;
	}
	
	/**
	 * Alternate constructor for when a start point, direction, and magnitude are known (calculates endpoint)
	 * @param a Start point
	 * @param direction
	 * @param magnitude
	 */
	public Vector2D(Point2D a, double direction, double magnitude){
		this.a = a;
		double deltaX = magnitude * Math.sin(direction);
		double deltaY = magnitude * Math.cos(direction);
		b = new Point2D.Double(a.getX() + deltaX, a.getY() + deltaY);
	}
	
	/**** COMPARISONS ****/
	
	/**
	 * Basic equals() method, returns true if the two vectors have the same magnitude and direction, ignoring their relative positions
	 * @param other The vector to compare to
	 * @return True if the vectors are equal
	 */
	public boolean equals(Vector2D other){
		return other.getDirection() == getDirection() && other.getMagnitude() == getMagnitude();
	}
	
	
	/**
	 * Checks perpendicularity of two vectors, regardless of magnitude and position
	 * @param other
	 * @return True if the vectors are perpendicular
	 */
	public boolean perpendicularTo(Vector2D other){
		if(getMagnitude() == 0 || other.getMagnitude() == 0) // Point vectors (0 magnitude) will always return perpendicular to any other vector
			return true;
		int direction = (int) Math.round(Math.toDegrees(getDirection()));
		int otherDirection = (int) Math.round(Math.toDegrees(other.getDirection()));
		return otherDirection == direction + 90 || otherDirection == direction - 90;
	}
	
	/**** TRANSFORMATIONS ****/
	
	/**
	 * Adds one vector to another, tip-to-tail and returns the new combined vector
	 * @param other The vector to add to this one
	 * @return A new vector which is the sum of the current instance and other
	 */
	public Vector2D add(Vector2D other){
		return new Vector2D(a, new Point2D.Double(b.getX() + other.getDeltaX(), b.getY() + other.getDeltaY()));
	}
	
	// TODO
	public void translate(int deltaX, int deltaY){}
	public void scale(double scalar){}
	
	/**** GETTERS ****/
	
	public Point2D getA(){
		return a;
	}
	
	public Point2D getB(){
		return b;
	}
	
	public double getDeltaX(){
		return b.getX() - a.getX();
	}
	
	public double getDeltaY(){
		return b.getY() - a.getY();
	}
	
	/**
	 * Computes the direction of the vector via trigonometry
	 * @return Direction of the vector
	 */
	public double getDirection(){
		double deltaX = getDeltaX();
		double deltaY = getDeltaY();
		double theta = Math.atan(deltaX/deltaY);

		if(deltaX >= 0 && deltaY < 0)
			theta += Math.toRadians(180);
		else if(deltaX <= 0 && deltaY < 0)
			theta += Math.toRadians(180);
		
		return theta;
	}
	
	public double getMagnitude(){
		return a.distance(b);
	}
	
	/**** SETTERS ****/
	public void setA(Point a){
		this.a = a;
	}
	
	public void setB(Point b){
		this.b = b;
	}
	
	/**
	 * Sets the direction of the vector without changing the start point or magnitude, and recalculates the endpoint
	 * @param direction
	 */
	public void setDirection(double direction){
		double magnitude = getMagnitude();
		double deltaX = magnitude * Math.sin(direction);
		double deltaY = magnitude * Math.cos(direction);
		b = new Point2D.Double(a.getX() + deltaX, a.getY() + deltaY);
	}
	
	/**
	 * Sets the magnitude of the vector without changing the start point or direction, and recalculates the endpoint
	 * @param magnitude
	 */
	public void setMagnitude(double magnitude){
		double direction = getDirection();
		double deltaX = magnitude * Math.sin(direction);
		double deltaY = magnitude * Math.cos(direction);
		b = new Point2D.Double(a.getX() + deltaX, a.getY() + deltaY);
	}
	
	/**
	 * Rotates a vector by the given angle
	 * @param delta Angle to rotate by, IN RADIANS
	 */
	public void rotate(double delta){ // Delta must be in radians
		setDirection(getDirection() + delta);
	}
}
