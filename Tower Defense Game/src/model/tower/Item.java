package model.tower;

import java.io.Serializable;

import model.unit.Unit;

public abstract class Item implements Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	private int energyCost = 0,levelCost = 0;
	private Class<? extends Tower> applicableTower;

	/**
	 * This class does all the work for Item. It applies the effects to the given unit
	 * @param unit
	 */
	public abstract void applyEffect(Unit unit);
	
	/**
	 * Override this method to apply an effect to a tower, rather than a unit
	 * @param tower
	 */
	public void applyEffectToTower(Tower tower) {
		// nothing default
	}
	
	/**
	 * Override this method to apply an effect to a tower through the game update
	 * @param tower
	 */
	public void applyEffectOnUpdate(Tower tower) {
		// nothing default
	}

	/**
	 * Override this method to apply an effect that happens whenever a unit levels
	 * @param unit
	 */
	public void applyLevelUpEffect(Unit unit) {
		// nothing default
	}
	
	/**
	 * Constructor for Item
	 * Sets description to default value
	 */
	public Item () {
		setDescription("\n" + "Name: " + this.name + "\n"
				+ "Energy Cost: " + this.energyCost
				+ "Level Cost: " + this.levelCost
				);
		setApplicableTower(Tower.class);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		Tower tower = null;
		try {
			tower = applicableTower.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getName() + " - " + tower;
	}
	
	public String getDescription(){
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Class<? extends Tower> getApplicableTower() {
		return applicableTower;
	}

	public void setApplicableTower(Class<? extends Tower> class1) {
		this.applicableTower = class1;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj.getClass().equals( getClass());
	}

	public int getLevelCost() {
		return levelCost;
	}

	public void setLevelCost(int levelCost) {
		this.levelCost = levelCost;
	}

	public int getEnergyCost() {
		return energyCost;
	}

	public void setEnergyCost(int energyCost) {
		this.energyCost = energyCost;
	}

}