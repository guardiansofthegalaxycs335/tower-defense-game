package model.tower;

import java.awt.Image;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;

import model.Game;
import model.map.Map;
import model.tower.item.SpecializationItem;
import model.unit.Unit;
import model.unit.towerunit.TowerUnit;

/**
 * An abstract Tower class, encapsulating all the common functionality and defining a few abstract methods to require 
 */

public abstract class Tower implements Serializable{
	private static final long serialVersionUID = -5740263515290495134L;
	private int xp, level, xpNeededtoLevel, range, maxUnits, cost;
	private boolean justLevelled;
	private transient Image image = null, hoverImage = null;
	private List<TowerUnit> units;
	private List<Item> items;
	private Point2D topLeft;
	private Map map;
	private String imageUrl;
	private String hoverImageUrl;
	
	public Tower cloneTower(){
		Tower clone = null;
		Constructor<? extends Tower> constructor = null;
		try {
			constructor = this.getClass().getConstructor();
			clone = constructor.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		clone.setTopLeft(getTopLeft());
		clone.setXp(getXp());
		clone.setLevel(getLevel());
		clone.setRange(getRange());
		clone.setMaxUnits(getMaxUnits());
		clone.setCost(getCost());
		clone.setXpNeededtoLevel();
		for(TowerUnit unit : units)
			clone.addUnit(unit);
		if(items != null)
			for(Item item : items)
				clone.addItem(item); // TODO Deep copy?
		clone.setMap(getMap());
		return clone;
	}
	
	/*
	 * Abstract methods
	 * createNew() instantiates a new tower class and returns it
	 */
	
	public abstract Tower createNew(Map map, Point2D point);
	
	/**
	 * The core update function. It is passed the map and the displacement, and runs its updates accordingly
	 * It is overridden in the subtowers
	 * @param deltaTime
	 * @param game
	 */
	public void update(double deltaTime, Game game) {
		checkLevelUp();
	}

	/**
	 * A constructor without a position defined is required for listing abstract information about towers in the GUI.
	 */
	public Tower(){
		units = new Vector<TowerUnit>();
		items = new Vector<Item>();
		xp = 0;
		level = 1;
		justLevelled = false;
		maxUnits = 4;
	}
	
	public Tower (Point2D topLeft) {
		this();
		this.topLeft = topLeft;
	}
	

	/**
	 * Checks if the tower can apply the given item
	 * @param item The item to check
	 * @return
	 */
	public boolean canHaveItem(Item item) {
		if(item.getApplicableTower().isInstance(this) 
				&& !items.contains(item)) {
			// There can only be one specialization item
			if (item instanceof SpecializationItem)
				for(Item i : items)
					// If there is another specialization item...
					if(!i.equals(item) && i instanceof SpecializationItem)
						return false;
			return true;
		}
		return false;
	}
	
	/**
	 * Prints debug info about the tower
	 * @return Debug info
	 */
	public String debug(){
		String output = "     "+this;
		output+="\n      - Top Left: "+topLeft;
		output+="\n      - Range: "+range;
		output+="\n      - Level: "+level;
		output+="\n      - XP: "+xp;
		output+="\n      - Max Units: "+maxUnits;
		output+="\n      - Units ("+units.size()+") {";
		for(TowerUnit unit : units)
			output+="\n"+unit.debug();
		output+="\n      }";
		return output;
	}
	
	/**
	 * Checks if a certain point is inside a tower object, based on a defined position, width, and height
	 */
	public boolean containsPoint(Point point){
		if(point.getX() 
				>= topLeft.getX() 
				&& point.getX() 
				< topLeft.getX() 
				+ getWidth())
			if(point.getY() >= topLeft.getY() && point.getY() < topLeft.getY() + getHeight())
				return true;
		return false;
	}
	
	public void checkLevelUp() {
		if (getXp() > getXpNeededtoLevel()) {
			int spillXp = getXp()-getXpNeededtoLevel();
			//int tempLevel = getLevel();
			levelUp();
			setJustLevelled(true);
			setXp(spillXp);
			setXpNeededtoLevel();
			setUnitsLevelUp();
		}
	}
	
	public void setUnitsLevelUp () {
		if (getJustLevelled() == true) {
			for(TowerUnit unit : getUnits()) {
				for(Item item : getItems()) {
					item.applyLevelUpEffect(unit);
				}
				unit.levelUp();
			}
		}
	}
	
	/**
	 * Applies all this tower's item effects on the given unit
	 * @param unit
	 */
	public void applyItems(Unit unit) {
		// Loop through each item, and apply it to each unit
		for(Item item : items) {
			item.applyEffect(unit);
		}
	}
	
	
	/**** GETTERS & SETTERS ****/
	/**
	 * Prints helpful information about the tower
	 */
	public String getDescription() {
		String output = null;
		String itemList = "";
		for(Item item : getItems()) {
			itemList += " + " + item.getDescription() + "\n";
		}
		boolean name = false;
		for (Item item : getItems()) {
			if (item.getName().equals("Guardian's Shield (10L) [SPECIALIZATION]")) {
				output = "Tower Name: " + "Guardian Tower";
				name = true;
			}
			if (item.getName().equals("Berserker's Axe (10L) [SPECIALIZATION]")) {
				output = "Tower Name: " + "Berserker Tower";
				name = true;
			}
			if (item.getName().equals("Empress' Scepter (10L) [SPECIALIZATION]")) {
				output = "Tower Name: " + "Empress Tower";
				name = true;
			}
			if (item.getName().equals("Outcast's Tome (10L) [SPECIALIZATION]")) {
				output = "Tower Name: " + "Outcast Tower";
				name = true;
			}
			if (item.getName().equals("Agent's Submachine Gun (10L) [SPECIALIZATION]")) {
				output = "Tower Name: " + "Agent Tower";
				name = true;
			}
			if (item.getName().equals("Marksman's Sniper Rifle (10L) [SPECIALIZATION]")) {
				output = "Tower Name: " + "Marksman Tower";
				name = true;
			}
				
		}
		if (name == false) {
		output = "Tower Name: " + toString();
		}
		output+="\nLevel: " + getLevel();
		output+="\nMax Units: " +getMaxUnits();
		output+="\nUnits: " + getUnits().size();
		output+="\nModifiers: " + itemList;
		output+="\nCost: " + getCost();
		if(getUnits().size() > 0) {
			TowerUnit unit = getUnits().get(0);
			output+="\nUnit information: " + unit.getName() + "\n";
			//output+="\nDamages: ";
			//int totalDamageAmount = 0;
			//for(Damage dmg : unit.getAttackDamages()) {
			//	if(dmg instanceof BasicDamage)
			//		totalDamageAmount += dmg.getAmount();
			//	output += dmg + ", ";
			//}
			output += "Total Damage Amount: "+unit.getBasicDamage() +"\n";
			output+="Move Speed: "+unit.getMoveSpeed() +"\n";
			output+="\n"+getTextDescription();
		}
		
		return output;
	}
	
	private String getTextDescription() {
		return "Tower";
	}

	public int getWidth(){
		return 64;
	}
	
	public int getHeight(){
		return 64;
	}
	
	public boolean getJustLevelled() {
		return justLevelled;
	}
	
	public void setJustLevelled(boolean justLevelled) {
		this.justLevelled = justLevelled;
	}
	
	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	public void levelUp(){
		level++;
	}

	public int getXpNeededtoLevel() {
		return xpNeededtoLevel;
	}

	/**
	 * Sets the xp needed for next level
	 * e.g., if the unit is currently a 2, then it needs 300 xp to level
	 */
	public void setXpNeededtoLevel() {
		xpNeededtoLevel = 150 * getLevel();
	}
	
	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}

	public void addXp(int xp) {
		this.xp += xp;
		checkLevelUp();
	}

	public List<TowerUnit> getUnits() {
		return units;
	}

	public void addUnit(TowerUnit unit) {
		// When adding a unit, apply this tower's item effects
		applyItems(unit);
		units.add(unit);
	}
	
	public void removeUnit(TowerUnit unit) {
		units.remove(unit);		
	}
	
	public List<Item> getItems() {
		return items;
	}

	public void addItem(Item item) {
		// First add it to the list for future units
		items.add(item);
		// then add item on all units
		for(Unit unit : units) {
			applyItems(unit);
		}
		// Also, apply the tower effect
		item.applyEffectToTower(this);
	}
	
	public void removeItem(Item item){
		items.remove(item);
	}

	public Point2D getTopLeft() {
		return topLeft;
	}

	public void setTopLeft(Point2D point2d) {
		this.topLeft = point2d;
	}
	
	public Point2D getCenter() {
		return new Point2D.Double(topLeft.getX() + getWidth() / 2, topLeft.getY() + getHeight() / 2);
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}

	public int getMaxUnits() {
		return maxUnits;
	}

	public void setMaxUnits(int maxUnits) {
		this.maxUnits = maxUnits;
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}
	
	public int getCost(){
		return this.cost;
	}
	
	public void setCost(int cost){
		this.cost = cost;
	}

	public void setHoverImage(Image hoverImage) {
		this.hoverImage = hoverImage;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	public Image getImage() {
		if(image == null)
			try {
				setImage(ImageIO.read(getClass().getResource("/"+imageUrl)));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return image;
	}

	public Image getHoverImage() {
		if(hoverImage == null)
			try {
				setHoverImage(ImageIO.read(getClass().getResource("/"+hoverImageUrl)));
				return hoverImage;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getHoverImageUrl() {
		return hoverImageUrl;
	}

	public void setHoverImageUrl(String hoverImageUrl) {
		this.hoverImageUrl = hoverImageUrl;
	}
}
