package model.tower.item;

import model.tower.Tower;
import model.tower.type.RiflemanTower;
import model.unit.Unit;

public class Agent extends SpecializationItem {
	private static final long serialVersionUID = 1L;

	public Agent() {
		setName("Agent's Submachine Gun (10L) [SPECIALIZATION]");
		setDescription("Upgrade your riflemen into Agents. Agile and quick, they tear apart mobs of weaker enemies.");
		setLevelCost(10);
		setApplicableTower(RiflemanTower.class);
	}

	public void applyEffect(Unit unit) {
		// Fire gives each unit a timer damage
		unit.setHits(unit.getHits()+3);
		unit.getSprite().setImageUrl("images/sprites/AgentAnimation.png");
		unit.getSprite().setImage(null);
	}

	public void applyEffectToTower(Tower tower) {
		tower.setMaxUnits(2);
		for(Unit unit : tower.getUnits())
			unit.setHealth(0);
		tower.setImageUrl("images/AgentZone.png");
		tower.setImage(null);
		tower.setLevel(11);
	}
	
	public void applyLevelUpEffect(Unit unit) {
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+5);
	}

}
