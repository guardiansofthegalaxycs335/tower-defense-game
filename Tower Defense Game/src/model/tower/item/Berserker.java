package model.tower.item;

import model.tower.Tower;
import model.tower.type.DefenderTower;
import model.unit.Unit;

public class Berserker extends SpecializationItem {
	private static final long serialVersionUID = 1L;

	public Berserker() {
		setName("Berserker's Axe (10L) [SPECIALIZATION]");
		setDescription("Upgrade your defenders into Berserkers. Berserkers easily destroy enemy units, but sacrifice survivability.");
		setLevelCost(10);
		setApplicableTower(DefenderTower.class);
	}

	public void applyEffect(Unit unit) {
		// Fire gives each unit a timer damage
		unit.setHits(unit.getHits()+1);
		unit.setMaxHealth(unit.getMaxHealth()+20);
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+20);
		unit.getSprite().setImageUrl("images/sprites/BerserkerAnimation.png");
		unit.getSprite().setImage(null);
	}

	public void applyEffectToTower(Tower tower) {
		tower.setImageUrl("images/BerserkerZone.png");
		tower.setImage(null);
		tower.setLevel(11);
	}
	
	public void applyLevelUpEffect(Unit unit) {
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+10);
	}

}
