package model.tower.item;

import model.tower.Item;
import model.tower.Tower;
import model.tower.type.AttackTower;
import model.unit.Unit;
import model.unit.damage.ArmorPenetratingDamage;
import model.unit.damage.BasicDamage;
import model.unit.damage.CritDamage;
import model.unit.damage.Damage;

public class Criticola extends Item {
	private static final long serialVersionUID = 5923340341497321603L;


	public Criticola() {
		setName( "Criticola (200E)");
		setDescription("Makes a tower have a 5% chance to inflict critical damage. (X3 Damage)");
		setEnergyCost(200);
		setApplicableTower(AttackTower.class);
	}


	@Override
	public void applyEffect(Unit unit) {
		
		int totalDamageAmount = 0;
		for(Damage dmg : unit.getAttackDamages()) {
			if(dmg instanceof BasicDamage)
				totalDamageAmount += dmg.getAmount();
			if (dmg instanceof ArmorPenetratingDamage)
				totalDamageAmount += dmg.getAmount();
		}
		unit.addAttackDamage(new CritDamage(totalDamageAmount));
	}


	@Override
	public void applyEffectToTower(Tower tower) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void applyEffectOnUpdate(Tower tower) {
		// TODO Auto-generated method stub
		
	}

}
