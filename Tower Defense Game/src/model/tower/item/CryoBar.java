package model.tower.item;

import controller.GameConfig;
import model.tower.Item;
import model.tower.Tower;
import model.tower.type.AttackTower;
import model.unit.Unit;
import model.unit.damage.FreezeTimerDamage;

public class CryoBar extends Item {
	private static final long serialVersionUID = 5923340341497321603L;


	public CryoBar() {
		setName( "Cryo Bar (300E)");
		setDescription("Makes a tower have a chance to freeze enemies on hit for "+100/GameConfig.FPS+" seconds");
		setEnergyCost(300);
		setApplicableTower(AttackTower.class);
	}


	@Override
	public void applyEffect(Unit unit) {
		unit.addAttackDamage(new FreezeTimerDamage(100000/GameConfig.FPS,100));
	}


	@Override
	public void applyEffectToTower(Tower tower) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void applyEffectOnUpdate(Tower tower) {
		// TODO Auto-generated method stub
		
	}

}
