package model.tower.item;

import model.tower.Tower;
import model.tower.type.ScionTower;
import model.unit.Unit;

public class Empress extends SpecializationItem {
	private static final long serialVersionUID = 1L;

	public Empress() {
		setName("Empress' Scepter (10L) [SPECIALIZATION]");
		setDescription("Upgrade your scions into Empresses. The magic of an Empress is so powerful crushes enemies with ease.");
		setLevelCost(10);
		setApplicableTower(ScionTower.class);
	}

	public void applyEffect(Unit unit) {
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+40);
		// Need to create a singularity effect that has a chance to warp enemies backwards.
		unit.getSprite().setImageUrl("images/sprites/EmpressAnimation.png");
		unit.getSprite().setImage(null);
	}

	public void applyEffectToTower(Tower tower) {
		tower.setImageUrl("images/EmpressZone.png");
		tower.setImage(null);
		tower.setLevel(11);
	}
	
	public void applyLevelUpEffect(Unit unit) {
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+30);
	}

}
