package model.tower.item;

import model.tower.Tower;
import model.tower.type.DefenderTower;
import model.unit.Unit;

public class Guardian extends SpecializationItem {
	private static final long serialVersionUID = 1L;

	public Guardian() {
		setName("Guardian's Shield (10L) [SPECIALIZATION]");
		setDescription("Upgrade your defenders into Guardians. Guardians are very difficult to kill, gaining Armor on level up.");
		setLevelCost(10);
		setApplicableTower(DefenderTower.class);
	}

	public void applyEffect(Unit unit) {
		// Fire gives each unit a timer damage
		unit.setArmor(unit.getArmor()+2);
		unit.setMaxHealth(unit.getMaxHealth()+40);
		unit.setHealth(unit.getHealth()+40);
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+10);
		unit.getSprite().setImageUrl("images/sprites/GuardianAnimation.png");
		unit.getSprite().setImage(null);
	}

	public void applyEffectToTower(Tower tower) {
		tower.setImageUrl("images/GuardianZone.png");
		tower.setImage(null);
		tower.setLevel(11);
	}
	
	public void applyLevelUpEffect(Unit unit) {
		unit.setMaxHealth(unit.getMaxHealth()+20);
		unit.setArmor(unit.getArmor()+1);
	}

}
