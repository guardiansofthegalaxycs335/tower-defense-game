package model.tower.item;

import model.tower.Item;
import model.tower.Tower;
import model.tower.type.EngineerTower;
import model.unit.Unit;

public class IncreaseEngineerTowerRate extends Item {
	private static final long serialVersionUID = 1L;

	public IncreaseEngineerTowerRate() {
		setName("Energy Generation Rate +100% (50E)");
		setDescription("Increases the energy generation rate by 100%");
		setEnergyCost(50);
		setApplicableTower(EngineerTower.class);
	}
	
	public void applyEffectToTower(Tower tower) {
		EngineerTower engTower = (EngineerTower)tower;
		/*engTower.setProduction(engTower.getProduction());*/
		engTower.setApplied(false);
	}

	public void applyEffect(Unit unit) {
		// Nothing
	}

	public void applyEffectOnUpdate(Tower tower) {
		
	}

}
