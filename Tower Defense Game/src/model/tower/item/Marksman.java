package model.tower.item;

import view.sprites.MarksmanSprite;
import model.tower.Tower;
import model.tower.type.RiflemanTower;
import model.unit.Unit;

public class Marksman extends SpecializationItem {
	private static final long serialVersionUID = 1L;

	public Marksman() {
		setName("Marksman's Sniper Rifle (10L) [SPECIALIZATION]");
		setDescription("Upgrade your riflemen into a Marksman. Slow but powerful, they excell at defeating powerful units. e");
		setLevelCost(10);
		setApplicableTower(RiflemanTower.class);
	}

	public void applyEffect(Unit unit) {
		// Fire gives each unit a timer damage
		unit.setHits(1);
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+475);
		unit.setAttackSpeed(5);
		unit.setSprite(new MarksmanSprite(unit));
		unit.getSprite().setImage(null);
	}

	public void applyEffectToTower(Tower tower) {
		tower.setMaxUnits(1);
		for(Unit unit : tower.getUnits())
			unit.setHealth(0);
		tower.setImageUrl("images/MarksmanZone.png");
		tower.setImage(null);
		tower.setRange(tower.getRange()+15);
		tower.setLevel(11);
	}
	
	public void applyLevelUpEffect(Unit unit) {
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+15);
	}

}
