package model.tower.item;

import controller.GameConfig;
import model.tower.Tower;
import model.tower.type.ScionTower;
import model.unit.Unit;
import model.unit.damage.AcidDamage;

public class Outcast extends SpecializationItem {
	private static final long serialVersionUID = 1L;

	public Outcast() {
		setName("Outcast's Tome (10L) [SPECIALIZATION]");
		setDescription("Upgrade your scions into Outcasts. The forbidden magic of the Outcast rends enemy armor, opening them up to further attack.");
		setLevelCost(10);
		setApplicableTower(ScionTower.class);
	}

	public void applyEffect(Unit unit) {
		unit.getBasicDamage().setAmount(unit.getBasicDamage().getAmount()+10);
		unit.addAttackDamage(new AcidDamage(250000/GameConfig.FPS,75));
		unit.getSprite().setImageUrl("images/sprites/OutcastAnimation.png");
		unit.getSprite().setImage(null);
	}

	public void applyEffectToTower(Tower tower) {
		tower.setImageUrl("images/OutcastZone.png");
		tower.setImage(null);
		tower.setLevel(11);
	}
	
	public void applyLevelUpEffect(Unit unit) {
		
	}

}
