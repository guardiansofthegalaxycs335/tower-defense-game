package model.tower.item;

import model.tower.Item;
import model.tower.Tower;
import model.tower.type.AttackTower;
import model.unit.Unit;
import model.unit.damage.ArmorPenetratingDamage;
import model.unit.damage.BasicDamage;
import model.unit.damage.BasicTimerDamage;
import model.unit.damage.Damage;
import model.unit.towerunit.DefenderUnit;
import model.unit.towerunit.RiflemanUnit;
import model.unit.towerunit.ScionUnit;

public class PlasmaCoreI extends Item {
	private static final long serialVersionUID = 1L;

	public PlasmaCoreI() {
		setName("Plasma Sprite (150E)");
		setDescription("Sets an enemy on fire, inflicting 100% of base damage over 2.5 seconds");
		setEnergyCost(150);
		setApplicableTower(AttackTower.class);
	}

	@Override
	public void applyEffect(Unit unit) {
		// Fire gives each unit a timer damage
		int totalDamageAmount = 0;
		for (Damage dmg : unit.getAttackDamages()) {
			if (dmg instanceof BasicDamage)
				totalDamageAmount += dmg.getAmount();
			if (dmg instanceof ArmorPenetratingDamage)
				totalDamageAmount += dmg.getAmount();
		}
		if (unit instanceof RiflemanUnit)
			unit.addAttackDamage(new BasicTimerDamage(totalDamageAmount / 50,
					20));
		if (unit instanceof DefenderUnit)
			unit.addAttackDamage(new BasicTimerDamage(totalDamageAmount / 50,
					20));
		if (unit instanceof ScionUnit)
			unit.addAttackDamage(new BasicTimerDamage(totalDamageAmount / 50,
					20));
	}

	@Override
	public void applyEffectToTower(Tower tower) {
		// TODO Auto-generated method stub

	}

	@Override
	public void applyEffectOnUpdate(Tower tower) {
		// TODO Auto-generated method stub

	}

}
