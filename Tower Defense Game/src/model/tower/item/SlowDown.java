package model.tower.item;

import controller.GameConfig;
import model.tower.Item;
import model.tower.Tower;
import model.tower.type.RiflemanTower;
import model.unit.Unit;
import model.unit.damage.SlowDownTimerDamage;
import model.unit.towerunit.RiflemanUnit;

public class SlowDown extends Item {
	private static final long serialVersionUID = 1L;
	
	public SlowDown() {
		setName( "Tar Bullets (100E)");
		setDescription("Makes rifleman slow down enemies to 25% for "+250/GameConfig.FPS+" seconds");
		setEnergyCost(100);
		setApplicableTower(RiflemanTower.class);
	}


	@Override
	public void applyEffect(Unit unit) {
		// Poison gives each rifleman unit a timer damage
		if(unit instanceof RiflemanUnit)
			unit.addAttackDamage(new SlowDownTimerDamage(250000/GameConfig.FPS,25));
		//This is about 8 seconds
	}


	@Override
	public void applyEffectToTower(Tower tower) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void applyEffectOnUpdate(Tower tower) {
		// TODO Auto-generated method stub
		
	}

}
