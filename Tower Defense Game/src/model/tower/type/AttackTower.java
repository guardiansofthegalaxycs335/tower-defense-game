package model.tower.type;

import java.awt.geom.Point2D;

import model.Game;
import model.map.Map;
import model.tower.Tower;

public class AttackTower extends Tower {

	public AttackTower(Point2D topLeft) {
		super(topLeft);
	}
	
	public AttackTower() {
		// Nothing
	}
	
	public String toString() {
		return "All Attack Towers";
	}
	
	private static final long serialVersionUID = 1L;

	@Override
	public Tower createNew(Map map, Point2D point) {
		return null;
	}

	@Override
	public void update(double deltaTime, Game game) {
		
	}

}
