package model.tower.type;

import java.awt.geom.Point2D;
import java.util.List;

import model.Cooldown;
import model.Game;
import model.map.Geometry;
import model.map.Map;
import model.map.Vector2D;
import model.tower.Tower;
import model.unit.UnitState;
import model.unit.towerunit.DefenderUnit;
import model.unit.towerunit.TowerUnit;

/**
 * The basic defender tower, which spawns friendly soldiers to block the enemy.
 * @author Nathan
 */

public class DefenderTower extends AttackTower {
	private static final long serialVersionUID = -2800248863886741614L;
	private String imageUrl = "images/DefenderZone.png";
	private String hoverImageUrl = "images/swordTowerHover.png";
	private Cooldown respawnCooldown;
	private double respawnTime = 5;
	private Point2D spawnPoint;
	private final String name = "Defender Tower";
	
	/**** CONSTRUCTORS ****/
	public DefenderTower(){
		init();
	}
	
	public DefenderTower(Point2D topLeft) {
		super(topLeft);
		init();
		setSpawnPoint(getCenter());
	}
	
	private void init() {
		respawnCooldown = new Cooldown(respawnTime);
		setRange(100);
		setCost(100);
		setImageUrl(imageUrl);
		setHoverImageUrl(hoverImageUrl);
	}

	/**** CONCRETE METHODS ****/
	
	@Override
	public void update(double deltaTime, Game game) {
		super.update(deltaTime, game);
		spawnDefenders(deltaTime);
		updateDefenders(deltaTime);
		checkLevelUp();
	}

	/**
	 * Handles the spawning of defenders.
	 * @param deltaTime Needed to handle respawn time cooldown
	 */
	public void spawnDefenders(double deltaTime) {
		DefenderTower tower = this;
		// Cooldown
		if(respawnCooldown.getCurrent() > 0.0)
			if(getUnits().size() < getMaxUnits())
				respawnCooldown.decrease(deltaTime);
		
		// Spawning
		if(tower.getUnits().size() < tower.getMaxUnits()){
			if(respawnCooldown.getCurrent() > 0.0) return;
			DefenderUnit newguy = new DefenderUnit();
			tower.addUnit(newguy);
			newguy.setPosition(tower.getCenter());
			setDefenderOrigins();
			int temp = getLevel();
			if (temp > 1) {
			for (int i = 0; i < temp-1; i++)
			newguy.levelUp();
			}
			newguy.setHealth(newguy.getMaxHealth());
			respawnCooldown.reset();
//			System.out.println("Swordsman spawned at "+tower.getCenter());
		}
	}	

	/**
	 * Handles defender movement and combat
	 * @param deltaTime
	 */
	public void updateDefenders(double deltaTime){ 
		for(TowerUnit unit : getUnits()){
			// Check if unit is dead
			unit.checkDead();
			// Check if target is dead
			if(unit.hasTarget())
				if(unit.getTarget().isDead()) {
					addXp(unit.getTarget().getPower()*3);
					unit.setTarget(null);
				}
			
			// Cooldowns
			if(unit.getAttackCooldown().getCurrent() > 0)
				unit.getAttackCooldown().decrease(deltaTime);

			// Movement and Attack
			double displacement = unit.getMoveSpeed() * deltaTime;
			if(unit.getSprite().getState() != UnitState.DYING && unit.getSprite().getState() != UnitState.DEAD) {
				if(unit.hasTarget()){
					Point2D e = unit.getTarget().getPosition();
					Point2D t = unit.getPosition();
					Vector2D et = new Vector2D(e, t);
					if(e.distance(getCenter()) > getRange()){
						moveDefenderTowardsOrigin((DefenderUnit) unit, displacement);
						unit.setTarget(null);
					} else if(Math.round(t.distance(e)) > unit.getRange()){ // Move towards enemy
	//					System.out.println("Moving "+unit+" towards "+unit.getTarget());
						unit.moveTowardsTarget(displacement);
					} else {// Attack
						unit.changeAttackState(et.getDirection());
						unit.attack();
					}
				} else {
					moveDefenderTowardsOrigin((DefenderUnit) unit, displacement);
				}
			}
		}
	}

	/**
	 * Moves a defender towards their origin. Finds a vector in the right direction and sets its magnitude to the appropriate displacement.
	 * Defenders only ever move in a straight line.
	 * TODO Maybe move this to DefenderUnit?
	 * @param unit The unit to move.
	 * @param displacement The displacement
	 */
	public void moveDefenderTowardsOrigin(DefenderUnit unit, double displacement){
		Point2D u = unit.getPosition();
		Point2D o = unit.getOrigin();
		Vector2D uo = new Vector2D(u, o);
		if(uo.getMagnitude() > displacement)
			uo.setMagnitude(displacement);
		double deltaX = uo.getDeltaX();
		double deltaY = uo.getDeltaY();
		unit.setPosition(new Point2D.Double(u.getX() + deltaX, u.getY() + deltaY));
	}
	
	/**
	 * Sets the origin of the defender. Does some geometry to avoid defenders standing on top of eachother at the spawnpoint.
	 */
	public void setDefenderOrigins(){
		List<Point2D> origins = Geometry.distributePoints(getMap().closestPointOnAnyPath(getSpawnPoint()), getUnits().size(), 20);
		for(int i=0; i<getUnits().size(); i++)
			((DefenderUnit) getUnits().get(i)).setOrigin(origins.get(i));
	}
	
	/**** GETTERS & SETTERS ****/
	@Override
	public Tower createNew(Map map, Point2D topLeft) {
		Tower tower = new DefenderTower(topLeft);
		tower.setMap(map);
		return tower;
	}
	
	public String getTextDescription() {
		return "Spawns 4 defender units which block the path and attack the enemy";
	}

	public Cooldown getRespawnCooldown() {
		return respawnCooldown;
	}
	
	public Point2D getSpawnPoint(){
		return spawnPoint;
	}
	
	public void setSpawnPoint(Point2D pt){
		spawnPoint = pt;
	}
	
	public String toString(){
		return this.name;
	}
}
