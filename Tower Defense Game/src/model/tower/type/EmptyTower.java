package model.tower.type;

import java.awt.Image;
import java.awt.geom.Point2D;

import model.Game;
import model.map.Map;
import model.tower.Item;
import model.tower.Tower;

/**
 * An empty tower, used to represent an available spot for towers in the GUI
 * @author Nathan
 */

public class EmptyTower extends Tower {
	private static final long serialVersionUID = 1L;
	
	public EmptyTower(){
		super();
	}

	public EmptyTower(Point2D point) {
		super(point);
	}

	public Image getImage() {
		return null;
	}

	public Image getHoverImage() {
		return null;
	}
	
	public String getDescription() {
		return "Empty tower slot";
	}

	@Override
	public Tower createNew(Map map, Point2D point) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(double displacement,Game game) {
		// nothing
	}
	
	/**
	 * Empty type cannot have any item
	 */
	public boolean canHaveItem(Item item) {
		return false;
	}
}
