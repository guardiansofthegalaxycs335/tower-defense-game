package model.tower.type;

import java.awt.geom.Point2D;

import model.Game;
import model.map.Map;
import model.tower.Tower;

/**
 * The basic engineer tower, which increases resource production but does not spawn enemies.
 */

public class EngineerTower extends Tower {
	private static final long serialVersionUID = -4857539802720920889L;
	private String imageUrl = "images/EngineerTower.png";
	private String hoverImageUrl = "images/medicTowerHover.png";
	private int production;
	private final String name = "Engineer Tower";
	private boolean applied;
	
	public EngineerTower(Point2D point) {
		super(point);
		init();
	}

	public EngineerTower() {
		super();
		init();
	}

	private void init() {
		production = 1;
		setCost(50);
		setImageUrl(imageUrl);
		setHoverImageUrl(hoverImageUrl);
	}
	
	public String getTextDescription() {
		return "This tower increases the resource generation rate by " + production + " every 2 seconds";
	}

	@Override
	public Tower createNew(Map map, Point2D point) {
		return new EngineerTower(point);
	}

	@Override
	public void update(double deltaTime, Game game) {
		super.update(deltaTime, game);
		// Engineer tower only updates once, simply updating game resource gen rate
		if(!isApplied()) {
			game.setResourceGenAmount(game.getResourceGenAmount()+production);
			setApplied(true);
		}
	}

	public int getProduction() {
		return production;
	}

	public void setProduction(int production) {
		this.production = production;
	}
	
	public String toString(){
		return this.name;
	}

	public boolean isApplied() {
		return applied;
	}

	public void setApplied(boolean applied) {
		this.applied = applied;
	}
}
