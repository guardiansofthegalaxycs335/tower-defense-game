package model.tower.type;

import java.awt.geom.Point2D;
import java.util.List;

import model.Cooldown;
import model.Game;
import model.map.Geometry;
import model.map.Map;
import model.map.Vector2D;
import model.tower.Tower;
import model.unit.UnitState;
import model.unit.towerunit.ScionUnit;
import model.unit.towerunit.TowerUnit;

/**
 * The scion tower, a ranged magic tower.
 * @author Christopher
 */

public class ScionTower extends AttackTower {
	private static final long serialVersionUID = -2800248863886741614L;
	private final String imageUrl = "images/ScionZone.png";
	private final String hoverImageUrl = "images/swordTowerHover.png";
	private Cooldown respawnCooldown;
	private double respawnTime = 1;
	private Point2D spawnPoint;
	private final String name = "Scion Tower";
	
	/**** CONSTRUCTORS ****/
	public ScionTower(){
		init();
	}
	
	public ScionTower(Point2D topLeft) {
		super(topLeft);
		init();
		setSpawnPoint(getCenter());
	}
	
	private void init() {
		respawnCooldown = new Cooldown(respawnTime);
		setRange(125);
		setCost(150);
		setImageUrl(imageUrl);
		setHoverImageUrl(hoverImageUrl);
	}

	/**** CONCRETE METHODS ****/
	
	@Override
	public void update(double deltaTime, Game game) {
		super.update(deltaTime, game);
		spawnUnits(deltaTime);
		updateUnits(deltaTime);
		checkLevelUp();
	}
	
	@Override
	public void setUnitsLevelUp () {
		if (getJustLevelled() == true) {
			// Set the tower range
			setRange(getRange()+2);
			// Set the unit level up
			for(TowerUnit unit : getUnits()) {
				unit.levelUp();
			}
		}
	}

	/**
	 * Handles the spawning of Units.
	 * @param deltaTime Needed to handle respawn time cooldown
	 */
	public void spawnUnits(double deltaTime) {
		ScionTower tower = this;
		// Cooldown
		if(respawnCooldown.getCurrent() > 0.0)
			if(getUnits().size() < getMaxUnits())
				respawnCooldown.decrease(deltaTime);
		
		// Spawning
		if(tower.getUnits().size() < tower.getMaxUnits()){
			if(respawnCooldown.getCurrent() > 0.0) return;
			ScionUnit newguy = new ScionUnit();
			tower.addUnit(newguy);
			newguy.setPosition(tower.getCenter());
			setUnitOrigins();
			respawnCooldown.reset();
//			System.out.println("Scion spawned at "+tower.getCenter());
		}
	}	

	/**
	 * Handles Unit movement and combat
	 * @param deltaTime
	 */
	public void updateUnits(double deltaTime){ 
		for(TowerUnit unit : getUnits()){
			// Check if unit is dead
			unit.checkDead();
			// Check if target is dead
			if(unit.hasTarget())
				if(unit.getTarget().isDead()) {
					addXp(unit.getTarget().getPower());
					unit.setTarget(null);
				}
			
			// Cooldowns
			if(unit.getAttackCooldown().getCurrent() > 0)
				unit.getAttackCooldown().decrease(deltaTime);

			// Movement and Attack
			double displacement = unit.getMoveSpeed() * deltaTime;
			if(unit.getSprite().getState() != UnitState.DYING && unit.getSprite().getState() != UnitState.DEAD) {
				if(unit.hasTarget()){
					Point2D e = unit.getTarget().getPosition();
					Point2D t = getCenter();
					Vector2D et = new Vector2D(e, t);
					if(unit.getTarget().getPosition().distance(getCenter()) > getRange())
						unit.setTarget(null);
					else {// Attack
						unit.changeAttackState(et.getDirection());
						unit.attack();
					}
				} else {
					moveUnitTowardsOrigin((ScionUnit) unit, displacement);
				}
			}
		}
	}

	/**
	 * Moves a Unit towards their origin. Finds a vector in the right direction and sets its magnitude to the appropriate displacement.
	 * Units only ever move in a straight line.
	 * TODO Maybe move this to UnitUnit?
	 * @param unit The unit to move.
	 * @param displacement The displacement
	 */
	public void moveUnitTowardsOrigin(ScionUnit unit, double displacement){
		Point2D u = unit.getPosition();
		Point2D o = unit.getOrigin();
		Vector2D uo = new Vector2D(u, o);
		if(uo.getMagnitude() > displacement)
			uo.setMagnitude(displacement);
		double deltaX = uo.getDeltaX();
		double deltaY = uo.getDeltaY();
		unit.setPosition(new Point2D.Double(u.getX() + deltaX, u.getY() + deltaY));
	}
	
	/**
	 * Sets the origin of the Unit. Does some geometry to avoid Units standing on top of eachother at the spawnpoint.
	 */
	public void setUnitOrigins() {
		List<Point2D> origins = Geometry.distributePoints(getCenter(), getUnits().size(), 20);
		for(int i=0; i<getUnits().size(); i++)
			((ScionUnit) getUnits().get(i)).setOrigin(origins.get(i));
	}
	
	/**** GETTERS & SETTERS ****/
	@Override
	public Tower createNew(Map map, Point2D topLeft) {
		Tower tower = new ScionTower(topLeft);
		tower.setMap(map);
		return tower;
	}
	
	public String getTextDescription() {
		return "This tower spawns 4 scions that attack enemies from range with an anti-armor attack.";
	}

	public Cooldown getRespawnCooldown() {
		return respawnCooldown;
	}
	
	public Point2D getSpawnPoint(){
		return spawnPoint;
	}
	
	public void setSpawnPoint(Point2D pt){
		spawnPoint = pt;
	}
	
	public String toString(){
		return this.name;
	}
}
