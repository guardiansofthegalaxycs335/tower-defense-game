package model.unit;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import model.Cooldown;
import model.map.Map;
import model.map.Vector2D;
import model.unit.damage.Damage;
import model.unit.damage.TimerDamage;
import view.sprites.Sprite;

/**
 * The abstract Unit class, which can represent any entity which moves and fights on the map (...so EnemyUnits and DefenderUnits)
 */

public class Unit implements Serializable {
	private static final long serialVersionUID = 1156398901788402376L;
	private String name;
	private Damage basicDamage;
	private int health, maxHealth, armor, range, hits;
	private double attackSpeed, moveSpeed;
	private Cooldown attackCooldown;
	private Point2D position;
	private Unit target;
	private Map map;
	private int power;
	// Attack damages are the damages this unit can give
	private List<Damage> attackDamages;
	// timerDamages are the timer damages inflicted on this unit
	private List<TimerDamage> timerDamages;

	// Sprite
	private Sprite sprite = null;

	public Unit() {
		attackCooldown = new Cooldown();
		attackCooldown.setCurrent(0.0);
		setPower(0);
		//setAttackDamage(10);
		setAttackDamages(new ArrayList<Damage>());
		setTimerDamages(new ArrayList<TimerDamage>());
		setMaxHealth(100);
		setHealth(100);
		setArmor(0);
		setHits(1);
	}

	/**
	 * Attacks the unit's current target
	 */
	public void attack() {
		if(! hasTarget()) {
			return;
		}
		if(getAttackCooldown().getCurrent() > 0) {
			return;
		}
		
		// Loop through number of hits
		for(int i = 0; i < getHits(); i++) {
			basicDamage.createNew().inflictOn(getTarget());
			for(Damage damage : attackDamages) {
					damage.createNew().inflictOn(getTarget());
			}
		}
		getAttackCooldown().reset();
	}
	
	/**
	 * Used by attack towers to determine which attack state to give units
	 * @param unit
	 * @param magnitude
	 */
	public void changeAttackState(double dir) {
		if(dir > 0 && dir <= Math.PI)
			getSprite().setState(UnitState.ATTACK_EAST);
		else
			getSprite().setState(UnitState.ATTACK_WEST);
	}

	/**** CONCRETE METHODS ****/
	public String debug(){
		String output = "        " + this;
		if(isDead()) output+=" (DEAD)";
		output+="\n         - Position: "+position;
		output+="\n         - Target: "+target;
		output+="\n         - Health: "+health+"/"+maxHealth+" ("+(((double) health)/maxHealth)*100+"%)";
		int num = 1;
		for(Damage dmg : getAttackDamages()) {
			output += "\n         - Damage " + num++ + dmg;
		}
		output+="\n         - Attack Speed: "+attackSpeed;
		output+="\n         - Move Speed: "+moveSpeed;
		output+="\n         - Next attack in: "+attackCooldown.getCurrent();
		output+="\n         - Range: "+range;
		return output;
	}

	/**
	 * Moves the unit towards its target in a straight line using Vector math
	 * @param displacement The distance to move
	 */
	public void moveTowardsTarget(double displacement){
		Point2D e = getPosition();
		Point2D t = getTarget().getPosition();
		Vector2D et = new Vector2D(e, t);
		int radius = getRange();
		if (et.getMagnitude() < radius)
			return;
		et.setMagnitude(et.getMagnitude() - radius);
		if(et.getMagnitude() > displacement)
			et.setMagnitude(displacement);
		double deltaX = et.getDeltaX();
		double deltaY = et.getDeltaY();
		// State variables
		changeMoveState(et.getDirection());
		setPosition(new Point2D.Double(e.getX() + deltaX, e.getY() + deltaY));
	}

	/**
	 * Changes the units move state based on a current direction in radians
	 * @param dir
	 */
	protected void changeMoveState(Double dir) {
		if(dir > Math.PI/4 && dir < 3*Math.PI/4)
			sprite.setState(UnitState.MOVE_EAST);
		else if(dir > 3*Math.PI/4 || dir < -3*Math.PI/4)
			sprite.setState(UnitState.MOVE_NORTH);
		else if(dir > -3*Math.PI/4 && dir < -Math.PI/4)
			sprite.setState(UnitState.MOVE_WEST);
		else if(dir > -Math.PI/4 && dir < Math.PI/4)
			sprite.setState(UnitState.MOVE_SOUTH);
	}

	/**** GETTERS & SETTERS ****/
	public Cooldown getAttackCooldown(){
		return attackCooldown;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health){
		this.health = health;
	}

	public boolean isDead(){
		return health <= 0;
	}
	
	public void checkDead() {
		if(isDead() && getSprite().getState() != UnitState.DEAD)
			getSprite().setState(UnitState.DYING);
	}

	public Unit getTarget(){
		return target;
	}

	public boolean hasTarget() {
		return target != null;
	}

	/**
	 * Sets the target. If the target is null, sets the unit state to IDLE
	 * @param target
	 */
	public void setTarget(Unit target){
		if(getSprite() != null && target == null)
			getSprite().setState(UnitState.IDLE);
		this.target = target;
	}

	public int getRange(){
		return range;
	}

	public void setRange(int range){
		this.range = range;
	}

	public double getAttackSpeed() {
		return attackSpeed;
	}

	public void setAttackSpeed(double attackSpeed) {
		this.attackSpeed = attackSpeed;
		attackCooldown.setBase(attackSpeed);
	}

	public double getMoveSpeed() {
		return moveSpeed;
	}

	public void setMoveSpeed(double moveSpeed) {
		this.moveSpeed = moveSpeed;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Point2D getPosition(){
		return position;
	}

	public void setPosition(Point2D pos){
		this.position = pos;
	}

	public Map getMap() {
		return map;
	}

	public void setMap(Map map) {
		this.map = map;
	}

	public String getDescription(){
		return "Name: " + getName() + "\n"
				+ "Health: " +getHealth()+"/"+getMaxHealth() + "\n"
				+ "Armor: " +getArmor() + "\n"
				+ "Attack speed: "+getAttackSpeed() + "\n"
				+ "Move speed: "+getMoveSpeed() + "\n";
	}

	public int getPower() {
		return power;
	}

	public void setPower(int rating) {
		this.power = rating;
	}

	public List<Damage> getAttackDamages() {
		return attackDamages;
	}

	public void addAttackDamage(Damage attackDamage) {
		attackDamages.add(attackDamage);
	}

	public List<TimerDamage> getTimerDamages() {
		return timerDamages;
	}

	public void addTimerDamage(TimerDamage timerDamage) {
		timerDamages.add(timerDamage);
	}

	public Sprite getSprite() {
		return sprite;
	}

	public void setSprite(Sprite sprite) {
		this.sprite = sprite;
	}

	public void setAttackCooldown(Cooldown cd){
		this.attackCooldown = cd;
	}

	public void setAttackDamages(List<Damage> damages){
		attackDamages = damages;
	}

	public void setTimerDamages(List<TimerDamage> damages){
		timerDamages = damages;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public Damage getBasicDamage() {
		return basicDamage;
	}

	public void setBasicDamage(Damage basicDamage) {
		this.basicDamage = basicDamage;
	}
}
