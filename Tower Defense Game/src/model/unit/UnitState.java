package model.unit;

/**
 * All the states a unit can be in, used for sprite animation.
 * @author Christopher
 *
 */

public enum UnitState {
	IDLE, MOVE_SOUTH, MOVE_WEST, MOVE_NORTH, MOVE_EAST, ATTACK_EAST,ATTACK_WEST, DYING, DEAD;
}
