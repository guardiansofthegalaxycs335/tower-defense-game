package model.unit.damage;

import java.util.TimerTask;

import model.unit.Unit;

/**
 * The  deals armor damage on the enemy unit as opposed to dealing direct damage.
 * @author mwatson007
 *
 */
public class AcidDamage extends TimerDamage {
	private static final long serialVersionUID = 9158520534888330364L;
	private int percentage;
	private int time;
	
	public AcidDamage(int time, int percentage) {
		this.setTime(time);
		this.setPercentage(percentage);
	}

	@Override
	public void inflictOn(Unit unit) {
		setUnit(unit);

		// Only adds if there is no timer damage of same type
		for(TimerDamage dmg : unit.getTimerDamages()) {
			if (dmg.equals(this))
				return;
		}
		// Make a reference to the damage in the unit, to keep from adding multiple timers
		unit.addTimerDamage(this);
		// Slow it down, then set timer to speed up again
		AcidDamage.this.getUnit().setArmor((int) (AcidDamage.this.getUnit().getArmor() * percentage/100.00));
		getTimer().schedule(new TimerTask(){public void run() {
			// Invert to speed up again
			AcidDamage.this.getUnit().setArmor((int) (AcidDamage.this.getUnit().getArmor() * 100.00/percentage));
			AcidDamage.this.getUnit().getTimerDamages().remove(AcidDamage.this);
		}}, time);
	}

	@Override
	public Damage createNew() {
		return new AcidDamage(getTime(),getPercentage());
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}


}
