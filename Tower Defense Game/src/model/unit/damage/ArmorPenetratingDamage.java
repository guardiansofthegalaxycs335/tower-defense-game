package model.unit.damage;

import model.unit.Unit;

public class ArmorPenetratingDamage extends Damage {
	private static final long serialVersionUID = 1L;

	public ArmorPenetratingDamage(int amount) {
		setAmount(amount);
	}

	@Override
	public void inflictOn(Unit unit) {
		int cumulativeAmount = (this.getAmount()-unit.getArmor()/5);
		if(cumulativeAmount < 0) cumulativeAmount = 0;
		unit.setHealth(unit.getHealth()-cumulativeAmount );
	}

	@Override
	public Damage createNew() {
		return new ArmorPenetratingDamage(getAmount());
	}
	
	public String toString() {
		return "+"+getAmount()+" armor penetrating damage";
	}

}
