package model.unit.damage;

import model.unit.Unit;

/**
 * The basic damage class simply removes health one time from
 * the given unit to the inflictDamageOn method
 * @author echochristopher
 *
 */
public class BasicDamage extends Damage {
	private static final long serialVersionUID = -7441061276256090418L;

	public BasicDamage(int amount) {
		setAmount(amount);
	}

	@Override
	public void inflictOn(Unit unit) {
		int cumulativeAmount = (this.getAmount()-unit.getArmor());
		if(cumulativeAmount < 0) cumulativeAmount = 0;
		unit.setHealth(unit.getHealth()-cumulativeAmount );
		// Decrease the armor by some every hit
		//Basic Damage should not be decaying armor! What does this do exactly? 
		//if(cumulativeAmount>unit.getArmor())
		//	unit.setArmor(unit.getArmor()-1);
	}

	@Override
	public Damage createNew() {
		return new BasicDamage(getAmount());
	}
	
	public String toString() {
		return "+"+getAmount()+" Basic Damage";
	}

}
