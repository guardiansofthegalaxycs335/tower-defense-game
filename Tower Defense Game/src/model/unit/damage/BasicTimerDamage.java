package model.unit.damage;

import java.util.TimerTask;

import model.unit.Unit;

/**
 * The basic timer damage class simply removes health based on a timer
 * @author echochristopher
 *
 */
public class BasicTimerDamage extends TimerDamage {
	private static final long serialVersionUID = 1524165248452838650L;

	public BasicTimerDamage(int amount, int runLimit) {
		setAmount(amount);
		setRunLimit(runLimit);
	}

	@Override
	public void inflictOn(Unit unit) {
		setUnit(unit);

		// Only adds if there is no timer damage of same type
		for(TimerDamage dmg : unit.getTimerDamages()) {
			if (dmg.equals(this))
				return;
		}
		// Make a reference to the damage in the unit, to keep from adding multiple timers
		unit.addTimerDamage(this);
		getTimer().scheduleAtFixedRate(new TimerTask(){public void run() {
			if(getRunCount() >= BasicTimerDamage.this.getRunLimit()) {
				BasicTimerDamage.this.getUnit().getTimerDamages().remove(BasicTimerDamage.this);
				this.cancel();
			}
			BasicTimerDamage.this.getUnit().setHealth(BasicTimerDamage.this.getUnit().getHealth()-BasicTimerDamage.this.getAmount());
			incrementRunCount();
		}}, 0, getDelay());
	}

	@Override
	public Damage createNew() {
		return new BasicTimerDamage(getAmount(),getRunLimit());
	}


}
