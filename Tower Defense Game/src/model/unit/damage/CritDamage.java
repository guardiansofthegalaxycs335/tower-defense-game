package model.unit.damage;

import model.unit.Unit;

/**
 * Based on basic damage, simply does 3X the damage on a percentage based chance.
 * @author mwatson007
 *
 */
public class CritDamage extends Damage {
	private static final long serialVersionUID = -7441061276256090418L;

	public CritDamage(int amount) {
		setAmount(amount);
	}

	@Override
	public void inflictOn(Unit unit) {
		int random = (int) (Math.random()*100);
		if (random <= 15) {
			int cumulativeAmount = ((this.getAmount()*3)-unit.getArmor());
			if(cumulativeAmount < 0) cumulativeAmount = 0;
			unit.setHealth(unit.getHealth()-cumulativeAmount );
			// Decrease the armor by some every hit
			//Basic Damage should not be decaying armor! What does this do exactly? 
			if(cumulativeAmount>unit.getArmor())
				unit.setArmor(unit.getArmor()-1);
		}
		
	}

	@Override
	public Damage createNew() {
		return new CritDamage(getAmount());
	}
	
	public String toString() {
		return  "Critical Damage";
	}

}
