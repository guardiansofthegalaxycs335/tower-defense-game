package model.unit.damage;

import java.io.Serializable;

import model.unit.Unit;

public abstract class Damage implements Serializable {
	private static final long serialVersionUID = 343853873967600389L;
	private int amount;
	
	public int getAmount(){
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public abstract void inflictOn(Unit unit);
	
	public abstract Damage createNew();
	
	public String toString() {
		return "+"+getAmount()+" Damage";
	}
}
