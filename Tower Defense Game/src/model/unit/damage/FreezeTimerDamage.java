package model.unit.damage;

import java.util.TimerTask;

import model.unit.Unit;

/**
 * The  deals freeze damage on random proc chance, freezing the enemy for a few moments.
 * @author mwatson007
 *
 */
public class FreezeTimerDamage extends TimerDamage {
	private static final long serialVersionUID = -5260581750280324406L;
	private int percentage;
	private int time;
	private boolean isInflicted = false;
	protected double previousMoveSpeed;
	
	public FreezeTimerDamage(int time, int percentage) {
		this.setTime(time);
		this.setPercentage(percentage);
	}

	@Override
	public void inflictOn(Unit unit) {
		int random = (int) (Math.random()*100);
		if (random <= 15) {
			setUnit(unit);
	
			// Only adds if there is no timer damage of same type
			for(TimerDamage dmg : unit.getTimerDamages()) {
				if (dmg.equals(this))
					return;
			}
			// Make a reference to the damage in the unit, to keep from adding multiple timers
			unit.addTimerDamage(this);
			// Slow it down, then set timer to speed up again
			previousMoveSpeed = getUnit().getMoveSpeed();
			getUnit().setMoveSpeed(0);
			getTimer().schedule(new TimerTask(){public void run() {
				// Invert to speed up again
				FreezeTimerDamage.this.getUnit().setMoveSpeed(previousMoveSpeed);
				FreezeTimerDamage.this.getUnit().getTimerDamages().remove(FreezeTimerDamage.this);
			}}, time);
		}
	}

	@Override
	public Damage createNew() {
		return new FreezeTimerDamage(getTime(),getPercentage());
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public boolean isInflicted() {
		return isInflicted;
	}

	public void setInflicted(boolean isInflicted) {
		this.isInflicted = isInflicted;
	}


}
