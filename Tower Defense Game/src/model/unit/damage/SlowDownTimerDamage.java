package model.unit.damage;

import java.util.TimerTask;

import model.unit.Unit;

/**
 * The  deals slowdown damage which slows down the unit as opposed to dealing direct damage.
 * @author echochristopher
 *
 */
public class SlowDownTimerDamage extends TimerDamage {
	private static final long serialVersionUID = 9158520534888330364L;
	private int percentage;
	private int time;
	
	public SlowDownTimerDamage(int time, int percentage) {
		this.setTime(time);
		this.setPercentage(percentage);
	}

	@Override
	public void inflictOn(Unit unit) {
		setUnit(unit);

		// Only adds if there is no timer damage of same type
		for(TimerDamage dmg : unit.getTimerDamages()) {
			if (dmg.equals(this))
				return;
		}
		// Make a reference to the damage in the unit, to keep from adding multiple timers
		unit.addTimerDamage(this);
		// Slow it down, then set timer to speed up again
		SlowDownTimerDamage.this.getUnit().setMoveSpeed(SlowDownTimerDamage.this.getUnit().getMoveSpeed() * percentage/100.00);
		getTimer().schedule(new TimerTask(){public void run() {
			// Invert to speed up again
			SlowDownTimerDamage.this.getUnit().setMoveSpeed(SlowDownTimerDamage.this.getUnit().getMoveSpeed() * 100.00/percentage);
			SlowDownTimerDamage.this.getUnit().getTimerDamages().remove(SlowDownTimerDamage.this);
		}}, time);
	}

	@Override
	public Damage createNew() {
		return new SlowDownTimerDamage(getTime(),getPercentage());
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}


}
