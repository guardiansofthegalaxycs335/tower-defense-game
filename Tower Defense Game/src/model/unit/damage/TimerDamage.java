package model.unit.damage;

import java.util.Timer;

import model.unit.Unit;
import controller.GameConfig;

/**
 * Timer damage class is the basis for running timer damages
 * The default rate is 5000/FPS
 * @author echochristopher
 *
 */
public abstract class TimerDamage extends Damage {
	private static final long serialVersionUID = 1664664063620803221L;
	private int runLimit;
	private int runCount = 0;
	private int delay;
	private transient Timer timer;
	private Unit unit;
	
	public TimerDamage() {
		setDelay(5000/GameConfig.FPS);
		setTimer(new Timer());
	}

	public int getRunLimit() {
		return runLimit;
	}

	public void setRunLimit(int runLimit) {
		this.runLimit = runLimit;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public int getRunCount() {
		return runCount;
	}

	public void incrementRunCount() {
		runCount++;
	}

	public Timer getTimer() {
		return timer;
	}

	public void setTimer(Timer timer) {
		this.timer = timer;
	}
	
	/**
	 * This is for telling whether there already is damage of this type on the unit
	 * @param dmg
	 * @return
	 */
	public boolean equals(TimerDamage dmg) {
		return dmg.getClass().equals(this.getClass());
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}
	
}
