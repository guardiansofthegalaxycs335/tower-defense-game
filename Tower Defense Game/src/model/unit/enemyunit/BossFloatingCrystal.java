package model.unit.enemyunit;

import model.map.Path;
import model.unit.damage.BasicDamage;
import view.sprites.FloatingCrystPurpleGSprite;

public class BossFloatingCrystal extends EnemyUnit {
private static final long serialVersionUID = 5632853803953843123L;

	public BossFloatingCrystal(Path path) {
		super(path);
		setMoveSpeed(10);
		setRange(15);
		setBasicDamage(new BasicDamage(250));
		setAttackSpeed(5);
		setMaxHealth(45000);
		setHealth(45000);
		setPower(90);
		setArmor(20);
		setSprite(new FloatingCrystPurpleGSprite());
		setName("Floating Crystal Boss");
	}
	

}
