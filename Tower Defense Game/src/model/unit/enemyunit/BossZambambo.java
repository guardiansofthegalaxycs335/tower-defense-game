package model.unit.enemyunit;

import view.sprites.ZambamboSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class BossZambambo extends EnemyUnit {
	private static final long serialVersionUID = -8581774406652173226L;

	public BossZambambo(Path path) {
		super(path);
		setMoveSpeed(15);
		setRange(15);
		setBasicDamage(new BasicDamage(150));
		setAttackSpeed(3);
		setMaxHealth(20000);
		setHealth(20000);
		setPower(90);
		setSprite(new ZambamboSprite());
		setName("Zambambo Boss");
	}

}
