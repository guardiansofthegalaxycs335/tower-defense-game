package model.unit.enemyunit;

import model.map.Path;
import model.unit.damage.BasicDamage;
import view.sprites.FloatingCrystGreenSprite;

public class EliteFloatingCrystal extends EnemyUnit {
private static final long serialVersionUID = -3789688458689295937L;

	public EliteFloatingCrystal(Path path) {
		super(path);
		setMoveSpeed(20);
		setRange(15);
		setBasicDamage(new BasicDamage(80));
		setAttackSpeed(2);
		setMaxHealth(5000);
		setHealth(5000);
		setPower(45);
		setArmor(15);
		setSprite(new FloatingCrystGreenSprite());
		setName("Floating Crystal Elite");
	}
	

}
