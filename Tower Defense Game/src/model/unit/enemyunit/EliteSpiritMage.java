package model.unit.enemyunit;

import view.sprites.SpiritMageGreenSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class EliteSpiritMage extends EnemyUnit {
	private static final long serialVersionUID = -7083350052283820970L;

	public EliteSpiritMage(Path path) {
		super(path);
		setMoveSpeed(35);
		setRange(15);
		setBasicDamage(new BasicDamage(75));
		setAttackSpeed(0.5);
		setMaxHealth(4500);
		setHealth(4500);
		setArmor(5);
		setPower(15);
		setSprite(new SpiritMageGreenSprite());
		setName("Elite Spirit Mage");
	}

}
