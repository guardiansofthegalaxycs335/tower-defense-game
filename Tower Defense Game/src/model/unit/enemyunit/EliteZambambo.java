package model.unit.enemyunit;

import view.sprites.ZambamboSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;


public class EliteZambambo extends EnemyUnit {
	private static final long serialVersionUID = 7904866897592922375L;

	public EliteZambambo(Path path) {
		super(path);
		setMoveSpeed(40);
		setRange(15);
		setBasicDamage(new BasicDamage(40));
		setAttackSpeed(1);
		setMaxHealth(3500);
		setHealth(3500);
		setPower(10);
		setArmor(8);
		setSprite(new ZambamboSprite());
		setName("Zambambo Elite");
	}

}
