package model.unit.enemyunit;

import java.awt.geom.Point2D;
import java.io.Serializable;

import model.map.Geometry;
import model.map.Path;
import model.map.Vector2D;
import model.unit.Unit;
import model.unit.UnitState;

/**
 * The abstract enemy class, extended by all concrete implemented enemies
 * @author Nathan
 *
 */

public abstract class EnemyUnit extends Unit implements Serializable{
	private static final long serialVersionUID = -4104210570312987302L;
	private Path path;
	
	public EnemyUnit(Path path){
		super();
		this.path = path;
		setPower(10);
		setName("Enemy Unit");
	}

	public void update(double deltaTime) {
		// Check if unit is dead
		checkDead();
		// Check if target is dead
		if(hasTarget())
			if(getTarget().isDead())
				setTarget(null);
		
		// Cooldowns
		if(getAttackCooldown().getCurrent() > 0)
			getAttackCooldown().decrease(deltaTime);

		if(getSprite().getState() != UnitState.DYING && getSprite().getState() != UnitState.DEAD) {
			// Movement and attack
			double displacement = getMoveSpeed() * deltaTime;
			if(hasTarget()){
				if(getTarget().getTarget() == this) {
					Point2D e = getTarget().getPosition();
					Point2D t = getPosition();
					Vector2D et = new Vector2D(e, t);
					if(Math.round(t.distance(e)) > getRange() + 1)
						moveTowardsPath(displacement);
					if(Math.round(t.distance(e)) < getRange() + 1) { // +/- 1 pixel cushion to avoid enemy escape bugs
						changeAttackState(et.getMagnitude());
						attack();
					}
				} else
					setTarget(null);
			} else
				moveTowardsPath(displacement);
		}
	}
	
	/**
	 * Moves the enemy along the path, either right along it if they're already on it, or at a 45 degree angle to it in the right direction if they are off it.
	 * @param displacement The distance to move.
	 */
	public void moveTowardsPath(double displacement){
		EnemyUnit enemy = this;
		Point2D e = enemy.getPosition();
		// If enemy is on the path...
		if(path.contains(e)){
			int curIndex = getPath().indexOf(enemy.getPosition());
			Point2D destination = getPath().get((int) (Math.round(curIndex + displacement)));
			Vector2D vector = new Vector2D(enemy.getPosition(), destination);
			enemy.setPosition(destination);
			// Change state
			changeMoveState(vector.getDirection());
		} else { // If enemy off the path...
			// Find closest point on the path, giving preference to points towards the end of the path
			Point2D closestP = path.closestPointOnPath(e);

			// Calculate the translation vector
			double deltaX, deltaY;
			Vector2D ep = new Vector2D(e, closestP);
			if(ep.getMagnitude() < 3){ // Almost back on the path - jump right to it
				deltaX = ep.getDeltaX();
				deltaY = ep.getDeltaY();
			} else { // Still far away - move at a 45 degree angle
				Vector2D tangent = Geometry.tangentVector(path.getDiscretePoints(), path.indexOf(closestP));
				ep.setMagnitude(displacement);
				tangent.setMagnitude(displacement);
				Vector2D translation = ep.add(tangent);
				translation.setMagnitude(displacement);

				deltaX = translation.getDeltaX();
				deltaY = translation.getDeltaY();
			}
			// Change state
			changeMoveState(ep.getDirection());
			enemy.setPosition(new Point2D.Double(e.getX() + deltaX, e.getY() + deltaY));
			return;
		}
	}
	
	/**** GETTERS & SETTERS ****/
	public Path getPath(){
		return path;
	}
	
	public void setPath(Path path){
		this.path = path;
	}
	
}
