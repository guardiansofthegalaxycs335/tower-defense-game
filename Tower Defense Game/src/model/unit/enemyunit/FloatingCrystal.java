package model.unit.enemyunit;

import view.sprites.FloatingCrystalSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class FloatingCrystal extends EnemyUnit {
	private static final long serialVersionUID = -811736128961896566L;

	public FloatingCrystal(Path path) {
		super(path);
		setMoveSpeed(20);
		setRange(15);
		setBasicDamage(new BasicDamage(35));
		setAttackSpeed(2);
		setMaxHealth(1500);
		setHealth(1500);
		setPower(10);
		setArmor(5);
		setSprite(new FloatingCrystalSprite());
		setName("Floating Crystal");
	}
	

}
