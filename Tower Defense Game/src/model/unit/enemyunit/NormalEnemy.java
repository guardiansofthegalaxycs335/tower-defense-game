package model.unit.enemyunit;

import view.sprites.ZambamboSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

/**
 * The basic enemy we have been using for debugging
 * @author Nathan
 */

public class NormalEnemy extends EnemyUnit {
	private static final long serialVersionUID = -430238483331011730L;

	public NormalEnemy(Path path) {
		super(path);
		setMoveSpeed(60);
		setRange(15);
		setBasicDamage(new BasicDamage(10));
		setAttackSpeed(1);
		setMaxHealth(100);
		setHealth(100);
		setPower(10);
		setSprite(new ZambamboSprite());
	}
}
