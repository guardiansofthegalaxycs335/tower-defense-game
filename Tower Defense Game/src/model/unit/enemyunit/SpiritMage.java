package model.unit.enemyunit;

import view.sprites.SpiritMageSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class SpiritMage extends EnemyUnit {
	private static final long serialVersionUID = -7083350052283820970L;

	public SpiritMage(Path path) {
		super(path);
		setMoveSpeed(30);
		setRange(15);
		setBasicDamage(new BasicDamage(40));
		setAttackSpeed(0.5);
		setMaxHealth(1250);
		setHealth(1250);
		setArmor(2);
		setPower(5);
		setSprite(new SpiritMageSprite());
		setName("Spirit Mage");
	}

}
