package model.unit.enemyunit;

import view.sprites.SpiritMageGiantSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class SpiritMageGiant extends EnemyUnit {
	private static final long serialVersionUID = -7083350052283820970L;

	public SpiritMageGiant(Path path) {
		super(path);
		setMoveSpeed(10);
		setRange(15);
		setBasicDamage(new BasicDamage(150));
		setAttackSpeed(4);
		setMaxHealth(35000);
		setHealth(35000);
		setPower(80);
		setArmor(20);
		setSprite(new SpiritMageGiantSprite());
		setName("Spirit Mage Giant");
	}

}
