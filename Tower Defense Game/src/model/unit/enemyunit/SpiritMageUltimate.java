package model.unit.enemyunit;

import view.sprites.SpiritMagePurpleSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class SpiritMageUltimate extends EnemyUnit {
	private static final long serialVersionUID = -7083350052283820970L;

	public SpiritMageUltimate(Path path) {
		super(path);
		setMoveSpeed(40);
		setRange(15);
		setBasicDamage(new BasicDamage(100));
		setAttackSpeed(1);
		setMaxHealth(7500);
		setHealth(7500);
		setPower(25);
		setSprite(new SpiritMagePurpleSprite());
		setName("Ultimate Spirit Mage");
	}

}
