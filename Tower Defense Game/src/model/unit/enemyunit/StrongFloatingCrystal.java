package model.unit.enemyunit;

import model.map.Path;
import model.unit.damage.BasicDamage;
import view.sprites.FloatingCrystOrangeSprite;

public class StrongFloatingCrystal extends EnemyUnit {
private static final long serialVersionUID = -1205337238025789974L;

	public StrongFloatingCrystal(Path path) {
		super(path);
		setMoveSpeed(20);
		setRange(15);
		setBasicDamage(new BasicDamage(60));
		setAttackSpeed(2);
		setMaxHealth(2000);
		setHealth(2000);
		setPower(25);
		setArmor(12);
		setSprite(new FloatingCrystOrangeSprite());
		setName("Strong Floating Crystal");
	}

}
