package model.unit.enemyunit;


import view.sprites.SpiritMageRedSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class StrongSpiritMage extends EnemyUnit {
	private static final long serialVersionUID = -7083350052283820970L;

	public StrongSpiritMage(Path path) {
		super(path);
		setMoveSpeed(35);
		setRange(15);
		setBasicDamage(new BasicDamage(50));
		setAttackSpeed(0.5);
		setMaxHealth(2250);
		setHealth(2250);
		setArmor(2);
		setPower(10);
		setSprite(new SpiritMageRedSprite());
		setName("Strong Spirit Mage");
	}

}
