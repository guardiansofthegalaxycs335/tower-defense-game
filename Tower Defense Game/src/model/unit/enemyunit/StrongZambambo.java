package model.unit.enemyunit;

import view.sprites.ZambamboSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;


public class StrongZambambo extends EnemyUnit {
	private static final long serialVersionUID = 5709748216115756329L;

	public StrongZambambo(Path path) {
		super(path);
		setMoveSpeed(75);
		setRange(15);
		setBasicDamage(new BasicDamage(20));
		setAttackSpeed(0.5);
		setMaxHealth(2400);
		setHealth(2400);
		setPower(20);
		setArmor(4);
		setSprite(new ZambamboSprite());
		setName("Strong Zambambo");
	}

}
