package model.unit.enemyunit;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.LinkedList;

import model.Cooldown;
import model.map.Path;

/**
 * A Wave is a series of enemies and delays.
 * @author Nathan
 */
public class Wave implements Serializable {
	private static final long serialVersionUID = -3309329024536846657L;
	private LinkedList<EnemyUnit> enemies;
	private LinkedList<Double> delays;
	private Cooldown nextSpawnDelay, waveDelay;
	
	public Wave(double waveDelay){
		enemies = new LinkedList<EnemyUnit>();
		delays = new LinkedList<Double>();
		nextSpawnDelay = null;
		this.waveDelay = new Cooldown(waveDelay);
	}
	
	/**
	 * Adds a single enemy to the wave. Enemies are spawned in FIFO order
	 * @param enemy The enemy to add
	 * @param delay The delay between spawning the previous enemy and spawning this one
	 */
	public void add(EnemyUnit enemy, double delay){
		if(nextSpawnDelay == null)
			nextSpawnDelay = new Cooldown(delay);
		enemies.add(enemy);
		delays.add(delay);
	}
	
	/**
	 * Adds multiple enemies of the same type at once. Uses java's reflection capabilities to construct a new instance of the same enemy type and add that to the spawn queue.
	 * @param enemy The type of enemy to spawn, including path
	 * @param delay The delay before spawning
	 * @param quantity The number of these enemies to add to the queue in order
	 */
	public void addMultiple(EnemyUnit enemy, double delay, int quantity){
		for(int i=0; i<quantity; i++){
			Constructor<? extends EnemyUnit> constructor = null;
			EnemyUnit newEnemy = null;
			try {
				constructor = enemy.getClass().getConstructor(Path.class);
				newEnemy = constructor.newInstance(enemy.getPath());
			} catch (Exception e) {
				e.printStackTrace();
			}
			add(newEnemy, delay);
		}
	}
	
	/**
	 * Returns the next enemy and pops it (and it's associated delay) off the stack
	 * @return the next enemy in line
	 */
	public EnemyUnit next(){
		delays.pop();
		if(! delays.isEmpty()){
			nextSpawnDelay.setBase(delays.peek());
			nextSpawnDelay.reset();
		}
		return enemies.pop();
	}
	
	/**
	 * Checks to see if there's an enemy to send
	 * @return true or false
	 */
	public boolean hasNext(){
		return ! enemies.isEmpty();
	}
	
	/**
	 * Prints the enemies queue as an array
	 * @return String representation of the wave
	 */
	@Override
	public String toString(){
		return Arrays.toString(enemies.toArray());
	}
	
	/**** GETTERS & SETTERS ****/
	public Cooldown getNextSpawnDelay(){
		return nextSpawnDelay;
	}
	
	public Cooldown getWaveDelay(){
		return waveDelay;
	}
	
//	public static void main(String[] args){
//		Wave wave = new Wave();
//		wave.addMultiple(new NormalEnemy(null), 5, 5);
//		System.out.println(wave);
//	}
}
