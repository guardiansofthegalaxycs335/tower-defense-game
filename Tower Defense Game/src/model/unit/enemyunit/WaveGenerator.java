package model.unit.enemyunit;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import model.Game;
import model.map.Path;

/**
 * An algorithm for procedurally generating enemy waves which dynamically scale in difficulty
 * @author Christopher
 */

public class WaveGenerator {
	private static final int FINAL_POWER = 750;
	private static final double INCREMENT_POWER_TIMES = 1.30;
	private static final int START_POWER = 20;
	@SuppressWarnings("unused")
	private static final int MIN_WAVE_LENGTH = 1;
	private static final int TIME_BETWEEN_WAVES = 20;
	private Game game;

	public WaveGenerator(Game game) {
		this.game = game;
	}

	public void generate() {
		// First, instantiate enemy classes
		Path path = game.getMap().getPath();
		EnemyUnit[] enemiesArray = {
				new StrongFloatingCrystal(path),
				new FloatingCrystal(path),
				new EliteFloatingCrystal(path),
				new BossFloatingCrystal(path),
				new SpiritMageGiant(path),
				new WeakZambambo(path),
				new StrongZambambo(path),
				new StrongSpiritMage(path),
				new SpiritMageUltimate(path),
				new SpiritMageGiant(path),
				new SpiritMage(path),
				new EliteZambambo(path),
				new EliteSpiritMage(path),
				new BossZambambo(path)
		};

		List<EnemyUnit> enemies = Arrays.asList(enemiesArray);

		// Next, create random waves
		int remaining,randomNumber;
		double time;
		Wave wave;
		// Loop through increasing powers
		Random rand = new Random();
		for(int power = START_POWER; power < FINAL_POWER; power = (int) (power * INCREMENT_POWER_TIMES)) {
			int pathNum = rand.nextInt(game.getMap().getPaths().size());
			wave = new Wave(TIME_BETWEEN_WAVES);
			remaining = power;
			for(EnemyUnit enemy : enemies) {
				enemy.setPath(game.getMap().getPath(pathNum));
				// If the enemy is too weak or too powerful for the current wave, ignore it
				if(power/3 < enemy.getPower() || power/enemy.getPower() > 25)
					continue;
				// Else, select a random number of this enemy
				randomNumber = (int) (Math.random() * (remaining/enemy.getPower() + 1));
				// ... And add them
				/*randomTime = MIN_LENGTH_BETWEEN_UNITS + (int)(Math.random() * (MAX_LENGTH_BETWEEN_UNITS + 1) - MIN_LENGTH_BETWEEN_UNITS);*/
				time = ((double)enemy.getPower()/(double)power)*10;
				if(time<0)time*=-1;
				wave.addMultiple(enemy, time, randomNumber);
				remaining -= randomNumber*enemy.getPower();
			}
			// Finally, add the wave
			game.addWave(wave);

		}
	}
}
