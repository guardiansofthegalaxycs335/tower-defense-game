package model.unit.enemyunit;

import view.sprites.ZambamboSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class WeakZambambo extends EnemyUnit {
	private static final long serialVersionUID = -3246434814968147914L;

	public WeakZambambo(Path path) {
		super(path);
		setMoveSpeed(50);
		setRange(15);
		setBasicDamage(new BasicDamage(5));
		setAttackSpeed(0.5);
		setMaxHealth(300);
		setHealth(300);
		setPower(2);
		setSprite(new ZambamboSprite());
		setName("Weak Zambambo");
	}

}
