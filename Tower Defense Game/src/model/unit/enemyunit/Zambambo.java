package model.unit.enemyunit;

import view.sprites.ZambamboSprite;
import model.map.Path;
import model.unit.damage.BasicDamage;

public class Zambambo extends EnemyUnit {
	private static final long serialVersionUID = -7083350052283820970L;

	public Zambambo(Path path) {
		super(path);
		setMoveSpeed(50);
		setRange(15);
		setBasicDamage(new BasicDamage(10));
		setAttackSpeed(0.5);
		setMaxHealth(2000);
		setHealth(2000);
		setPower(5);
		setSprite(new ZambamboSprite());
		setName("Zambambo");
	}

}
