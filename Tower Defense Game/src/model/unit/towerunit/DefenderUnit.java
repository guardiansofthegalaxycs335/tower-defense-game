package model.unit.towerunit;

import view.sprites.DefenderSprite;
import model.unit.damage.BasicDamage;

/**
 * The Defender unit spawned by Defender towers to block and fight the enemy
 * @author Nathan
 *
 */

public class DefenderUnit extends TowerUnit {
	private static final long serialVersionUID = 1L;
	
	public DefenderUnit(){
		super();
		setMoveSpeed(80);
		setRange(15);
		setBasicDamage(new BasicDamage(15));
		setAttackSpeed(1);
		setMaxHealth(100);
		setHealth(100);
		setHits(2);
		setArmor(1);
		setSprite(new DefenderSprite());
		setName("Defender");
	}
	
	public void levelUp() {
		getBasicDamage().setAmount(getBasicDamage().getAmount()+10);
		setMaxHealth(getMaxHealth()+20);
	}
}
