package model.unit.towerunit;

import view.sprites.RiflemanSprite;
import model.unit.damage.BasicDamage;

/**
 * The riflemen units that are spawned on top of Rifleman towers and fire at enemies from range
 * @author Christopher
 *
 */

public class RiflemanUnit extends TowerUnit {
	private static final long serialVersionUID = 1L;
	
	public RiflemanUnit() {
		super();
		setMoveSpeed(60);
		setBasicDamage(new BasicDamage(10));
		setAttackSpeed(1);
		setMaxHealth(1);
		setHealth(1);
		setHits(3);
		setSprite(new RiflemanSprite());
		setName("Rifleman");
	}
	
	public void levelUp() {
		getBasicDamage().setAmount(getBasicDamage().getAmount()+5);
	}

}
