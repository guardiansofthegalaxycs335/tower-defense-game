package model.unit.towerunit;

import model.unit.damage.ArmorPenetratingDamage;
import view.sprites.ScionSprite;

/**
 * The Scion units that shoot magic from Scion towers at range
 * @author Christopher
 *
 */

public class ScionUnit extends TowerUnit {
	private static final long serialVersionUID = 1L;

	public ScionUnit() {
		super();
		setMoveSpeed(60);
		setBasicDamage(new ArmorPenetratingDamage(10));
		setAttackSpeed(1);
		setMaxHealth(1);
		setHealth(1);
		setHits(1);
		setSprite(new ScionSprite());
		setName("Scion");
	}

	@Override
	public void levelUp() {
		getBasicDamage().setAmount(getBasicDamage().getAmount()+10);
		int temp = getRange();
		temp += 5;
		setRange(temp);
		
	}
}
