package model.unit.towerunit;

import java.awt.geom.Point2D;
import java.io.Serializable;

import model.unit.Unit;

/**
 * An abstract class encompassing all functionality shared by all TowerUnits
 * @author Nathan
 * @author Christopher
 *
 */

public abstract class TowerUnit extends Unit implements Serializable{
	private static final long serialVersionUID = 1969828922272495964L;
	private Point2D origin;

	public TowerUnit(){
		super();
	}
	
	public int getCost(){
		return 0;
	}
	
	public abstract void levelUp();
	
	/**** GETTERS & SETTERS ****/
	
	public Point2D getOrigin(){
		return origin;
	}
	
	public void setOrigin(Point2D o){
		origin = o;
	}
}
