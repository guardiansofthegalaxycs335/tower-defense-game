package network;

import model.Game;
import controller.Multiplayer;

/**
 * Sends a command to clients to start the game
 * @author Nathan
 *
 */

public class GameStartCommand extends Command<Multiplayer>{
	private static final long serialVersionUID = -8557424886231888586L;
	private Game game;

	public GameStartCommand(Game game){
		this.game = game;
	}
	
	@Override
	public void execute(Multiplayer executeOn) {
		executeOn.startGame(game);
	}

}
