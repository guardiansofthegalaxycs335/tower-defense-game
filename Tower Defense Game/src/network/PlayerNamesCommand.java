package network;

import controller.Multiplayer;
/**
 * This command is sent whenever a player joins or leaves the game (not counting spectators)
 * @author Nathan
 *
 */
public class PlayerNamesCommand extends Command<Multiplayer>{
	private static final long serialVersionUID = -8557424886231888586L;
	String player1name, player2name;
	
	/**
	 * Creates a disconnect command for the given client
	 * 
	 * @param name	username of client to disconnect
	 */
	public PlayerNamesCommand(String player1name, String player2name){
		this.player1name = player1name;
		this.player2name = player2name;
	}
	
	@Override
	public void execute(Multiplayer executeOn) {
		executeOn.setPlayerNames(player1name, player2name);
	}

}
