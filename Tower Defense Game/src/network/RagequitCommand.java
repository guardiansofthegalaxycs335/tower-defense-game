package network;

import controller.Multiplayer;

/**
 * Sends a command sent when a player quits after the game has already begun
 * @author Nathan
 *
 */

public class RagequitCommand extends Command<Multiplayer>{
	private static final long serialVersionUID = -8557424886231888586L;
	
	@Override
	public void execute(Multiplayer executeOn) {
		executeOn.getGame().win();
	}

}
