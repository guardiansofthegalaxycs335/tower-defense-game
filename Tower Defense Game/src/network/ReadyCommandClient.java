package network;

import controller.Multiplayer;
/**
 * This command is sent when a player changes their ready state in the lobby
 * 
 * @author Nathan
 *
 */
public class ReadyCommandClient extends Command<Multiplayer>{
	private static final long serialVersionUID = -8557424886231888586L;
	boolean player1ready, player2ready;
	
	/**
	 * Creates a disconnect command for the given client
	 * 
	 * @param name	username of client to disconnect
	 */
	public ReadyCommandClient(boolean player1ready, boolean player2ready){
		this.player1ready = player1ready;
		this.player2ready = player2ready;
	}
	
	@Override
	public void execute(Multiplayer executeOn) {
		executeOn.setReadyStates(player1ready, player2ready);
	}

}
