package network;

import controller.Server;
/**
 * This command is sent when a player changes their ready state in the lobby
 * 
 * @author Nathan
 *
 */
public class ReadyCommandServer extends Command<Server>{
	private static final long serialVersionUID = -8557424886231888586L;
	boolean player1ready, player2ready;
	
	/**
	 * Creates a disconnect command for the given client
	 * 
	 * @param name	username of client to disconnect
	 */
	public ReadyCommandServer(boolean player1ready, boolean player2ready){
		this.player1ready = player1ready;
		this.player2ready = player2ready;
	}
	
	@Override
	public void execute(Server executeOn) {
		executeOn.setReadyStates(player1ready, player2ready);
	}

}
