package network;

import controller.Multiplayer;

/**
 * Sends a command to clients to start the game
 * @author Nathan
 *
 */

public class ServerTerminatedCommand extends Command<Multiplayer>{
	private static final long serialVersionUID = -8557424886231888586L;
	
	@Override
	public void execute(Multiplayer executeOn) {
		executeOn.serverTerminated();
	}

}
