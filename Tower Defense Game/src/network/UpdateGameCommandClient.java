package network;

import model.Game;
import controller.Multiplayer;

/**
 * Command sent by the server to deliver the latest copy of the gamestate to clients.
 * @author Nathan
 *
 */

public class UpdateGameCommandClient extends Command<Multiplayer> {
	private static final long serialVersionUID = 4222014184904080846L;
	private Game game; // the message log from the server
	private String clientName;
	
	/**
	 * Creates a new UpdateClientCommand with the given log of messages
	 * @param messages	the log of messages
	 */
	public UpdateGameCommandClient(String clientName, Game game){
		this.game = game;
		this.clientName = clientName;
	}

	public void execute(Multiplayer executeOn) {
		executeOn.updateGame(clientName, game);
	}
}
