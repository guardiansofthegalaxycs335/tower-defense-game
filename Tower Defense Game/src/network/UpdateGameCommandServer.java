package network;

import model.Game;
import controller.Server;

/**
 * Command sent from clients to server to supply the most updating copy of their gamestate, which will then be passed along from the server back to the other clients.
 * @author Nathan
 *
 */

public class UpdateGameCommandServer extends Command<Server> {
	private static final long serialVersionUID = 4222014184904080846L;
	private volatile Game game;
	private String clientName;

	public UpdateGameCommandServer(String clientName, Game game){
		this.game = game;
		this.clientName = clientName;
	}


	public void execute(Server executeOn) {
		executeOn.updateGame(clientName, game);
	}
}
