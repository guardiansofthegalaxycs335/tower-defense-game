package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Point;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * A 5-second countdown in a nice transparent overlay. Used at the beginning of multiplayer games
 * @author Nathan
 *
 */

public class CountdownDialog extends JDialog {
	private static final long serialVersionUID = -3052686141861512157L;
	private JLabel countdown;
	private int count;
	private Timer timer;

	public CountdownDialog(JFrame parent){
		super(parent);
		count = 6;
		
		// Label
		countdown = new JLabel(count+"", SwingConstants.CENTER);
		countdown.setFont(new Font(Font.DIALOG, Font.BOLD, 150));
		countdown.setForeground(Color.WHITE);
		
		// Schedule countdown
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask(){public void run(){
			if(--count <= 0)
				dispose();
			else
				countdown.setText(count+"");
		}}, 0, 1000);

		
		// Frame layout
		// TODO Linux doesn't know how to clean up the transparent undecorated windows. This is a linux-only bug so I'm leaving it here
		setUndecorated(true);
		setBackground(new Color(0.0f, 0.0f, 0.0f, 0.7f)); // Transparent black bg
		setSize(300, 200);
		Point location = parent.getLocationOnScreen();
		location.translate(parent.getWidth()/2-getWidth()/2, parent.getHeight()/2-getHeight()/2);
		setLocation(location);
		setLayout(new BorderLayout());
		setModalityType(Dialog.ModalityType.MODELESS);
		add(countdown, BorderLayout.CENTER);

		setVisible(true);
	}

	public static void main(String[] args){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		new CountdownDialog(frame);
	}

}
