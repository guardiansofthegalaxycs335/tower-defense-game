package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.Game;
import model.tower.Item;
import model.tower.Tower;
import model.tower.type.DefenderTower;
import model.tower.type.EngineerTower;
import model.tower.type.RiflemanTower;
import model.tower.type.ScionTower;
import controller.GameConfig;

/**
 * A panel containing the map, sidebar, and info panels. In multiplayer a ChatPanel will be placed underneath, in Singleplayer there will be none
 * 
 * @author Casey
 * @author Nathan
 *
 */

public class GamePanel extends JPanel implements Observer {
	private static final long serialVersionUID = 1L;
	private MapPanel map;
	private Game game;
	private JScrollPane infoScrollPane;
	private JTextField resourceField;
	private JPanel center, east, towers, shopPanel, towerPanel, towerSpacer;
	private ImageIcon swordTower = new ImageIcon(getClass().getResource("/images/SwordSymbol.png"));
	private ImageIcon engineerTower = new ImageIcon(getClass().getResource("/images/EngineerSymbol.png"));
	private ImageIcon scionTower = new ImageIcon(getClass().getResource("/images/ScionSymbol.png"));
	private ImageIcon rifleTower = new ImageIcon(getClass().getResource("/images/RiflemanSymbol.png"));
	private TowerButton engineerTowerButton, swordTowerButton,rifleTowerButton,scionTowerButton;
	private JTabbedPane buyOptionPane = new JTabbedPane();
	private ItemList itemShop;
	private List<Item> itemList;
	private InfoPanel infoPanel;
	private String currentMessage = "";
	private Tower selectedTower = null;
	private boolean singleplayer;

	public GamePanel(Game game){
		this(game, true);
	}
	
	public GamePanel(Game game, boolean singleplayer) {
		this.singleplayer = singleplayer;
		this.game = game;
		game.addObserver(this);

		// The info panel will be used by all children elements
		infoPanel = new InfoPanel();
		center = layoutCenter();
		east = layoutEast();
		add(center, BorderLayout.WEST);
		add(east, BorderLayout.EAST);
		setSize(new Dimension(center.getWidth() + east.getWidth() + 40, center.getHeight()));
		setVisible(true);
	}

	/**
	 * Handles layout for the center
	 * @return
	 */
	public JPanel layoutCenter(){
		// TODO testing multiplayer map panel is singleplayer only
//		map = new MapPanel(game,this);
//		JPanel center = new JPanel(); 
//		center.add(map, BorderLayout.NORTH);
//		center.setSize(map.getSize());
		return map = new MapPanel(game, this);
	}

	/**
	 * Handles sidebar layout
	 * @return
	 */
	public JPanel layoutEast(){
		JPanel east = new JPanel();
		east.setPreferredSize(new Dimension(300, 600));
		east.setSize(new Dimension(300, 600));
		east.setLayout(new BorderLayout());

		shopPanel = new JPanel(new BorderLayout());

		resourceField = new JTextField("Energy: " + game.getEnergy() + "         Lives: " + game.getLives() + "         Level: " + game.getLevel());
		resourceField.setEditable(false);

		itemList = new ArrayList<Item>(Arrays.asList(GameConfig.itemList));

		itemShop = new ItemList(new ArrayList<Item>());
		itemShop.setFocusable(false);
		itemShop.addListSelectionListener(new ItemShopListener());

		buyOptionPane.add(itemShop, "Item Shop");

		shopPanel.add(buyOptionPane, BorderLayout.CENTER);
		shopPanel.setPreferredSize(new Dimension(200, 500));

		if(singleplayer)
			shopPanel.add(speedPanel(), BorderLayout.NORTH);

		infoScrollPane = new JScrollPane(infoPanel);
		infoScrollPane.setFocusable(false);

		east.add(setupTowerPanel(), BorderLayout.NORTH);
		east.add(shopPanel, BorderLayout.CENTER);
		east.add(infoScrollPane, BorderLayout.SOUTH);

		return east;
	}

	/**
	 * Creates a new speed control panel. Only used in Singleplayer
	 * @return
	 */
	private JPanel speedPanel(){
		JPanel speedPanel = new JPanel();		
		// Pause
		JButton pause = new JButton("||");
		pause.setFocusable(false);
		pause.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
			game.togglePaused();
			new PauseMenu((controller.Singleplayer) SwingUtilities.getWindowAncestor(GamePanel.this));
			game.togglePaused();
		}});

		// Play
		JButton play = new JButton(">");
		play.setFocusable(false);
		play.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
			game.setGameSpeed(1);
		}});

		// Fast forward
		JButton ff = new JButton(">>");
		ff.setFocusable(false);
		ff.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
			game.setGameSpeed(3);
		}});

		// Layout panel
		speedPanel.setSize(100, 100);
		speedPanel.setLayout(new GridLayout(1, 6));
		speedPanel.add(new JPanel());
		speedPanel.add(pause);
		speedPanel.add(play);
		speedPanel.add(ff);
		speedPanel.add(new JPanel());
		speedPanel.setBorder(new EmptyBorder(0, 0, 10, 0));
		return speedPanel;
	}

	/**
	 * Listener for selected tower to put information in the information box
	 * @author Nathan
	 *
	 */
	private class TowerButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// Have to deselect the tower on the map first
			map.setSelectedTower(null);
			Tower tower = ((TowerButton) e.getSource()).getTower();
			infoPanel.setText("--------------------------- Information ---------------------------");
			infoPanel.append("\n" + tower.getDescription());

			// Update item shop to only show items that this tower supports
			setItemListFromTower(tower);
		}

	}

	/**
	 * Item SHOP listener. Not inventory listener
	 */
	private class ItemShopListener implements ListSelectionListener{
		@Override
		public void valueChanged(ListSelectionEvent arg0) {
			// TODO Auto-generated method stub
			if(itemShop.getSelectedValue() == null) return;
			game.setSelectedTower(null);
			infoPanel.setText("--------------------------- Information ---------------------------");
			infoPanel.append("\n" + itemShop.getSelectedValue().getDescription());
			infoPanel.append("\n\n-- DRAG AND DROP ONTO TOWER TO APPLY --");
		}

	}

	/**
	 * Cleaner layout for the tower section of the GUI
	 * @author Casey
	 * @return towerPanel
	 */
	public JPanel setupTowerPanel(){
		towers = new JPanel();
		towers.setPreferredSize(new Dimension(100, 65));
		towers.setLayout(new GridLayout(1, 3, 5, 5));

		swordTowerButton = new TowerButton(swordTower,new DefenderTower());
		rifleTowerButton = new TowerButton(rifleTower,new RiflemanTower());
		engineerTowerButton = new TowerButton(engineerTower,new EngineerTower());
		scionTowerButton = new TowerButton(scionTower, new ScionTower());

		swordTowerButton.addActionListener(new TowerButtonListener());
		rifleTowerButton.addActionListener(new TowerButtonListener());
		engineerTowerButton.addActionListener(new TowerButtonListener());
		scionTowerButton.addActionListener(new TowerButtonListener());

		engineerTowerButton.setMaximumSize(new Dimension(20, 20));
		swordTowerButton.setMaximumSize(new Dimension(20, 20));
		rifleTowerButton.setMaximumSize(new Dimension(20, 20));
		scionTowerButton.setMaximumSize(new Dimension(20, 20));

		engineerTowerButton.setFocusable(false);
		swordTowerButton.setFocusable(false);
		rifleTowerButton.setFocusable(false);
		scionTowerButton.setFocusable(false);

		towerSpacer = new JPanel(new BorderLayout());
		towerSpacer.add(new JPanel(), BorderLayout.NORTH);
		towerSpacer.add(towers, BorderLayout.CENTER);
		towerSpacer.add(new JPanel(), BorderLayout.SOUTH);

		towers.setMaximumSize(new Dimension(25, 25));
		towerPanel = new JPanel(new BorderLayout());
		towerPanel.setPreferredSize(new Dimension(150, 90));

		resourceField = new JTextField("Energy: " + game.getEnergy() + "         Lives: " + game.getLives() + "         Level: " + game.getLevel());
		resourceField.setEditable(false);

		towers.add(engineerTowerButton);
		towers.add(swordTowerButton);
		towers.add(rifleTowerButton);
		towers.add(scionTowerButton);
		towerPanel.add(towerSpacer, BorderLayout.CENTER);

		return towerPanel;
	}

	/**
	 * Updates the gamePanel whenever the game it is observing changes
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		map.repaint();
		JFrame frame = (JFrame) SwingUtilities.getWindowAncestor(this);
		if(game.lost()) new LostGameDialog(frame);
		if(game.won())	new WonGameDialog(frame);

		resourceField.setText("Energy: " + game.getEnergy() + "         Lives: " + game.getLives() + "         Level: " + game.getLevel());

		// Set the text in the info panel to the message in game
		// Saves the previous message so it does not keep updating
		if(!game.getMessage().equals(currentMessage)) {
			infoPanel.setText(game.getMessage());
			currentMessage = game.getMessage();
		}
		// Same as above. This is necessary for updating the item shop list. It is sort of a hack
		if(game.getSelectedTower() != null && !game.getSelectedTower().equals(selectedTower)) {
			setItemListFromTower(game.getSelectedTower());
			selectedTower  = game.getSelectedTower();
		}

		// Disable towers when they cost too much
		toggleTowers();
	}

	/**
	 * Handles disabling tower buttons when you don't have enough energy to buy them.
	 */
	private void toggleTowers() {
		// Yeah, this is hardcoded because we are not planning on adding many new buttons
		if(game.getEnergy() >= swordTowerButton.getTower().getCost())
			swordTowerButton.setEnabled(true);
		else
			swordTowerButton.setEnabled(false);

		if(game.getEnergy() >= rifleTowerButton.getTower().getCost())
			rifleTowerButton.setEnabled(true);
		else
			rifleTowerButton.setEnabled(false);

		if(game.getEnergy() >= engineerTowerButton.getTower().getCost())
			engineerTowerButton.setEnabled(true);
		else
			engineerTowerButton.setEnabled(false);

		if(game.getEnergy() >= scionTowerButton.getTower().getCost())
			scionTowerButton.setEnabled(true);
		else
			scionTowerButton.setEnabled(false);
	}

	/**
	 * Handles listing the items available for a tower
	 * @param tower
	 */
	public void setItemListFromTower(Tower tower) {
		// Update item shop to only show items that this tower supports
		List<Item> items = new ArrayList<Item>();
		for(Item item : itemList) {
			if(tower.canHaveItem(item) && item.getEnergyCost() <= game.getEnergy() && item.getLevelCost() <= tower.getLevel()) {
				items.add(item);
			}
		}
		itemShop.setItems(items);
	}

	public InfoPanel getInfoPanel() {
		return infoPanel;
	}

	public void setInfoPanel(InfoPanel infoPanel) {
		this.infoPanel = infoPanel;
	}
}

