package view;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.GameConfig;

/**
 * An alternative map selector with image thumbnails.
 * @author Nathan
 *
 */

public class ImageMapSelector extends JDialog {
	private static final long serialVersionUID = -6311024137014481247L;
	private JLabel title;
	private JPanel container;
	private String selectedValue;
	private static final int SPACING = 25;
	private static final int TILE_SIZE = 160;
	
	public ImageMapSelector(JFrame parent, String[] maps){
		super(parent);
		
		// Title
		title = new JLabel ("Select a map:", SwingConstants.CENTER);
		title.setFont(new Font(title.getFont().getName(), Font.PLAIN, 20));
		title.setBorder(new EmptyBorder(SPACING, 0, 0, 0));
		
		// Calculate grid size needed
		int numMaps = GameConfig.maps.length;
		int colsNeeded = (int) Math.floor(Math.sqrt(numMaps));
		int rowsNeeded = (int) Math.ceil(((double) numMaps)/colsNeeded);
		
		// Grid of map buttons
		container = new JPanel();
		container.setBorder(new EmptyBorder(SPACING, SPACING, SPACING, SPACING));
		container.setLayout(new GridLayout(rowsNeeded, colsNeeded, SPACING, SPACING));
		for(String mapID : maps){
			JButton mapButton = new JButton();
			mapButton.setPreferredSize(new Dimension(TILE_SIZE, TILE_SIZE));
			BufferedImage background = null;
			try {
				background = ImageIO.read(getClass().getResource("/maps/"+mapID+"_background.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			BufferedImage thumbnail = resize(background, TILE_SIZE, TILE_SIZE);
			mapButton.setIcon(new ImageIcon(thumbnail));
			
			mapButton.addActionListener(new MapListener(mapID));
			
			container.add(mapButton);
		}
		
		// Add components
		setLayout(new BorderLayout());
		add(title, BorderLayout.NORTH);
		add(container, BorderLayout.SOUTH);
		
		// Window
		setTitle("Select a map");
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setResizable(false);
		setVisible(true);
	}
	
	public static BufferedImage resize(BufferedImage image, int width, int height) {
	    BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
	    Graphics2D g2d = (Graphics2D) bi.createGraphics();
	    g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
	    g2d.drawImage(image, 0, 0, width, height, null);
	    g2d.dispose();
	    return bi;
	}
	
	public String getSelectedValue(){
		return selectedValue;
	}
	
	private class MapListener implements ActionListener {
		private String mapID;
		
		public MapListener(String mapID){
			super();
			this.mapID = mapID;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			selectedValue = mapID;
			dispose();
		}
		
	}
}