package view;

import java.awt.Dimension;

import javax.swing.JTextArea;
/**
 * Information panel to display selected information for towers, items, enemies, etc.
 * Possibly an observer? Repaints based on gui mouse click listener
 * Works as a canvas
 * @author Casey
 */
public class InfoPanel extends JTextArea {
	private static final long serialVersionUID = 1L;

	public InfoPanel(){
		setText("--------------------------- Information ---------------------------");
		setRows(15);
		setColumns(25);
		setEditable(false);
		setPreferredSize(new Dimension(300,200));
		setLineWrap(true);
		setWrapStyleWord(true);
	}
}
