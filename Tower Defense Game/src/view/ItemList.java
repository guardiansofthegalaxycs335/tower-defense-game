package view;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.*;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.TransferHandler;

import model.tower.Item;

/**
 * The JList for the item shop
 * @author Nathan
 *
 */

public class ItemList extends JList<Item> implements Transferable,
		DragSourceListener, DragGestureListener {

	// marks this JButton as the source of the Drag
	private static final long serialVersionUID = 1L;

	private DragSource source;
	private TransferHandler t;
	private List<Item> items;

	public ItemList(List<Item> values) {
		super((Item[]) values.toArray(new Item[values.size()]));
		setItems(values);
		setDragEnabled(true);
		setLayoutOrientation(JList.VERTICAL);

		// The TransferHandler returns a new ItemList
		// to be transferred in the Drag
		t = new TransferHandler() {
			private static final long serialVersionUID = 1L;
			private JList<Item> list;

			@SuppressWarnings("unchecked")
			public Transferable createTransferable(JComponent c) {
				list = (JList<Item>) c;
				List<Item> selectedValues = list.getSelectedValuesList();
				return new ItemList(selectedValues);
			}
		};
		setTransferHandler(t);

		// The Drag will copy the DnDButton rather than moving it
		source = new DragSource();
		source.createDefaultDragGestureRecognizer(this,
				DnDConstants.ACTION_COPY, this);
	}

	/**
	* The DataFlavor is a marker to let the DropTarget know how to
	* handle the Transferable
	*/
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[] { new DataFlavor(ItemList.class, "JButton") };
	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return true;
	}

	public Object getTransferData(DataFlavor flavor) {
		return this;
	}

	public void dragEnter(DragSourceDragEvent dsde) {
	}

	public void dragOver(DragSourceDragEvent dsde) {
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent dsde) {
	}

	public void dragExit(DragSourceEvent dse) {
	}

	/**
	 * when the drag finishes, then repaint the DnDButton
	 * so it doesn't look like it has still been pressed down
	 */
	public void dragDropEnd(DragSourceDropEvent dsde) {
		repaint();
	}

	/**
	 * when a DragGesture is recognized, initiate the Drag
	 */
	public void dragGestureRecognized(DragGestureEvent dge) {
		ItemList itemList = new ItemList(getSelectedValuesList());
		source.startDrag(dge, DragSource.DefaultMoveDrop, itemList, this);
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> values) {
		this.setListData((Item[]) values.toArray(new Item[values.size()]));
		this.items = values;
	}

}
