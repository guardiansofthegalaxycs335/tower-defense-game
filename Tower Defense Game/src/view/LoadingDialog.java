package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
/**
 * Displays an image as the map is loaded. Disposal handled by the parent JFrame, otherwise it sticks around forever.
 * @author Nathan
 *
 */
public class LoadingDialog extends JDialog {
	private static final long serialVersionUID = -3052686141861512157L;
	private JLabel message;

	public LoadingDialog(JFrame parent){
		super(parent);

		// Label
		message = new JLabel("Loading map...", SwingConstants.CENTER);
		message.setFont(new Font(Font.DIALOG, Font.PLAIN, 18));
		message.setForeground(Color.WHITE);

		// Frame layout
		setUndecorated(true);
		setBackground(new Color(0.0f, 0.0f, 0.0f, 0.7f)); // Transparent black bg
		setSize(200, 60);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setLayout(new BorderLayout());
		add(message, BorderLayout.CENTER);
		setVisible(true);
		toFront();
	}

	public static void main(String[] args){
		JFrame frame = new JFrame();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		new LoadingDialog(frame);
	}
}
