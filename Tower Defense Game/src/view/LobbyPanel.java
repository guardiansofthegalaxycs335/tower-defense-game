package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import network.ReadyCommandServer;
/**
 * The GUI associated with the lobby in multiplayer mode
 * @author Nathan
 *
 */
public class LobbyPanel extends JPanel {
	private static final long serialVersionUID = -6553777029442932607L;
	private JButton player1button, player2button;
	private JLabel player1label, player2label;
	private ObjectOutputStream output;
	String clientName;
	
	public LobbyPanel(String clientName, ObjectOutputStream output){
		this.output = output;
		this.clientName = clientName;
		
		// Buttons
		player1button = new JButton();
		player2button = new JButton();
		Font buttonFont = new Font(player1button.getFont().getName(), Font.PLAIN, 35);
		player1button.setFont(buttonFont);
		player2button.setFont(buttonFont);
		player1button.setFocusable(false);
		player2button.setFocusable(false);
		Dimension buttonSize = new Dimension(100, 225);
		player1button.setPreferredSize(buttonSize);
		player2button.setPreferredSize(buttonSize);
		setNotReady(player1button);
		setNotReady(player2button);
		player2button.setVisible(false);
		
		// Labels
		player1label = new JLabel("", SwingConstants.CENTER);
		player2label = new JLabel("", SwingConstants.CENTER);
		player1label.setVerticalAlignment(JLabel.BOTTOM);
		player2label.setVerticalAlignment(JLabel.BOTTOM);
		
		// Layout
		GridLayout layout = new GridLayout(2, 2, 10, 10);
		setLayout(layout);
		
		// Add components
		add(player1label);
		add(player2label);
		add(player1button);
		add(player2button);
	}
	
	public void setNotReady(JButton button){
		button.setText("Not ready");
		button.setBackground(Color.RED);
	}
	
	public void setReady(JButton button){
		button.setText("Ready");
		button.setBackground(Color.GREEN);
	}
	
	public void setReadyStates(boolean player1ready, boolean player2ready){
		if(player1ready) setReady(this.player1button);
		else setNotReady(this.player1button);
		if(player2ready) setReady(this.player2button);
		else setNotReady(this.player2button);
	}
	
	public void setPlayerNames(String player1name, String player2name){
		player1label.setText(player1name + " (host)");
		Font font = player2label.getFont();
		player1label.setFont(new Font(font.getName(), Font.PLAIN, 16));
		if(player2name == null){
			player2label.setText("Waiting for second player");
			player2label.setFont(new Font(font.getName(), Font.ITALIC, 16));
			player2button.setVisible(false);
		} else {
			player2label.setText(player2name);
			player2label.setFont(new Font(font.getName(), Font.PLAIN, 16));
			player2button.setVisible(true);
		}
		
		// Add action listeners if they aren't there yet
		if(player1button.getActionListeners().length == 0 && player2button.getActionListeners().length == 0){
			if(player1name.equals(clientName)){
				player1button.addActionListener(new ReadyListener(player1button));
				player2button.setEnabled(false);
			} else {
				player2button.addActionListener(new ReadyListener(player2button));
				player1button.setEnabled(false);
			}
		}
	}
	
	private class ReadyListener implements ActionListener{
		private JButton button;
		
		private ReadyListener(JButton button){
			this.button = button;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if(button.getText().equals("Ready"))
				setNotReady(button);
			else
				setReady(button);
			boolean player1ready = player1button.getText().equals("Ready");
			boolean player2ready = player2button.getText().equals("Ready");
			try{
				output.writeObject(new ReadyCommandServer(player1ready, player2ready));
			} catch(Exception err){
				err.printStackTrace();
			}
		}
	}
}
