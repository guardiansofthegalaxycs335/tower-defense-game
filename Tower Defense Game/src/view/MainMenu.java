package view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import controller.Multiplayer;

/**
 * The main menu of the game.
 * @author Casey
 *
 */

public class MainMenu extends JFrame{
	private static final long serialVersionUID = -7581975333313902078L;
	private Canvas canvas;
	private BufferedImage backGround = null;

	public MainMenu(){
		layoutGUI();
	}

	private void layoutGUI(){
		// Background image
		canvas = new Canvas();
		try{
			backGround = ImageIO.read(getClass().getResource("/images/AtaraxiaDefense.jpg"));
		}catch(IOException e){
			e.printStackTrace();
		}

		// Add listeners
		canvas.addMouseMotionListener(new ClickListener());
		canvas.addMouseListener(new ClickListener());

		// Add components
		add(canvas);

		// Configure window
		setSize(new Dimension(1900, 1000));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
	}

	private class Canvas extends JPanel {
		private static final long serialVersionUID = -4554754210310472371L;

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			g2.drawImage(backGround, 0, 0, null);
		}
	}

	private class ClickListener extends MouseAdapter{
		public void mouseClicked(MouseEvent e){
			int x = e.getX();
			int y = e.getY();

			if((x>=35 && x<=605) && (y>=315 && y<=380)){
				new SingleplayerMenu(MainMenu.this);

			} else if((x>=35 && x<=605) && (y>=470 && y<=532)){
				MultiplayerMenu multiplayerMenu = new MultiplayerMenu(MainMenu.this); // Modal blocking
				if(multiplayerMenu.getHost() != null){
					new Multiplayer(multiplayerMenu.getHost(), multiplayerMenu.getPort(), multiplayerMenu.getUsername());
					dispose();
				}
			} else if((x>=35 && x<=365) && (y>=775 && y<=845)){
				JOptionPane.showMessageDialog(MainMenu.this, "Options are available in game!");
			} else if((x>=35 && x<=425) && (y>=625 && y<= 695)){
				new TutorialPanel(MainMenu.this);
			}
		}
	}

	public static void main(String [] args){
		new MainMenu();
	}
}


