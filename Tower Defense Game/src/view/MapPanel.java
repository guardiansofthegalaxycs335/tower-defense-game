package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;

import model.Game;
import model.map.Map;
import model.tower.Item;
import model.tower.Tower;
import model.tower.type.DefenderTower;
import model.tower.type.EmptyTower;
import model.unit.Unit;
import model.unit.enemyunit.EnemyUnit;
import model.unit.towerunit.TowerUnit;
import view.sprites.Sprite;

public class MapPanel extends JPanel implements DropTargetListener {
	private static final long serialVersionUID = 1L;

	protected Map map;
	protected Game game;
	private GamePanel gamePanel;
	private Tower selectedTower;
	private EnemyUnit selectedEnemy;

	@SuppressWarnings("unused")
	private DropTarget dt;

	public MapPanel(Game game, GamePanel gamePanel) {
		//setLayout(new GridLayout(10, 10));
		setLayout(null);
		//setPreferredSize(new Dimension(640,640));
		this.game = game;
		this.gamePanel = gamePanel;
		this.map = game.getMap();
		selectedTower = null;
		setPreferredSize(new Dimension(map.getBackgroundImage().getWidth(), map.getBackgroundImage().getHeight()));
		setSize(new Dimension(map.getBackgroundImage().getWidth(), map.getBackgroundImage().getHeight()));
		//		setSize(new Dimension(map.getBackgroundImage().getWidth(), map.getBackgroundImage().getHeight()));
		drawMap();

		// Set up listeners
		addMouseMotionListener(new ListenToMouse());
		addMouseListener(new ListenToMouse());
		setFocusable(true);

		// Add drop target when highlighted
		dt = new DropTarget(this, this);

	}

	private void drawMap() {
		TowerPanel mapSpace;
		for(int i=0; i < game.getTowers().length; i++){
			Tower tower = game.getTower(i);
			mapSpace = new TowerPanel(tower, i, gamePanel);
			mapSpace.setGame(game);
			mapSpace.setBounds((int) tower.getTopLeft().getX(), (int) tower.getTopLeft().getY(), tower.getWidth(), tower.getHeight());
			add(mapSpace);
		}
	}

	private void drawTowers(Graphics2D g2) {
		TowerPanel towerPanel;
		for(int i = 0; i < MapPanel.this.getComponentCount();i++) {
			if(MapPanel.this.getComponent(i) instanceof TowerPanel) {
				towerPanel = (TowerPanel) MapPanel.this.getComponent(i);
				towerPanel.setTower(game.getTower(i));
				g2.drawImage(towerPanel.getImage(), towerPanel.getX(), towerPanel.getY(), null);
			}
		}
	}

	private void drawUnits(Graphics2D g2) {
		// Draw all enemies
		Sprite sprite = null;
		for(EnemyUnit enemy : game.getEnemies().toArray(new EnemyUnit[game.getEnemies().size()])) {
			sprite = ((Unit) enemy).getSprite();
			if(enemy == selectedEnemy){
				try {
					g2.drawImage(ImageIO.read(getClass().getResource("/images/indicator.png")), (int) enemy.getPosition().getX() - 7, (int) enemy.getPosition().getY() -  3*sprite.getHeight()/4-8, null);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(game.paused())
				sprite.setPaused(true);
			else
				sprite.setPaused(false);
			if (sprite != null && !sprite.isFinished())
				g2.drawImage(sprite.getSpriteImage(), (int)((Unit) enemy).getPosition().getX()  - sprite.getWidth()/2, (int)((Unit) enemy).getPosition().getY() - 3*sprite.getHeight()/4, null);
			// Health bar
			int hpwidth = 40;
			int greenwidth = (int) Math.round(hpwidth * (((double) enemy.getHealth()) / enemy.getMaxHealth()));
			int redwidth = hpwidth - greenwidth;
			g2.setColor(Color.GREEN);
			g2.fillRect((int) enemy.getPosition().getX() - hpwidth / 2, (int) enemy.getPosition().getY() -  3*sprite.getHeight()/4, greenwidth, 2);
			g2.setColor(Color.RED);
			g2.fillRect((int) (enemy.getPosition().getX() - hpwidth / 2) + greenwidth, (int) enemy.getPosition().getY() -  3*sprite.getHeight()/4, redwidth, 2);
			// Armor bar
			if(enemy.getArmor() > 0) {
				// Hue the color based on armor
				int armorHue = 6*enemy.getArmor() % 128;
				Color c = new Color(128-armorHue, 128-armorHue, 128-armorHue);
				g2.setColor(c);
				g2.fillRect((int) enemy.getPosition().getX() - hpwidth / 2, (int) enemy.getPosition().getY() -  3*sprite.getHeight()/4 - 2, hpwidth, 1);
			}

		}

		// Draw all units
		for(Tower tower : game.getTowers()){
			if(tower instanceof EmptyTower) continue;
			for(TowerUnit unit : tower.getUnits().toArray(new TowerUnit[tower.getUnits().size()])) {
				sprite = unit.getSprite();
				if(game.paused())
					sprite.setPaused(true);
				else
					sprite.setPaused(false);
				if (sprite != null && !sprite.isFinished())
					g2.drawImage(sprite.getSpriteImage(), (int)unit.getPosition().getX()  - sprite.getWidth()/2, (int)unit.getPosition().getY() - 2*sprite.getHeight()/3, null);
				// Health bar for defender units
				if(tower instanceof DefenderTower) {
					int hpwidth = 40;
					int greenwidth = (int) Math.round(hpwidth * (((double) unit.getHealth()) / unit.getMaxHealth()));
					int redwidth = hpwidth - greenwidth;
					g2.setColor(Color.GREEN);
					g2.fillRect((int) unit.getPosition().getX() - hpwidth / 2, (int) unit.getPosition().getY() - 24, greenwidth, 2);
					g2.setColor(Color.RED);
					g2.fillRect((int) (unit.getPosition().getX() - hpwidth / 2) + greenwidth, (int) unit.getPosition().getY() - 24, redwidth, 2);
					// Armor bar
					if(unit.getArmor() > 0) {
						// Hue the color based on armor
						int armorHue = 6*unit.getArmor() % 128;
						Color c = new Color(128-armorHue, 128-armorHue, 128-armorHue);
						g2.setColor(c);
						g2.fillRect((int) unit.getPosition().getX() - hpwidth / 2, (int) unit.getPosition().getY() - 26, hpwidth, 1);
					}
				}
			}
			// If tower is selected, draw range circle
			if(game.getSelectedTower() == tower) {
				g2.setColor(Color.RED);
				g2.drawOval((int) tower.getCenter().getX() - tower.getRange(), (int) tower.getCenter().getY() - tower.getRange(),  tower.getRange() * 2,  tower.getRange() * 2);
				if(gamePanel != null){
					gamePanel.getInfoPanel().setText("--------------------------- Information ---------------------------");
					gamePanel.getInfoPanel().append("\n" + tower.getDescription());
				}
			}
		}

	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;
		super.paintComponent(g);

		// Draw background image
		g2.drawImage(map.getBackgroundImage(), 0, 0, null);

		// Draw Towers
		drawTowers(g2);
		drawUnits(g2);

		drawInfoBar(g2);
	}

	private void drawInfoBar(Graphics g){
		final int HEIGHT = 40;
		final int BIG_FONT = 18;
		final int FONT = 12;

		// Player names, energy, and lives
		g.setColor(new Color(0.0f, 0.0f, 0.0f, 0.5f));
		g.fillRect(0, 0, getWidth(), HEIGHT);
		g.setColor(Color.WHITE);
		g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, BIG_FONT));
		String name = game.getPlayerName() == null ? "Singleplayer" : game.getPlayerName();
		g.drawString(name, 10, (int) (HEIGHT * 0.5 + BIG_FONT / 2) );
		g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, FONT)); 
		//		g.drawString(" Energy: "+game.getEnergy()+"   Lives: "+game.getLives()+" ", getWidth() - 170, (int) (HEIGHT * 0.5 + FONT / 2));
		BufferedImage lifeIcon = null;
		BufferedImage energyIcon = null;
		try {
			lifeIcon = ImageIO.read(getClass().getResource("/images/LifeSymbol.png"));
			energyIcon = ImageIO.read(getClass().getResource("/images/EnergySymbol.png"));
		} catch (IOException e){
			e.printStackTrace();
		}

		// Incoming waves ticker
		if(game.getWaves().peek() != null)
			if((int)game.getWaves().peek().getWaveDelay().getCurrent() != 0)
				g.drawString("More incoming enemy units in: " + (int)game.getWaves().peek().getWaveDelay().getCurrent(), getWidth()/2 - 90, (int) (HEIGHT * 0.5 + FONT / 2));
		g.drawImage(energyIcon, getWidth() - 150, (int) (HEIGHT * 0.5 - lifeIcon.getHeight() / 2), null);
		g.drawString(game.getEnergy()+"", getWidth() - 108, (int) (HEIGHT * 0.5 + FONT / 2));
		g.drawImage(lifeIcon, getWidth() - 70, (int) (HEIGHT * 0.5 - lifeIcon.getHeight() / 2), null);
		g.drawString(game.getLives()+"", getWidth() - 35, (int) (HEIGHT * 0.5 + FONT / 2));
	}

	void highlight() {
		for(int i = 0; i < MapPanel.this.getComponentCount();i++) {
			if(MapPanel.this.getComponent(i) instanceof TowerPanel) {
				TowerPanel towerPanel = (TowerPanel) MapPanel.this.getComponent(i);
				Tower space = towerPanel.getTower();
				// If space is tower space, and no tower exists, toggle it
				if(space instanceof EmptyTower) {
					// Set to selector image
					towerPanel.highlight();
				}
			}
		}
		repaint();
	}

	/**
	 * Gets rid of the highlights on the map
	 */
	private void removeHighlights() {
		setSelectedTower(null);
		for(int i = 0; i < MapPanel.this.getComponentCount();i++) {
			if(MapPanel.this.getComponent(i) instanceof TowerPanel) {
				TowerPanel towerPanel = (TowerPanel) MapPanel.this.getComponent(i);
				towerPanel.unhighlight();
			}
		}
		repaint();
	}

	public Tower getSelectedTower() {
		return game.getSelectedTower();
	}

	public void setSelectedTower(Tower selectedTower) {
		// Now send the request
		game.setSelectedTower(selectedTower);
	}

	// Drag and drop implementation

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		try {
			// Ok, get the object and try to figure out what it is
			Transferable tr = dtde.getTransferable();

			DataFlavor[] flavors = tr.getTransferDataFlavors();
			for (int i = 0; i < flavors.length; i++) {
				// Ok, is it a Java object?
				if (flavors[i].isFlavorSerializedObjectType()) {
					Object o = tr.getTransferData(flavors[i]);
					for(int j = 0; j < MapPanel.this.getComponentCount();j++) {
						if(MapPanel.this.getComponent(j) instanceof TowerPanel) {
							TowerPanel towerPanel = (TowerPanel) MapPanel.this.getComponent(j);
							// If object is a tower button, show places it can go
							if(o instanceof TowerButton) {
								// If its empty, then highlight it
								if(towerPanel.getTower() instanceof EmptyTower)
									towerPanel.highlight();
								// If object is list of items, highlight applicable towers
							} else if (o instanceof ItemList) {
								// Otherwise, get the items
								ItemList itemList = (ItemList) o;
								// Look at each item and highlight the tower it applies to
								for(Item item : itemList.getItems()) {
									// If tower is applicable and not empty, highlight it
									if(towerPanel.getTower().canHaveItem(item)) {
										towerPanel.highlight();
									}
								}
							}
						}

					}

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		// nothing
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
		// nothing
	}

	@Override
	public void dragExit(DropTargetEvent dte) {
		removeHighlights();
	}

	@Override
	public void drop(DropTargetDropEvent dtde) {
		removeHighlights();
	}

	private class ListenToMouse extends MouseInputAdapter {

		private Point point;
		private TowerPanel lastHover = null;

		public void mouseClicked(MouseEvent e) {
			requestFocus();
			// Get point from x, y
			point = new Point(e.getX(), e.getY());
			// Get space at point

			/* ENEMY HIGHLIGHTING */
			EnemyUnit foundEnemy = null;
			for(EnemyUnit enemy : game.getEnemies()){
				if(point.distance(enemy.getPosition()) <= 20){
					foundEnemy = enemy;
					break;
				}
			}
			if(foundEnemy != null){
				if(gamePanel != null){
					game.setSelectedTower(null);
					selectedEnemy = foundEnemy;
					gamePanel.getInfoPanel().setText("--------------------------- Information ---------------------------");
					gamePanel.getInfoPanel().append("\n" + selectedEnemy.getDescription());
				}
			} else {
				selectedEnemy = null;
				gamePanel.getInfoPanel().setText("");
			}

			/* TOWER HIGHLIGHTING */

			Object componentAt = MapPanel.this.getComponentAt(point);
			if((!(componentAt instanceof TowerPanel)))
				return;

			TowerPanel towerPanel = (TowerPanel) componentAt;
			Tower space = towerPanel.getTower();


			// If there is no tower and there is a selected tower, make one
			if(space instanceof EmptyTower && selectedTower != null) {
				// Set tower to the selected tower
				// If it is null, then it will still work
				// It actually creates a new tower that will be on the map

				game.setTower(towerPanel.getTowerIndex(), selectedTower.createNew(map,towerPanel.getTower().getTopLeft()));
				towerPanel.setTower(game.getTower(towerPanel.getTowerIndex()));

				// Remove hover images
				/*removeHighlights();*/

				repaint();
			}
			// Otherwise, if there is a tower in the slot, display tower information
			if(!(space instanceof EmptyTower)){
				// Display range highlight circle
				game.setSelectedTower(space);
			}
		}

		public void mouseMoved(MouseEvent evt) {
			// Get point from x, y
			point = new Point(evt.getX(), evt.getY());

			// Get space at point
			Object componentAt = MapPanel.this.getComponentAt(point);
			if((!(componentAt instanceof TowerPanel))){
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				if(lastHover != null){
					lastHover.setImage(lastHover.getTower().getImage());
					lastHover = null;
					repaint();
				}
				return;
			} else if(selectedTower != null || !(((TowerPanel) componentAt).getTower() instanceof EmptyTower)) {
				setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			}
		}

	}

	public void setGame(Game game){
		this.game = game;
		this.map = game.getMap();
		this.map.findBackgroundImage();
		repaint();
	}

}
