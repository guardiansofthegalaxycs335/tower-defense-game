package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import controller.GameConfig;
import controller.Singleplayer;
/**
 * Deprecated list-based map selector.
 * GUI component which allows the user to select a map
 * @author Casey
 *
 */
public class MapSelector extends JFrame {
	private static final long serialVersionUID = 1L;
	private JList<String> mapList;
	private JScrollPane mapPane;
	private JPanel mapPanel, buttonPanel;
	private JButton startButton;
	private MainMenu host;
	
	public MapSelector(MainMenu host){
		this.host = host;
		this.setSize(245, 245);
		layoutSelector();
	}
	
	private void layoutSelector(){
		mapList = new JList<String>(GameConfig.maps);
		mapPane = new JScrollPane(mapList);
		mapPanel = new JPanel();
		mapPanel.add(mapPane);
		
		startButton = new JButton("Start");
		startButton.addActionListener(new StartGameListener());
		startButton.setPreferredSize(new Dimension(60, 30));
		
		buttonPanel = new JPanel(new GridLayout(1, 3, 5, 10));
		buttonPanel.add(new JPanel());
		buttonPanel.add(startButton);
		buttonPanel.add(new JPanel());
		
		this.setLayout(new BorderLayout());
		
		this.setTitle("Select a map to play");
		this.add(mapPanel, BorderLayout.CENTER);
		this.add(buttonPanel, BorderLayout.SOUTH);
		this.setVisible(true);
		this.setLocation(300, 300);
	}
	
	private class StartGameListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			new Singleplayer(mapList.getSelectedValue());
			dispose();
			host.dispose();
		}
		
	}
}
