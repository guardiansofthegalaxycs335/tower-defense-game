package view;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.GameConfig;
import controller.Server;

/**
 * The menu that appears after Multiplayer is clicked on the main menu, allowing players to host or join a server.
 * @author Nathan
 */

public class MultiplayerMenu extends JDialog {
	private static final long serialVersionUID = 6805376524930737167L;
	private JButton hostButton, joinButton;
	private JLabel hostLabel, joinLabel, hostPortLabel, hostUsernameLabel, ipLabel, portLabel, usernameLabel;
	private JTextField hostPort, hostUsername, joinIp, joinPort, joinUsername;
	private String host, port, username;

	private static final int PADDING = 10;

	public MultiplayerMenu(JFrame parent){
		super(parent);

		JPanel container = new JPanel();
		container.setBorder(new EmptyBorder(PADDING, PADDING, PADDING, PADDING));

		// Buttons
		hostButton = new JButton("Host server");
		joinButton = new JButton("Join server");

		// Column titles
		hostLabel = new JLabel("Host", SwingConstants.CENTER);
		Font font = hostLabel.getFont();
		hostLabel.setFont(new Font(font.getName(), Font.PLAIN, 20));
		joinLabel = new JLabel("Join", SwingConstants.CENTER);
		joinLabel.setFont(new Font(font.getName(), Font.PLAIN, 20));

		// Field labels
		hostPortLabel = new JLabel("Port: ");
		hostUsernameLabel = new JLabel("Username: ");
		ipLabel = new JLabel("IP: ");
		portLabel = new JLabel("Port: ");
		usernameLabel = new JLabel("Username: ");

		// Fields
		hostPort = new JTextField("9001");
		hostUsername = new JTextField("player1");
		joinIp = new JTextField("localhost");
		joinPort = new JTextField("9001");
		joinUsername = new JTextField("player2");

		// Layout
		GridLayout layout = new GridLayout(8, 2, 10, 10);
		container.setLayout(layout);

		// Register listeners
		joinButton.addActionListener(new JoinListener());
		hostButton.addActionListener(new HostListener());

		// Add components
		container.add(hostLabel);
		container.add(joinLabel);
		container.add(hostPortLabel);
		container.add(ipLabel);
		container.add(hostPort);
		container.add(joinIp);
		container.add(hostUsernameLabel);
		container.add(portLabel);
		container.add(hostUsername);
		container.add(joinPort);
		container.add(new JPanel());
		container.add(usernameLabel);
		container.add(new JPanel());
		container.add(joinUsername);
		container.add(hostButton);
		container.add(joinButton);
		add(container);

		// Window
		container.setSize(600, 400);
		setSize(container.getWidth() + PADDING * 2, container.getHeight() + PADDING * 2);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setTitle("Start Multiplayer");
		setResizable(false);
		toFront();
		setVisible(true);
	}

	/**
	 * Listener for clicking join button
	 */
	private class JoinListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(! validateInput()) return;
			host = joinIp.getText();
			port = joinPort.getText();
			username = joinUsername.getText();
			dispose(); // This method closes and destroys the JFrame and lets the OS clean it up from memory
		}

		public boolean validateInput(){
			// TODO More specific validation?
			if(joinUsername.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Please choose a username", "Error", JOptionPane.OK_OPTION);
				return false;
			} else if (joinPort.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Please specify the port", "Error", JOptionPane.OK_OPTION);
				return false;
			} else if (joinIp.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Please specify an IP address", "Error", JOptionPane.OK_OPTION);
				return false;
			}
			return true;
		}
	}

	/**
	 * Listener for clicking host button
	 */
	private class HostListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(! validateInput()) return;
			setVisible(false);
			ImageMapSelector mapSelector = new ImageMapSelector((JFrame) getParent(), GameConfig.multiplayerMaps);
			if(mapSelector.getSelectedValue() != null) {
				new Server(hostPort.getText(), mapSelector.getSelectedValue());
				host = "localhost";
				port = hostPort.getText();
				username = hostUsername.getText();
				dispose(); // This method closes and destroys the JFrame and lets the OS clean it up from memory
			} else {// TODO handle null
				setVisible(true);
			}
		}

		public boolean validateInput(){
			// TODO More specific validation?
			if(hostUsername.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Please choose a username", "Error", JOptionPane.OK_OPTION);
				return false;
			} else if (hostPort.getText().equals("")) {
				JOptionPane.showMessageDialog(null, "Please specify the port", "Error", JOptionPane.OK_OPTION);
				return false;
			}
			return true;
		}
	}

	public String getHost(){
		return host;
	}

	public String getPort(){
		return port;
	}

	public String getUsername(){
		return username;
	}

	public static void main(String[] args){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		new MultiplayerMenu(frame);
	}
}
