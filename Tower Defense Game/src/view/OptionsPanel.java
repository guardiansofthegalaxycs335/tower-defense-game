package view;

import java.awt.GridLayout;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 * Creates a simple panel with which to change game characteristics
 * @author cskowron11
 *
 */
public class OptionsPanel extends JDialog {
	private static final long serialVersionUID = -1403044392845423894L;
	private JPanel options, musicPanel, effectsPanel, gameSpeedPanel;
	private JSlider musicSlider, effectsSlider, gameSpeedSlider;
	private JLabel musicLabel, effectsLabel, gameSpeedLabel;
	
	public OptionsPanel(JFrame parent){
		layoutPanel();
	}
	
	private void layoutPanel(){
		options = new JPanel(new GridLayout(3, 4));
		
		options.add(layoutMusicPanel());
		options.add(layoutEffectsPanel());
		options.add(layoutGameSpeedPanel());
		
		add(options);
		setSize(350, 350);
		setLocation(400, 400);
		setVisible(true);
	}
	
	private JPanel layoutMusicPanel(){
		musicPanel = new JPanel();
		
		musicSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
		musicSlider.setMajorTickSpacing(25);
		musicSlider.setMinorTickSpacing(5);
		musicSlider.setPaintTicks(true);
		musicSlider.setPaintLabels(true);
		
		musicLabel = new JLabel("Music");
		musicPanel.add(musicLabel);
		musicPanel.add(musicSlider);
		
		return musicPanel;
	}
	
	private JPanel layoutEffectsPanel(){
		effectsPanel = new JPanel();
		
		effectsSlider = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
		effectsSlider.setMajorTickSpacing(25);
		effectsSlider.setMinorTickSpacing(5);
		effectsSlider.setPaintTicks(true);
		effectsSlider.setPaintLabels(true);
		
		effectsLabel = new JLabel("Effects");
		effectsPanel.add(effectsLabel);
		effectsPanel.add(effectsSlider);
		
		return effectsPanel;
	}
	
	private JPanel layoutGameSpeedPanel(){
		gameSpeedPanel = new JPanel();
		
		gameSpeedSlider = new JSlider(JSlider.HORIZONTAL, 1, 5, 1);
		gameSpeedSlider.setMajorTickSpacing(1);
		gameSpeedSlider.setPaintLabels(true);
		gameSpeedSlider.setPaintTicks(true);
		gameSpeedSlider.setSnapToTicks(true);
	
		gameSpeedLabel = new JLabel("Game Speed");
		gameSpeedPanel.add(gameSpeedLabel);
		gameSpeedPanel.add(gameSpeedSlider);
		
		return gameSpeedPanel;
	}
}
