package view;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.Singleplayer;
/**
 * The menu that pops up when the user pauses the game. Does not handle the actual pausing of the game, but does block the instantiating thread.
 * 
 * @author Nathan
 *
 */
public class PauseMenu extends JDialog {
	private static final long serialVersionUID = -5687369931511772200L;
	private static final int PADDING = 10;
	
	public PauseMenu(final Singleplayer parent){
		super(parent);
		
		// Title
		JLabel title = new JLabel("Game Paused", SwingConstants.CENTER);
		title.setFont(new Font(title.getFont().getName(), Font.PLAIN, 20));
		
		// Container
		JPanel container = new JPanel();
		container.setBorder(new EmptyBorder(PADDING, PADDING, PADDING, PADDING));
		container.setLayout(new GridLayout(5, 1, PADDING, PADDING));
		
		// Buttons
		JButton resume = new JButton("Resume Game");
		JButton rules = new JButton("Rules");
		JButton save = new JButton("Save Game");
		JButton quit = new JButton("Quit to Main Menu");
		
		// Listeners
		resume.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				dispose();
            }
		});
		rules.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				new TutorialPanel(parent);
            }
		});
		save.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				saveGame(parent);
            }
		});
		quit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				parent.close();
            }
		});
		
		// Add components
		setLayout(new BorderLayout());
		container.add(title, BorderLayout.CENTER);
		container.add(resume);
		container.add(rules);
		container.add(save);
		container.add(quit);
		add(container);
		
		// Dialog layout
		container.setSize(300, 350);
		setSize(container.getWidth() + 2 * PADDING, container.getHeight() + 2 * PADDING);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setVisible(true);
	}
	
	private void saveGame(Singleplayer parent){
		String name = JOptionPane.showInputDialog("Please choose a name for this savegame.");
		if(! name.equals(""))
			if(parent.saveGame(name))
				JOptionPane.showMessageDialog(parent, "Game saved!");
			else
				JOptionPane.showMessageDialog(parent, "Game save failed.");
	}
}
