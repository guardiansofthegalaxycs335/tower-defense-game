package view;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

/**
 * GUI showing a list of savegames to load or delete
 * @author Nathan
 *
 */

public class SaveSelector extends JDialog {
	private static final long serialVersionUID = 1L;
	private JList<String> saveList;
	private JScrollPane savesPane;
	private JPanel savePanel, buttonPanel;
	private JButton startButton, deleteButton, cancelButton;
	private boolean cancelled = false;

	public SaveSelector(final JFrame parent){
		super(parent);

		DefaultListModel<String> model = new DefaultListModel<String>();
		for(String s : fileNames("saves"))
			model.addElement(s);
		saveList = new JList<String>(model);
		savesPane = new JScrollPane(saveList);
		savesPane.setPreferredSize(new Dimension(275, 200));
		savePanel = new JPanel();
		savePanel.add(savesPane);

		// Start Button
		startButton = new JButton("Load");
		startButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(getSelectedValue() == null)
					JOptionPane.showMessageDialog(parent, "Please select a save to load.");
				else
					dispose();
			}
		});

		// Delete button
		deleteButton = new JButton("Delete");
		deleteButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				if(getSelectedValue() != null){
					if(JOptionPane.showConfirmDialog(parent, "Are you sure you want to delete this save?", "Confirm Deletion", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
						try {
							File save = new File(getClass().getResource("/saves/"+getSelectedValue()).toURI());
							if(save.delete()){
								JOptionPane.showMessageDialog(parent, "Save deleted.", "Success", JOptionPane.INFORMATION_MESSAGE);
								((DefaultListModel<String>) saveList.getModel()).removeElementAt(saveList.getSelectedIndex());
							} else
								JOptionPane.showMessageDialog(parent, "Error deleting save.", "Error", JOptionPane.ERROR_MESSAGE);
						} catch (Exception e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(parent, "Error deleting save.", "Error", JOptionPane.ERROR_MESSAGE);
						}
				}
			}
		});

		// Cancel Button
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				cancelled = true;
				dispose();
			}
		});

		buttonPanel = new JPanel(new GridLayout(1, 3, 5, 10));
		buttonPanel.add(startButton);
		buttonPanel.add(deleteButton);
		buttonPanel.add(cancelButton);
		buttonPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		setLayout(new BorderLayout());
		add(savePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

		setTitle("Select a save to load");
		setSize(300, 280);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setVisible(true);
	}

	/**
	 * Returns the currently selected choice from the list of saves
	 * @return
	 */
	public String getSelectedValue(){
		if(cancelled) return null;
		return saveList.getSelectedValue();
	}

	/**
	 * Helper method returning all files in a given directory
	 * @param directoryPath
	 * @return
	 */
	public String[] fileNames(String directoryPath) {
		File dir = null;
		try {
			dir = new File(getClass().getResource("/"+directoryPath).toURI());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Collection<String> files = new ArrayList<String>();
		if(dir.isDirectory()){
			File[] listFiles = dir.listFiles();
			for(File file : listFiles){
				if(file.isFile() && !file.getName().equals(".savelocation")) {
					files.add(file.getName());
				}
			}
		}
		return files.toArray(new String[]{});
	}
}
