package view;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Game;
import controller.GameConfig;
import controller.Singleplayer;

/**
 * The menu that pops up after clicking on Singleplayer to allow the player to choose whether to start a new game or load a previous one.
 * @author Nathan
 *
 */

public class SingleplayerMenu extends JDialog {
	private static final long serialVersionUID = -6561884921682742374L;
	private static final int PADDING = 10;

	public SingleplayerMenu(final JFrame parent){
		super(parent);

		JPanel container = new JPanel();
		container.setSize(400, 200);
		container.setBorder(new EmptyBorder(PADDING, PADDING, PADDING, PADDING));

		// New Game
		JButton newGame = new JButton("New Game");
		newGame.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ImageMapSelector mapSelector = new ImageMapSelector(parent, GameConfig.maps); // This is a modal dialog which blocks until a map is selected
				if(mapSelector.getSelectedValue() != null){
					LoadingDialog loading = new LoadingDialog(parent);
					dispose();
					new Singleplayer(mapSelector.getSelectedValue());
					loading.dispose();
					parent.dispose();
				}
			}

		});

		// Load Game Button
		JButton loadGame = new JButton("Load Game");
		loadGame.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SaveSelector selector = new SaveSelector(parent);
				String saveName = selector.getSelectedValue();
				if(saveName != null){
					Game game = null;
					try {
						FileInputStream fileIn = new FileInputStream(new File(getClass().getResource("/saves/"+saveName).toURI()));
						ObjectInputStream objectIn = new ObjectInputStream(fileIn);
						game = (Game) objectIn.readObject();
						objectIn.close();
						fileIn.close();
					} catch (Exception e) {
						System.err.println("Error loading save: " + e);
					}
					JDialog loading = new LoadingDialog(parent);
					dispose();
					parent.dispose();
					loading.dispose();
					new Singleplayer(game);
				}
			}
		});

		Font font = new Font(newGame.getFont().getName(), Font.PLAIN, 20);
		newGame.setFont(font);
		loadGame.setFont(font);

		// Add Components
		container.setLayout(new GridLayout(1, 2, PADDING, PADDING));
		container.add(newGame);
		container.add(loadGame);

		setLayout(new BorderLayout());
		add(container, BorderLayout.CENTER);

		// Configure Dialog
		setTitle("Start a new game or load save?");
		setSize(400, 200);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setVisible(true);
	}
}
