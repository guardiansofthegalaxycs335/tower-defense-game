package view;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.*;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.TransferHandler;

import model.tower.Tower;

/**
 * A tower button from the sidebar in GamePanel
 * @author Christopher
 * @author Casey
 *
 */

public class TowerButton extends JButton implements Transferable, DragSourceListener, DragGestureListener {

	// marks this JButton as the source of the Drag
	private static final long serialVersionUID = 1L;

	private DragSource source;
	private TransferHandler t;
	private Tower tower;

	private ImageIcon towerIcon;

	public TowerButton(ImageIcon towerIcon,Tower tower) {
		super(towerIcon);
		this.towerIcon = towerIcon;
		setTower(tower);

		// The TransferHandler returns a new TowerButton
		// to be transferred in the Drag

		t = new TransferHandler() {
			private static final long serialVersionUID = 1L;

			public Transferable createTransferable(JComponent c) {
				return new TowerButton(TowerButton.this.towerIcon,getTower());
			}
		};

		setTransferHandler(t);

		// The Drag will copy the DnDButton rather than moving it

		source = new DragSource();

		source.createDefaultDragGestureRecognizer(this,
				DnDConstants.ACTION_COPY, this);

	}

	// The DataFlavor is a marker to let the DropTarget know how to

	// handle the Transferable

	public DataFlavor[] getTransferDataFlavors() {

		return new DataFlavor[] { new DataFlavor(TowerButton.class, "JButton") };

	}

	public boolean isDataFlavorSupported(DataFlavor flavor) {

		return true;

	}

	public Object getTransferData(DataFlavor flavor) {

		return this;

	}

	public void dragEnter(DragSourceDragEvent dsde) {
	}

	public void dragOver(DragSourceDragEvent dsde) {
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent dsde) {
	}

	public void dragExit(DragSourceEvent dse) {
	}

	// when the drag finishes, then repaint the DnDButton

	// so it doesn't look like it has still been pressed down

	public void dragDropEnd(DragSourceDropEvent dsde) {

		repaint();

	}

	// when a DragGesture is recognized, initiate the Drag

	public void dragGestureRecognized(DragGestureEvent dge) {

		source.startDrag(dge, DragSource.DefaultMoveDrop,
				new TowerButton(towerIcon,getTower()), this);

	}

	public Tower getTower() {
		return tower;
	}

	public void setTower(Tower tower) {
		this.tower = tower;
	}

}
