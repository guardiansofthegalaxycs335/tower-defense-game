package view;

import java.awt.Color;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import model.Game;
import model.tower.Item;
import model.tower.Tower;
import model.tower.type.EmptyTower;

/**
 * A GUI element used in map rendering. It is drawn over any instance of a tower in the MapPanel.
 * @author Nathan
 *
 */

public class TowerPanel extends JPanel  implements DropTargetListener {
	private static final long serialVersionUID = 1L;
	
	private Image image;
	private Tower tower;
	private int towerIndex;
	private Game game;
	private GamePanel gamePanel;

	@SuppressWarnings("unused")
	private DropTarget dt;
	
	public TowerPanel(Tower tower, int towerIndex, GamePanel gamePanel) {
		this.towerIndex = towerIndex;
		this.gamePanel = gamePanel;
		image = null;
		setBackground(new Color(0,0,0,0));
		setSize(32,32);
		setTower(tower);
		setOpaque(true);
		
		// Add drop target when highlighted
		dt = new DropTarget(this, this);
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Tower getTower() {
		return tower;
	}

	public void setTower(Tower tower) {
		this.tower = tower;
		// When setting a new tower, automatically change image
		if(tower.getImage() != null)
			setImage(tower.getImage());
	}
	
	public int getTowerIndex(){
		return towerIndex;
	}

	public void highlight() {
		if(getTower() instanceof EmptyTower)
			try {
				setImage(ImageIO.read(getClass().getResource("/images/Selector.png")));
			} catch (IOException e) {
				e.printStackTrace();
			}
		else {
			// TODO: Find a better way to highlight
			Border border = BorderFactory.createLineBorder(Color.BLUE,3);
			setBorder(border); 
		}
	}
	
	public void unhighlight() {
		if(getTower() instanceof EmptyTower)
			setImage(null);
		else {
			setBorder(null);
		}
	}
	
	// Drag and drop implementation

	/**
	 * This method finds out what the object dragged over it is,
	 * and highlights accordingly
	 */
	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		try {
			// Ok, get the object and try to figure out what it is
			Transferable tr = dtde.getTransferable();
			
			DataFlavor[] flavors = tr.getTransferDataFlavors();
			for (int i = 0; i < flavors.length; i++) {
				// Ok, is it a Java object?
				if (flavors[i].isFlavorSerializedObjectType()) {
					Object o = tr.getTransferData(flavors[i]);
					// If object is a tower button, show places it can go
					if(o instanceof TowerButton) {
						// If its empty, then highlight it
						if(getTower() instanceof EmptyTower)
							highlight();
					// If object is list of items, highlight applicable towers
					} else if (o instanceof ItemList) {
						// Otherwise, get the items
						ItemList itemList = (ItemList) o;
						// Look at each item and highlight the tower it applies to
						for(Item item : itemList.getItems()) {
							if(getTower().canHaveItem(item)) {
								highlight();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		// nothing
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
		// nothing

	}

	@Override
	public void dragExit(DropTargetEvent dte) {
		unhighlight();
	}

	@Override
	public void drop(DropTargetDropEvent dtde) {
		try {
			// Ok, get the dropped object and try to figure out what it is
			Transferable tr = dtde.getTransferable();
			
			DataFlavor[] flavors = tr.getTransferDataFlavors();
			for (int i = 0; i < flavors.length; i++) {
				// Ok, is it a Java object?
				if (flavors[i].isFlavorSerializedObjectType()) {
					// Assume it is a tower. This might not work always
					dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
					Object o = tr.getTransferData(flavors[i]);
					
					if(o instanceof TowerButton) {
						// If is tower, make sure this is originally empty
						if(!(tower instanceof EmptyTower))
							return;
						// if so, add the tower
						Tower newTower = ((TowerButton) o).getTower();
						// If we have enough energy, buy it
						if(game.getEnergy() > newTower.getCost()) {
							game.setTower(getTowerIndex(), newTower.createNew(game.getMap(),getTower().getTopLeft()));
							setTower(game.getTower(getTowerIndex()));
							// Subtract cost
							game.removeEnergy(newTower.getCost());
							game.setMessage("Created new \""+getTower()+"\"!");
						}
					} else if (o instanceof ItemList) {
						// If tower is empty, ignore
						if(tower instanceof EmptyTower)
							return;
						// Otherwise, get the items
						ItemList itemList = (ItemList) o;
						// Apply each item
						for(Item item : itemList.getItems()) {
							// If this item applies to this tower, buy it
							if(getTower().canHaveItem(item)) {
								// If we have enough levels
								if(getTower().getLevel() >= item.getLevelCost()) {
									// If we have enough energy, buy it
									if(game.getEnergy() >= item.getEnergyCost()) {
										tower.addItem(item);
										// Subtract cost
										game.removeEnergy(item.getEnergyCost());
										game.setMessage("Applied \""+item+"\" to \""+getTower()+"\"!");
									} else
										game.setMessage("Cannot buy \""+item+"\". Not enough energy. Build more engineer towers");
								} else
									game.setMessage("Cannot apply \""+item+"\". Not enough levels.");
							} else
								game.setMessage("Cannot apply \""+item+"\". This tower either cannot have the item, or it already has it");
						}
					}
					// Close things up
					dtde.dropComplete(true);
					gamePanel.setItemListFromTower(getTower());
					unhighlight();
					return;
				}
			}
			System.out.println("Drop failed: " + dtde);
			dtde.rejectDrop();
		} catch (Exception e) {
			e.printStackTrace();
			dtde.rejectDrop();
		}
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}
	
	

	
}
