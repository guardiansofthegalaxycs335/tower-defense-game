package view;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Creates a window explaining how to play the game
 * @author Casey
 *
 */
public class TutorialPanel extends JDialog {
	private static final long serialVersionUID = -8259428054616024497L;
	private Canvas canvas;
	private BufferedImage backGround = null;
	
	public TutorialPanel(JFrame parent){
		super(parent);
		layoutGUI();
	}
	
	private void layoutGUI(){
		// Background image
		canvas = new Canvas();
		try{
			backGround = ImageIO.read(getClass().getResource("/images/AtaraxiaDefenseTutorial.jpg"));
		}catch(IOException e){
			e.printStackTrace();
		}
		
		// Add listeners
		canvas.addMouseMotionListener(new ClickListener());
		canvas.addMouseListener(new ClickListener());
		
		// Add components
		add(canvas);
		
		// Configure window
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(dim);
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		setVisible(true);
	}
	
	private class Canvas extends JPanel {
		private static final long serialVersionUID = -6418098279288462883L;

		@Override
	    public void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D) g;
			g2.drawImage(backGround, 0, 0, null);
		}
	}
	
	private class ClickListener extends MouseAdapter{
		public void mouseClicked(MouseEvent e){
			int x = e.getX();
			int y = e.getY();
			
			if((x>=435 && x<=1375) && (y>=905 && y<=980)){
				new MainMenu();
				TutorialPanel.this.dispose();
				((JFrame) getParent()).dispose();
			} 
		}
	}
	
	public static void main(String [] args){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		new WonGameDialog(frame);
	}
}
