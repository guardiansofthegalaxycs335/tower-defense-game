package view.sprites;

import java.awt.Image;

import model.unit.UnitState;
import controller.GameConfig;


/**
 * Sprite for purple floating crystals
 * @author Christopher
 *
 */

public class FloatingCrystPurpleGSprite extends Sprite {
	private static final long serialVersionUID = -2572574655667975219L;
	private static final int width = 64, height = 64;
	private String imageUrl = "images/sprites/FloatingCrystalAnimationPurpleGIANT.png";

	public FloatingCrystPurpleGSprite() {
		super();

		setImageUrl(imageUrl);
	}

	@Override
	public Image getSpriteImage() {
		
		if(paused)
			return previousImage;
		
		if(getState() == UnitState.DYING) {
			setState(UnitState.DEAD);
			return previousImage;
		}
		
		if(cooldown.getCurrent() > 0) {
			cooldown.decrease(1.0/GameConfig.FPS);
			return previousImage;
		} else
			cooldown.reset();
		
		int maxFrames = 1;
		int row = 0, col = frame;

		switch (getState()) {
		case IDLE:
			row = 1;
			maxFrames = 4;
			break;
		case MOVE_NORTH:
			row = 0;
			maxFrames = 8;
			break;
		case MOVE_SOUTH:
			row = 0;
			maxFrames = 8;
			break;
		case MOVE_WEST:
			row = 0;
			maxFrames = 8;
			break;
		case MOVE_EAST:
			row = 0;
			maxFrames = 8;
			break;
		case ATTACK_WEST:
			row = 2;
			maxFrames = 13;
			break;
		case ATTACK_EAST:
			row = 2;
			maxFrames = 13;
			break;
		default:
			break;
		}
		
		frame = (frame + 1) % maxFrames;
		previousImage = getImage().getSubimage(col * width,
				row * height, width, height);
		return previousImage;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getWidth() {
		return width;
	}
}
