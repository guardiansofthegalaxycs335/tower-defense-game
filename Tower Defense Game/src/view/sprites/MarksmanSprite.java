package view.sprites;

import java.awt.Image;

import model.unit.Unit;
import model.unit.UnitState;
import controller.GameConfig;


/**
 * Sprite for marksmen
 * @author Christopher
 *
 */

public class MarksmanSprite extends Sprite {
	private static final long serialVersionUID = -2572574655667975219L;
	private static final int width = 32, height = 32;
	private String imageUrl = "images/sprites/MarksmanAnimation.png";
	private Unit unit;

	public MarksmanSprite(Unit unit) {
		super();
		this.unit = unit;
		setImageUrl(imageUrl);
	}

	@Override
	public Image getSpriteImage() {
		if(paused)
			return previousImage;
		
		if(getState() == UnitState.DYING) {
			setState(UnitState.DEAD);
			return previousImage;
		}
		UnitState state = getState();
		if(state != UnitState.ATTACK_EAST && state != UnitState.ATTACK_WEST) {
			if(cooldown.getCurrent() > 0) {
				cooldown.decrease(1.0/GameConfig.FPS);
				return previousImage;
			} else
				cooldown.reset();
		}
		else if(unit.getAttackCooldown().getCurrent() < 5 && frame > 0) {
			return previousImage;
		}
		
		int maxFrames = 4;
		int row = 0, col = frame;

		switch (state) {
		case IDLE:
			row = 0;
			break;
		case MOVE_NORTH:
			row = 1;
			break;
		case MOVE_SOUTH:
			row = 1;
			break;
		case MOVE_WEST:
			row = 1;
			break;
		case MOVE_EAST:
			row = 1;
			break;
		case ATTACK_WEST:
			row = 1;
			maxFrames = 2;
			break;
		case ATTACK_EAST:
			row = 2;
			maxFrames = 2;
			break;
		default:
			break;
		}

		frame = (frame + 1) % maxFrames;
		previousImage = getImage().getSubimage(col * width,
				row * height, width, height);
		return previousImage;
	}

	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getWidth() {
		return width;
	}
}