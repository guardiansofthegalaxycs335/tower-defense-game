package view.sprites;

import java.awt.Image;

import model.unit.UnitState;
import controller.GameConfig;


/**
 * Sprite for Scions
 * @author Christopher
 *
 */

public class ScionSprite extends Sprite {
	private static final long serialVersionUID = -2572574655667975219L;
	private static final int width = 32, height = 32;
	private String imageUrl = "images/sprites/ScionAnimation.png";

	public ScionSprite() {
		super();
		setImageUrl(imageUrl);
	}

	@Override
	public Image getSpriteImage() {
		if(paused)
			return previousImage;
		
		if(getState() == UnitState.DYING) {
			setState(UnitState.DEAD);
			return previousImage;
		}
		
		if(cooldown.getCurrent() > 0) {
			cooldown.decrease(1.0/GameConfig.FPS);
			return previousImage;
		} else
			cooldown.reset();
		
		int maxFrames = 12;
		int row = 0, col = frame;

		switch (getState()) {
		case IDLE:
			row = 3;
			break;
		case MOVE_NORTH:
			row = 1;
			break;
		case MOVE_SOUTH:
			row = 1;
			break;
		case MOVE_WEST:
			row = 1;
			break;
		case MOVE_EAST:
			row = 1;
			break;
		case ATTACK_WEST:
			row = 1;
			maxFrames = 9;
			break;
		case ATTACK_EAST:
			row = 2;
			maxFrames = 9;
			break;
		default:
			break;
		}

		frame = (frame + 1) % maxFrames;
		previousImage = getImage().getSubimage(col * width,
				row * height, width, height);
		return previousImage;
	}
	
	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getWidth() {
		return width;
	}
}
