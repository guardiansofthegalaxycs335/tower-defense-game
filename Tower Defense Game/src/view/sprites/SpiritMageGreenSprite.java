package view.sprites;

import java.awt.Image;

import model.unit.UnitState;
import controller.GameConfig;


/**
 * Sprite for Spirit Mage
 * @author Matt
 *
 */

public class SpiritMageGreenSprite extends Sprite {
	private static final long serialVersionUID = -2572574655667975219L;
	private static final int width = 32, height = 32;
	private String imageUrl = "images/sprites/SpiritMageAnimationGreen.png";

	public SpiritMageGreenSprite() {
		super();

		setImageUrl(imageUrl);
	}

	@Override
	public Image getSpriteImage() {
		if(paused)
			return previousImage;
		
		if(getState() == UnitState.DYING) {
			setState(UnitState.DEAD);
			return previousImage;
		}
		
		if(cooldown.getCurrent() > 0) {
			cooldown.decrease(1.0/GameConfig.FPS);
			return previousImage;
		} else
			cooldown.reset();
		
		int maxFrames = 4;
		int row = 0, col = frame;

		switch (getState()) {
		case IDLE:
			row = 9;
			break;
		case MOVE_NORTH:
			row = 4;
			maxFrames = 8;
			break;
		case MOVE_SOUTH:
			row = 3;
			maxFrames = 8;
			break;
		case MOVE_WEST:
			row = 2;
			maxFrames = 8;
			break;
		case MOVE_EAST:
			row = 1;
			maxFrames = 8;
			break;
		case ATTACK_WEST:
			row = 6;
			maxFrames = 9;
			break;
		case ATTACK_EAST:
			row = 5;
			maxFrames = 9;
			break;
		case DYING:
			row = 13;
			maxFrames = 10;
		default:
			break;
		}
		
		if(col > maxFrames)
			col = 0;
		
		frame = (frame + 1) % maxFrames;
		previousImage = getImage().getSubimage(col * width,
				row * height, width, height);
		return previousImage;
	}
	
	@Override
	public int getHeight() {
		return height;
	}

	@Override
	public int getWidth() {
		return width;
	}
}
