package view.sprites;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import javax.imageio.ImageIO;

import model.Cooldown;
import model.unit.UnitState;

/**
 * An abstract Sprite class encompassing all common functionalities of the other Sprites
 * @author Christopher
 *
 */

public abstract class Sprite implements Serializable {
	private static final long serialVersionUID = -9118708817470011005L;
	private UnitState state;
	protected int frame;
	protected Cooldown cooldown;
	protected transient BufferedImage previousImage, image;
	private String imageUrl;
	protected boolean paused = false;

	public Sprite() {
		state = UnitState.IDLE;
		cooldown = new Cooldown(0.05);
		frame = 0;
	}

	public void setState(UnitState state) {
		this.state = state;
	}

	public UnitState getState() {
		return state;
	}

	public boolean paused() {
		return paused;
	}

	public void setPaused(boolean paused) {
		this.paused = paused;
	}

	// by default, all sprites never finish
	public boolean isFinished() {
		return false;
	}

	// by default, reset does nothing
	public void reset() {
	}

	// Abstract methods, vary from sprite to sprite
	public abstract Image getSpriteImage();

	public abstract int getHeight();

	public abstract int getWidth();

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}
	
	public BufferedImage getImage() {
		if(image == null)
			try {
				image = ImageIO.read(getClass().getResource("/"+getImageUrl()));
			} catch (Exception e) {}
		return image;
	}
}
